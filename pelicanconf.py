#!/usr/bin/env python
# -*- coding: utf-8 -*- #
import os

AUTHOR = "Pierre Augier"
SITENAME = "Pierre Augier's website"

# for developing (redefined in publishconf.py)
SITEURL = ""

PATH = "content"

TIMEZONE = "Europe/Paris"
DEFAULT_LANG = "fr"

# THEME = "themes/sober"
THEME = os.getenv("WEBSITE_PA_THEME", "../bootstrap-next")
THEME_TEMPLATES_OVERRIDES = ["themes/extra"]
# BOOTSTRAP_THEME = "cosmo"

STATIC_PATHS = ["images", "docs", "docs/ipynb", "extra/custom.css"]
EXTRA_PATH_METADATA = {"extra/custom.css": {"path": "theme/css/custom.css"}}
CUSTOM_CSS = "theme/css/custom.css"

DIRECT_TEMPLATES = ("index", "categories", "authors", "archives", "publications")

MENUITEMS = (
    ("Research", "pages/research.html"),
    ("Publications", "publications.html"),
    ("Teaching", "pages/teaching.html"),
    ("CV", "pages/cv.html"),
    ("Links", "pages/links.html"),
    ("FluidDyn", "pages/fluiddyn-project.html"),
    ("Archives", "archives.html"),
)
DISPLAY_PAGES_ON_MENU = False
# Plugins
PLUGIN_PATHS = ["plugins"]
PLUGINS = ["pelican_bibtex", "render_math", "i18n_subsites"]

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

JINJA_ENVIRONMENT = {"extensions": ["jinja2.ext.i18n"]}
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_CATEGORIES_ON_SIDEBAR = True
DISABLE_SIDEBAR_TITLE_ICONS = True
DOCUTIL_CSS = True

LINKS = (
    ("LEGI", "http://www.legi.grenoble-inp.fr"),
    ("CNRS", "http://www.cnrs.fr"),
    ("UGA", "https://www.univ-grenoble-alpes.fr/"),
    ("Python.org", "http://python.org/"),
)

ORCID_ID = "0000-0001-9481-4459"

SUMMARY_MAX_LENGTH = 20

# Social widget
SOCIAL = (  # ('You can add links in your config file', '#'),
    # ('Another social link', '#'),)
)

DEFAULT_PAGINATION = 20

RELATIVE_URLS = True

# TAGS_SAVE_AS = ''
# TAG_SAVE_AS = ''

PUBLICATIONS_SRC = "content/pubs.bib"

READERS = {"html": None}
IGNORE_FILES = ["*~", ".ipynb_checkpoints", "README.md"]
ARTICLE_EXCLUDES = ["docs/ipynb", "docs"]

# GOOGLE_ANALYTICS = "UA-47565467-1"

MYST_DOCUTILS_SETTINGS = {
    # "myst_gfm_only": True,
    "myst_enable_extensions": {
        "amsmath",
        "dollarmath",
        "linkify",
        "tasklist",
    },
}

MYST_SPHINX_SETTINGS = {
    # Sphinx settings
    "nitpicky": True,
    "keep_warnings": True,
    # MyST settings
    # issue with myst_gfm_only
    # "myst_gfm_only": True,
    "myst_substitutions": {
        "key1": "I'm a **substitution**",
    },
    "myst_enable_extensions": {
        "amsmath",
        "dollarmath",
        "tasklist",
        "linkify",
    },
    "suppress_warnings": ["myst.header"],
}

# There is an issue with new docutils (0.20.1)
# unknown references lead to an exception and nothing works
MYST_FORCE_SPHINX = True

MATHJAX = True
LOAD_CONTENT_CACHE = True
CACHE_CONTENT = True
STATIC_CREATE_LINKS = True
