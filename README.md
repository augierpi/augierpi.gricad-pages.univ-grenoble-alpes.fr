# Sources of my professional website

Just in case it can be useful for anyone.

For help: `make help`

## Install bootstrap-next

This website uses a [slightly modified
version](https://github.com/shvchk/bootstrap-next/pull/2) of the theme
bootstrap-next.

```bash
cd ..
hg clone git@github.com:paugier/bootstrap-next.git
cd bootstrap-next
hg up mathjax-more-optional
```

## Setup the environment

```bash
pip install pipx
pipx install pdm
pdm install
eval $(pdm venv activate)
```

mdformat and formatbibtex are used to format the sources of the book (command `make format`). They
can be installed for example with `pipx`:

```sh
pipx install mdformat
pipx inject mdformat mdformat-myst
pipx install formatbibtex
```
