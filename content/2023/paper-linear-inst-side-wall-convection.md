---
Title: Article on vertical convection accepted to JFM
Date: 2023-12-13
Modified: 2023-12-13
Category: Articles
summary: |-
  Our paper describing a study on the linear instabilities in a
  two-dimensional rectangular cavity has been accepted to the Journal
  of Fluid Mechanics.
---

Good news, our article "Vertical convection regimes in a two-dimensional
rectangular cavity: Prandtl and aspect ratio dependance"
([arXiv link](https://arxiv.org/abs/2304.12657)) by Arman Khoubani, Ashwin Vishnu
Mohanan, Pierre Augier and Jan-Bert Flór, has been accepted to the Journal of
Fluid Mechanics.

This study has been done during the PhD thesis of Arman Khoubani, who is
finalizing his manuscript and should defend his PhD next year. This study would
not have been possible without our new tool [Snek5000].

```{figure} ./images/fig_summary_sidewall-conv-instabilities.png
---
alt: Summary of the linear regimes
width: 80%
align: center
---
This figure summarizes the linear regimes obtained by varying the Prandtl
number at fixed aspect ratio.
```

**Abstract:**

Vertical convection is the fluid motion that is induced by the heating and cooling
of two opposed vertical boundaries of a rectangular cavity (see e.g. Wang et al.
2021). We consider the linear stability of the steady two-dimensional flow reached
at Rayleigh numbers of O($10^8$).

As a function of the Prandtl number, $Pr$, and the height-to-width aspect ratio of
the domain, $A$, the base flow of each case is computed numerically and linear
simulations are used to obtain the properties of the leading linear instability
mode. Flow regimes depend on the presence of a circulation in the entire cavity,
detachment of the thermal layer from the boundary or the corner regions, and on
the oscillation frequency relative to the natural frequency of oscillation in the
stably temperature-stratified interior, allowing for the presence of internal
waves or not. Accordingly the regime is called slow or fast, respectively. Either
the global circulation or internal waves in the interior may couple the top and
bottom buoyancy currents, while their absence implies asymmetry in their
perturbation amplitude.

Six flow regimes are found in the range of $0.1 \leq Pr \leq 4$ and
$0.5 \leq A \leq
2$. For $Pr \lessapprox 0.4 $ and $A>1$ the base flow is driven
by a large circulation in the entire cavity. For $Pr \gtrapprox 0.7$ the thermal
boundary layers are thin and the instability is driven by the motion along the
wall and the detached boundary layer. A transition between these regimes is marked
by a dramatic change in oscillation frequency at $Pr = 0.55 \pm0.15$ and $A <2$.

**Citation with Bibtex:**

```bibtex
@misc{khoubani2023vertical,
      title={Vertical convection regimes in a rectangular cavity: Prandtl and aspect ratio dependance},
      author={Arman Khoubani and Ashwin Vishnu Mohanan and Pierre Augier and Jan-Bert Flór},
      year={2023},
      eprint={2304.12657},
      archivePrefix={arXiv},
      primaryClass={physics.flu-dyn}
}
```

[snek5000]: https://snek5000.readthedocs.io
