---
Title: Submission of 2 articles on vertical convection
Date: 2023-04-28
Modified: 2023-04-28
Category: Articles
summary: |-
  We submitted two papers presenting work done during Arman Khoubani's PhD thesis.
---

We submitted two papers presenting work done during Arman Khoubani's PhD thesis.

<h3>Article on vertical convection regimes</h3>

The first paper was submitted to the Journal of Fluid Mechanics and the submitted
version is on ArXiv [here](https://arxiv.org/abs/2304.12657).

**Title:** Vertical convection regimes in a rectangular cavity: Prandtl and aspect
ratio dependance

**Authors:** Arman Khoubani, Ashwin Vishnu Mohanan, Pierre Augier and Jan-Bert
Flór

**Abstract:**

Vertical convection, often also called lateral convection, is the fluid motion
that is induced by the heating and cooling of two opposed vertical boundaries of a
rectangular cavity. In this numerical study, we consider the linear stability of
the steady two-dimensional flow reached at Rayleigh numbers of $O(108)$. This flow
consists of two plume motions near the boundaries and a linear stable temperature
stratification in the interior. As a function of the Prandtl number, Pr, and the
height-to-width aspect ratio of the domain, A, the base flow (steady state) of
each case is computed and linear simulations are used to obtain the properties of
the leading linear mode of instability.

The flow regimes show a rich variation with Prandtl number and aspect ratio. These
regimes depend on whether the plumes generate a circulation in the entire cavity,
detach from the horizontal boundaries or the corner regions, and further on
whether the oscillation frequency of the instability is slower (or faster) than
the buoyancy frequency of the stratification in the interior, and allows for the
presence of internal waves (or not), Accordingly, the regime is called slow or
fast, respectively. Internal wave allow for the coupling between the top and
bottom plumes, and their absence implies asymmetry in part of the regimes. Six
essentially different flow regimes are found in the range of $0.1\leq Pr≤4$ and
$0.5≤A≤2$.

<h3>Article on our new tool Snek5000</h3>

The second article was submitted on the Journal of Open-Source Software (JOSS).

**Title:** Snek5000: a new Python framework for Nek5000

**Authors:** Ashwin Vishnu Mohanan, Arman Khoubani and Pierre Augier

**Abstract:**

Computational fluid dynamics (CFD) simulations are essential tools in various
scientific and engineering disciplines. Nek5000 is a CFD Fortran code based on
spectral element methods with a proven track record in numerous applications. In
this article, we present Snek5000, a Python package designed to streamline the
management and visualization of fluid dynamics simulations based on Nek5000. The
package builds upon the functionality of Nek5000 by providing a user-friendly
interface for launching and restarting simulations, loading simulation data, and
generating figures and movies. This paper introduces Snek5000, discusses its
design principles, and highlights its impact on the scientific community.
