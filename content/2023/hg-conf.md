---
Title: Mercurial conference 2023 in Paris
Date: 2023-04-06
Modified: 2023-04-06
Category: Talks
summary: |-
  I will give a presentation at Mercurial conference 2023 about "Using
  Mercurial, evolve and hg-git in an academic context".
---

I gave a presentation at [Mercurial conference 2023] about "Using Mercurial,
evolve and hg-git in an academic context".

The slides are available [here](./docs/2023/hg-conf/pres.html).

## Abstract of my presentation

In our group, we base a lot of our work on few Python packages. The development of
these packages involves people with very different levels of experience in coding
and in versioning. Depending on the software project, the development is hosted on
different platforms: https://foss.heptapod.net/, an university Gitlab instance and
Github. For the projects hosted on [Heptapod], we use a workflow based on Merge
Requests (MR), topics and the evolve extension. This workflow presents great
advantages for us:

- the code review associated with each MR is very efficient to improve the skills
  and productivity of the students.

- Topics and evolve allow the experienced developers to directly interact on the
  MR and on their history (in particular with rebase, amend, fold, absorb, ...).
  The readability of the history of the repositories is largely improved.

- Fairly quickly, students master at least amend, lose the apprehension of making
  bad commits and tend to commit small changesets and to push more often. With
  hg-git, Mercurial can also be used for projects hosted on Gitlab and Github so
  students do not need to directly interact with Git.

[heptapod]: https://heptapod.net/
[mercurial conference 2023]: https://mercurial.paris/events/mercurial-conference-paris-2023/
