---
Title: Paper on Snek5000 accepted to JOSS
Date: 2023-08-28
Modified: 2023-08-28
Category: Articles
summary: |-
  Our paper describing Snek5000 has been accepted to the Journal of Open-Source Software.
---

Good news,
[our paper describing Snek5000](https://joss.theoj.org/papers/10.21105/joss.05586)
(our new Nek5000 framework based on Python) has been accepted to the Journal of
Open-Source Software.

**Abstract:**

Computational fluid dynamics (CFD) simulations are essential tools in various
scientific and engineering disciplines. Nek5000 is a CFD Fortran code based on
spectral element methods with a proven track record in numerous applications. In
this article, we present [Snek5000], a Python package designed to streamline the
management and visualization of fluid dynamics simulations based on Nek5000. The
package builds upon the functionality of Nek5000 by providing a user-friendly
interface for launching and restarting simulations, loading simulation data, and
generating figures and movies. This paper introduces [Snek5000], discusses its
design principles, and highlights its impact on the scientific community.

**Citation with Bibtex:**

```bibtex
@article{Mohanan2023,
  doi = {10.21105/joss.05586},
  url = {https://doi.org/10.21105/joss.05586},
  year = {2023},
  publisher = {The Open Journal},
  volume = {8},
  number = {88},
  pages = {5586},
  author = {Ashwin Vishnu Mohanan and Arman Khoubani and Pierre Augier},
  title = {Snek5000: a new Python framework for Nek5000}, journal = {Journal of Open Source Software} }
```

[snek5000]: https://snek5000.readthedocs.io
