---
Title: 'Journées Convection naturelle : aspects fondamentaux et applications'
Date: 2023-07-04
Modified: 2023-07-04
Category: Talks
summary: |-
  We will present our numerical work on vertical convection.
---

Arman Khoubani, Jan-Bert Flor and I will present our numerical work on
instabilities in vertical convection in a 2D rectangular cavity at the workshop
[Convection naturelle : aspects fondamentaux et applications](https://www.lisn.upsaclay.fr/news/convection-naturelle-aspects-fondamentaux-et-applications/).

My slides are available [here](./docs/2023/convection-conf/pres.html).

We plan to divide the presentation in two short talks:

## Prandtl and aspect ratio effects in vertical convection in a 2D rectangular cavity

### Authors

Arman Khoubani, Ashwin Vishnu Mohanan, Jan-Bert Flor and Pierre Augier

### Abstract

In this presentation we consider numerical stability results on the motion that is
induced by the heating and cooling of two opposed vertical boundaries of a
rectangular cavity. In particular the linear stability of the steady
two-dimensional flow reached at Rayleigh numbers of O($10^8$) is considered \[1\].
This flow consists of two plume motions near the boundaries and a linear stable
temperature stratification in the interior. As a function of the Prandtl number,
$Pr$, and the height-to-width aspect ratio of the domain, $A$, the base flow of
each case is computed and linear simulations are used to obtain the properties of
the leading linear mode of the instability.

The flow regimes show a rich variation with Prandtl number and aspect ratio. These
regimes depend on whether the plumes generate a circulation in the entire cavity,
detach from the horizontal boundaries or the corner regions, and further on
whether the oscillation frequency of the instability is slower (or faster) than
the buoyancy frequency of the stratification in the interior, and allows for the
presence of internal waves (or not), Accordingly, the regime is called slow or
fast, respectively. Internal wave allow for the coupling between the top and
bottom plumes, and their absence implies asymmetry in part of the regimes.

Six essentially different flow regimes are found in the range of
$0.1 \leq Pr
\leq 4$ and $0.5 \leq A \leq 2$. For small $Pr \leq 0.4 $ the plume
thickness is large and the base flow is driven by a large circulation in the
entire cavity. The aspect ratio has an effect on the cell patterns, the plume
detachment, and the oscillation frequency of the instability, which is slow for
wide cavities and fast for tall cavities. For large $Pr \geq 0.7$ the wall plumes
are thin and the instability is driven by the motion at the wall and the detached
plume. A transition between these regimes is marked by a dramatic change in
oscillation frequency at $Pr = 0.55$.

## Numerical simulations of natural convection flows with Snek5000 and Fluidsimfoam

### Authors

Pierre Augier, Ashwin Vishnu Mohanan, Arman Khoubani, Pooria Danaeifar and
Jan-Bert Flor

### Abstract

We will present two twin projects, Snek5000 and Fluidsimfoam, designed to improve
the user experience of the very popular CFD codes Nek5000 and OpenFOAM,
respectively. These packages builds upon the functionality of Nek5000 and OpenFOAM
by providing a user-friendly interface for launching and restarting simulations,
loading simulation data, and generating figures and movies. Snek5000 and
Fluidsimfoam are based on the CFD framework FluidSim, which introduces the concept
of "FluidSim solvers". A FluidSim solver consists of few files describing a set of
potential and similar simulations. A concrete simulation can be created and run
via a simple and generic Python API. We will show how these tools can be used to
study different types of natural convection problems. We will see how Snek5000
helped us greatly for a recent study on linear instabilities in vertical
convection.
