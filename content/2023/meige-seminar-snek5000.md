---
Title: Snek5000, a new Python framework for numerical simulations based on Nek5000
Date: 2023-03-02
Modified: 2023-03-02
Category: Talks
summary: |-
  I gave a group seminar in my lab on our new software project Snek5000.
---

I gave a group seminar in my lab on our new software project [Snek5000].

[snek5000]: https://snek5000.readthedocs.io
