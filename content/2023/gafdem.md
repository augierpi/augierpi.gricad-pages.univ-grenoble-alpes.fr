---
Title: GAFDEM conference in Nice
Date: 2023-09-11
Modified: 2023-09-15
Category: Talks
summary: |-
  I will give a presentation at the GAFDEM conference about
  "Comprehensive open datasets of stratified turbulence forced in vertical
  vorticity or wave modes".
---

I will give a presentation at the
[GAFDEM conference](https://gafdem.sciencesconf.org/) (Geophysical and
Astrophysical Fluid Dynamics, Experiments and Models) about "Comprehensive open
datasets of stratified turbulence forced in vertical vorticity or wave modes". I
will actually focus on results obtained from simulations forced with internal
gravity waves.

The slides are available [here](./docs/2023/gafdem/pres.html).
