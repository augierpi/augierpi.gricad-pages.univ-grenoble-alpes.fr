---
Title: Fluidsimfoam at OpenFOAM users conference
Date: 2023-06-13
Modified: 2023-06-13
Category: Conferences
summary: |-
  I will present our new Python package Fluidsimfoam at the OpenFOAM users
  conference.
---

I will present our new Python package [Fluidsimfoam] at the
[OpenFOAM users conference]. My slides are available
[here](./docs/2023/openfoam-conf/pres.html). I also prepared a short poster, which
can be downloaded [here](./docs/2023/openfoam-conf/poster_fluidsimfoam.pdf).

<h3>Note for the live demo</h3>

During the presentation I will do a very simple demo (described in
[the slides](./docs/2023/openfoam-conf/pres.html)). I think it would be more
valuable if people can follow on their computers and try to play with Fluidsimfoam
during the conference. Therefore, I give here indications how to setup
Fluidsimfoam to follow the demo. Since we have only 20 min for the presentation,
one needs to install the requirements and Fluidsimfoam before! The requirements
are **Python >=3.9** (with `pip`!), **OpenFOAM** and if possible
**[Mercurial](https://www.mercurial-scm.org/)** (only to download the source code,
which you can actually also download without Mercurial from
https://foss.heptapod.net/fluiddyn/fluidsimfoam) and **Poetry** (to be installed
with one of the methods presented
[here](https://python-poetry.org/docs/#installation)).

When the requirements are installed, Fluidsimfoam can be installed and tested
with:

```sh
hg clone https://foss.heptapod.net/fluiddyn/fluidsimfoam
cd fluidsimfoam
poetry install
poetry shell
# setup OpenFOAM as usual
pytest -v
```

If everything runs without error, you are ready for the demo! Otherwise, you can
create an issue [here](https://foss.heptapod.net/fluiddyn/fluidsimfoam/-/issues)
and explain your problem.

[fluidsimfoam]: https://fluidsimfoam.readthedocs.io
[openfoam users conference]: https://www.foam-u.fr/
