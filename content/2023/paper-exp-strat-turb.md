---
Title: Letter on experiments of stratified turbulence forced in waves accepted to
  PRL
Date: 2023-11-22
Modified: 2023-11-22
Category: Articles
summary: |-
  Our paper describing experiments of stratified turbulence forced in waves has been accepted to Physical Review Letters.
---

Good news, our letter "From internal waves to turbulence in a stably stratified
fluid" ([arXiv link](https://arxiv.org/abs/2311.13476)), describing experiments of
stratified turbulence forced in waves has been accepted to Physical Review
Letters.

**Abstract:**

We report on the statistical analysis of stratified turbulence forced by
large-scale waves. The setup mimics some features of the tidal forcing of
turbulence in the ocean interior at submesoscales. Our experiments are performed
in the large-scale Coriolis facility in Grenoble which is 13 m in diameter and 1 m
deep. Four wavemakers excite large scale waves of moderate amplitude. In addition
to weak internal wave turbulence at large scales, we observe strongly nonlinear
waves, the breaking of which triggers intermittently strong turbulence at small
scales. A transition to strongly nonlinear turbulence is observed at smaller
scales. Our measurements are reminiscent of oceanic observations. Despite
similarities with the empirical Garrett & Munk spectrum that assumes weak wave
turbulence, our observed energy spectra are rather be attributed to strongly
nonlinear internal waves.

**Citation with Bibtex:**

```bibtex
@misc{rodda2023internal,
      title={From internal waves to turbulence in a stably stratified fluid},
      author={Costanza Rodda and Clément Savaro and Vincent Bouillaut and Pierre Augier and Joël Sommeria and Thomas Valran and Samuel Viboud and Nicolas Mordant},
      year={2023},
      eprint={2311.13476},
      archivePrefix={arXiv},
      primaryClass={physics.flu-dyn}
}
```
