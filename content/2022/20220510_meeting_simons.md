---
Title: Meeting Simons fundation stratified turbulence and waves
Date: 2022-05-10
Modified: 2022-05-10
Category: Talks
summary: Presentation on a numerical study on stratified turbulence forced by vortices.
---

Voilà une
<a
href=./docs/ipynb/20220510simons-strat-turb/pres.slides.html>présentation sur
notre travail avec Vincent Labarre sur des simulations de turbulence stratifiée
forcée par des tourbillons</a>.
