---
Title: Fluiddyn releases and teaching in September
Date: 2022-09-27
Modified: 2022-09-27
Category: Methods
summary: Many new package versions and courses...
---

Close to Grenoble, end of summer is perfect to harvest mushrooms. Unfortunately,
we didn't find so many good mushrooms with the family because we were a bit late.
Anyway, we have
[a good production of new versions of Fluiddyn Python packages](https://pythondevs.social/notice/ANje1cCYumsfDsEsyG).

```{image} https://cdn.pixabay.com/photo/2015/10/13/19/45/autumn-986710_960_720.jpg
---
width: 70%
alt: Harvest Mushrooms
align: center
---
```

So here are the brand new
[fluiddyn v0.5.1](https://pypi.org/project/fluiddyn/0.5.1/),
[conda-app v0.3.2](https://pypi.org/project/conda-app/0.3.2/),
[transonic v0.5.1](https://pypi.org/project/transonic/0.5.1/),
[fluidfft v0.3.3](https://pypi.org/project/fluidfft/0.3.3/) and
[fluidsim v0.6.1](https://pypi.org/project/fluidsim/0.6.1/). Except for fluidsim,
these are mostly "small" releases containing bug fixes and small improvements.

In contrast, the release of Fluidsim 0.6.1 is the result of a major work done with
Vincent Labarre from Observatoire de la Côte d'Azur. We recently presented our
work on stratified turbulence forced by vortices or internal waves at the
[summer school FDSE](%7Bfilename%7D/2022/pres_open_science_fluid.md), at the
[EuroMech Colloquium 619](%7Bfilename%7D/2022/pres_euromech619.md) and at
[ISSF 2022](%7Bfilename%7D/2022/issf.md).

Fluidsim 0.6.1 also contains some new features implemented for the course
[Advanced Numerical Methods](https://master-tma.gricad-pages.univ-grenoble-alpes.fr/num-methods-turb)
of the [TMA master](https://master-tma.gricad-pages.univ-grenoble-alpes.fr):

- Turbulence models with `extend_simul_class`
  ([Merge request !308](https://foss.heptapod.net/fluiddyn/fluidsim/-/merge_requests/308))

- Kolmogorov forcing
  ([Merge request !307](https://foss.heptapod.net/fluiddyn/fluidsim/-/merge_requests/307))

- Output
  [fluidsim.base.output.horiz_means](https://fluidsim.readthedocs.io/en/latest/generated/fluidsim.base.output.horiz_means.html)
  ([Merge request !309](https://foss.heptapod.net/fluiddyn/fluidsim/-/merge_requests/309))

I'm also doing quite a lot of teaching. I learned how to use [Jupyterbook] and
[MyST] (which are really great tools) to build nice websites for these courses:

- Course *Instabilities and turbulence* (see the
  [sources](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence)
  and the
  [website](https://augierpi.gricad-pages.univ-grenoble-alpes.fr/coursem1_pa_instabilities_turbulence/)),
  [Master 1 Applied Mechanics](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-mecanique-IAQK579W/parcours-applied-mechanics-1re-annee-KVQRVL29.html)

- Course *Introduction to scientific computing* (see the
  [sources](https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm)
  and
  [website](https://meige-legi.gricad-pages.univ-grenoble-alpes.fr/scientific-computing-m2-efm))
  [Master 2 EFM](http://master-efm.legi.grenoble-inp.fr/).

- New course *Méthodes numériques avancées pour la turbulence* (see the
  [sources](https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma/num-methods-turb)
  and
  [website](https://master-tma.gricad-pages.univ-grenoble-alpes.fr/num-methods-turb))
  [Master 2 TMA](https://brunch.gricad-pages.univ-grenoble-alpes.fr/master-tma-turbulences-methodes-et-applications)

[jupyterbook]: https://jupyterbook.org
[myst]: https://myst-parser.readthedocs.io
