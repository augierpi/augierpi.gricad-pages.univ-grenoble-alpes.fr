---
Title: Open Science in fluid mechanics
Date: 2022-06-28
Modified: 2022-06-28
Category: Talks
summary: Presentation at Ecole Polytechnique for the summer school FDSE
---

On Tuesday 28th June 2022, I gave a presentation at Ecole Polytechnique for the
[summer school FDSE](http://fdse.org/) on **Open science in fluid mechanics with
an example on a study on stratified turbulence forced in vortices**.

The slices of my presentation are
<a href=./docs/ipynb/20220628open-science-strat-turb/pres.slides.html>here</a>.
