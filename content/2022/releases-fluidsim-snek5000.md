---
Title: Snek5000 and Fluidsim releases
Date: 2022-11-24
Modified: 2022-11-24
Category: Methods
summary: Snek5000 0.8.0 finally available!
---

## Snek5000 0.8.0 and Fluisim 0.7.0

I am very pleased to announce the release of version 0.8.0 of the numerical
simulation code [Snek5000] (accompanied by the [new Fluidsim 0.7.0]).

These software are the result of an important work by [Ashwin Vishnu Mohanan],
Arman Khoubani and me. We finalized Snek5000 0.8.0 during a
[long sprint during October and November 2022 with not less than 60 issues closed on Snek5000 repository](https://github.com/snek5000/snek5000/milestone/1).

## Changes in Fluidsim 0.7.0

For Fluidsim 0.7.0, I made deep refactoring of the code and moved a lot of code
(related to movies and restart) in the pure Python package `fluidsim-core`.

I also improved quite a lot the code related to movies. So now, once a simulation
is loaded as `sim`, we can call things like
`sim.output.phys_fields.animate(interactive=True, equation="y=1")` to get a 2D
interactive movie with buttons to control the movie (see the
[documentation of the animate method](https://fluidsim.readthedocs.io/en/latest/generated/fluidsim_core.output.movies.html#fluidsim_core.output.movies.MoviesBase.animate)).
The code is general enough to be used for Fluidsim pseudospectral solvers and for
Snek5000 solvers. With the argument `save_file`, it can give a movie like this
one:

<video style="width:100%;" controls>Your browser does not support the \<video>
tag.
    <source src=https://user-images.githubusercontent.com/8842662/202872147-4ea3c749-dc63-4a73-98a0-6c787edb9cd3.mp4>
</video>

## What is Snek5000

[Snek5000] was initially created by [Ashwin Vishnu Mohanan] to make Nek5000
simulations more user-friendly and reliable. The first few words of the README
(https://github.com/snek5000/snek5000) give a good idea of what Snek5000 is:

"Snek5000 is a Python package which allows one to write [Fluidsim] solvers based
for the simulations on the Fortran CFD code [Nek5000]. There are open-source
solvers (in particular [snek5000-phill], [snek5000-cbox] and [snek5000-tgv]) and
it's not difficult to write your own solver based on your Nek5000 cases (as shown
in [this tutorial](https://snek5000.readthedocs.io/en/latest/packaging.html)).

With a Snek5000-Fluidsim solver, it becomes very easy to

- launch/restart simulations with Python scripts and terminal commands,
- load simulations, read the associated parameters/data and produce nice
  figures/movies.

Snek5000 can be seen as a workflow manager for Nek5000 or a Python wrapper around
Nek5000. It uses Nek5000 on the background and is thus NOT a rewrite of Nek5000!"

For the thesis of Arman Khoubani, we wanted to use [Nek5000] for simulations of
convection on side walls. Since this required a parametric study, which in turn
requires a good scriptable workflow to launch, organize and process several
Nek5000 simulations, [Snek5000] was a good solution. Thus, we started to use
[Nek5000] through the [Snek5000]. We contributed to the package, and it has now
reached a sufficient degree of maturity to be shared and be used by others, with
Snek5000 version 0.8.0.

The [documentation for Snek5000](https://snek5000.readthedocs.io) contains many
tutorials and examples. It seems to me that they demonstrate quite well how
Snek5000 completely changes the Nek5000 user experience. If you are a Nek5000 user
or have ever thought that spectral element methods could be useful to you, you
should take a look at Snek5000!

We plan to release Snek5000 0.9.0 in few days after taking into account your
feedback, so don't hesitate to contact us via our [chat], [issue tracker] or just
emails. Moreover, we are of course very open to [contributions].

## Fluidsim-core used to provide a Python interface to other CFD codes

More broadly, Snek5000 is a concrete proof of how Fluidsim can be used to add a
good user interface to a numerical simulation code based on the Python scientific
ecosystem.

It may seem a bit anecdotal, but adding a well-thought-out user interface can
drastically change what can be done with a simulation code (for example, a
parametric study with dozens of simulations) as well as the way to collaborate
around this software.

Even in the case of Nek5000, a monolithic code written in FORTRAN 77 and which
does not allow for a fine interfacing with another language, the use of Fluidsim
leads to a very clear improvement of the user experience. The possibilities would
be greater with more modern, modular and interfaceable code.

I would be happy to think about and help to create equivalents of Snek5000 for
other computational codes, so please feel free to contact me/[contact us].

[ashwin vishnu mohanan]: https://fluid.quest/
[chat]: https://matrix.to/#/#fluiddyn-users:matrix.org
[contact us]: https://matrix.to/#/#fluiddyn-users:matrix.org
[contributions]: https://github.com/snek5000/snek5000/blob/main/CONTRIBUTING.md
[fluidsim]: https://fluidsim.readthedocs.io
[issue tracker]: https://github.com/snek5000/snek5000/issues
[nek5000]: https://nek5000.mcs.anl.gov/
[new fluidsim 0.7.0]: https://fluidsim.readthedocs.io/en/latest/changes.html
[snek5000]: https://github.com/snek5000/snek5000
[snek5000-cbox]: https://github.com/snek5000/snek5000-cbox
[snek5000-phill]: https://github.com/snek5000/snek5000-phill
[snek5000-tgv]: https://github.com/snek5000/snek5000/tree/main/docs/examples/snek5000-tgv
