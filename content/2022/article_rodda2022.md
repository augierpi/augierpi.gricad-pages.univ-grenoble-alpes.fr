---
Title: New article on stratified turbulence forced in waves
Date: 2022-10-04
Modified: 2022-10-04
Category: Articles
summary: |-
  Experiments in the Coriolis platform of stratified turbulence forced in
  internal gravity waves
---

A [new article](https://doi.org/10.1103/PhysRevFluids.7.094802) by Costanza Rodda
et al. describing an experimental study on internal gravity wave turbulence has
just been published in
[Physical Review Fluids](https://journals.aps.org/prfluids). The title is:
**Experimental observations of internal wave turbulence transition in a stratified
fluid**. Such experiments in the Coriolis platform are complicated and involve a
lot of people: Costanza Rodda, Clément Savaro, Géraldine Davis, Jason Reneuve,
Pierre Augier, Joël Sommeria, Thomas Valran, Samuel Viboud, and Nicolas Mordant.

```{image} images/sketch_rodda2022.png
---
width: 70%
alt: Sketch wave turbulence experience in the Coriolis platform
align: center
---
```

**Abstract:** Recent developments of the weak turbulence theory applied to
internal waves exhibit a power-law solution of the kinetic energy equation close
to the oceanic Garrett-Munk spectrum, confirming weakly nonlinear wave
interactions as a likely explanation of the observed oceanic spectra. However,
finite-size effects can hinder wave interactions in bounded domains, and
observations often differ from theoretical predictions. This paper studies the
dynamical regimes experimentally developing in a stratified fluid forced by
internal gravity waves in a pentagonal domain. We find that by changing the shape
and increasing the dimensions of the domain finite-size effects diminish and wave
turbulence is observed. In this regime, the temporal spectra decay with a slope
compatible with the Garrett-Munk spectra. Different regimes appear by changing the
forcing conditions, namely, discrete wave turbulence, weak wave turbulence, and
strongly stratified turbulence. The buoyancy Reynolds number $Re_b$ marks well the
transitions between the regimes, with weak wave turbulence occurring for
$1 \leq Re_b \leq 3.5$ and strongly nonlinear stratified turbulence for higher
$Re_b$.
