---
Title: ANR DisET meeting à Lyon
Date: 2021-10-15
Modified: 2021-10-15
Category: Talks
---

Voilà une <a href=./docs/ipynb/20211015ANR_DisET/pres.slides.html>présentation
sur notre travail avec Jason Reneuve et Nicolas Mordant sur des simulations de
turbulence stratifiée forcée par des ondes internes</a>
