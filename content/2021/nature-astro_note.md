---
Title: New Nature Astronomy paper on Reducing the ecological impact of computing through education and Python compilers
Date: 2021-04-01
Modified: 2021-04-01
Category: Articles
---

Avec un petit groupe de collègues impliqués dans la communauté du calcul avec
Python, nous avons pu publier [une réponse](https://www.nature.com/articles/s41550-021-01342-y) ([preview](
https://www.nature.com/articles/s41550-021-01342-y.epdf) et
[lien HAL](https://hal.archives-ouvertes.fr/hal-03432227)) à un article paru dans
Nature Astronomy sur "The ecological impact of high-performance computing in
astrophysics" (Zwart, 2020).

```{figure} images/figs_reply_Zwart2020/fig_ecolo_impact.png
---
alt: fig_ecolo_impact.png
width: 60 %
align: center
---
Figure tirée de l'article de Zwart (2020). Python serait très mauvais pour cet algo.
```

Nous sommes entièrement d'accord avec son message principal : les scientifiques
doivent être attentifs à leur empreinte carbone. Par contre, nous avons montré
qu'une des solutions proposées, éviter le langage de programmation Python,
n'est pas pertinente. Nous montrons qu'elle serait contre-productive et que les
programmes scientifiques écrits en Python peuvent être efficaces et économes en
énergie. Nous soutenons que l'avancement de la technologie des compilateurs,
les facteurs humains et l'éducation sont bien plus importants que le choix du
langage.

Pour étayer son idée, Zwart présente un benchmark du problème N-Body avec une
implémentation inefficace en Python, 50 fois plus lente qu'une implémentation
en C++. En tant qu'utilisateurs de Python soucieux de notre impact écologique,
nous avons travaillé sur des benchmarks similaires sur le même problème.
problème. Contrairement à Zwart, nous (i) considérons des implémentations
efficaces en Python et en Julia et (ii) mesurons correctement la consommation
d'énergie avec du matériel dédié équipé de wattmètres.

Les résultats sont résumés sur la figure ci-dessous, qui représente pour le
même problème et différentes implémentations la production de CO$_2$ en
fonction du temps d'exécution. Ils démontrent qu'on peut obtenir avec des
compétences très moyennes de très bonnes performances avec des codes
scientifiques écrits en Python.

```{figure} images/figs_reply_Zwart2020/fig_bench_nbabel_parallel.png
---
alt: fig_bench_nbabel_parallel.png
width: 70 %
align: center
---
Figure tirée de notre article. Efficiency in terms of CO2 production and elapsed time for
implementations in Python, Julia, C++ and Fortran. Energy consumption
measurements were carried out on Grid’5000 clusters with 2.30 GHz Intel Xeon
E5-2630 processors and converted from kWh to CO2 using 283 g CO2 / kWh.
```
