---
Title: Mercurial plan in 2025
Date: 2025-01-08
Modified: 2025-01-08
Category: Numerics
summary: |-
  Mercurial is great for scientists. But it has to be improved!
---

# .

Mercurial is a distributed version control system, similar to Git but easier and for some
aspect more powerful. Version control systems should be used by all persons working with
code, like scientists. Mercurial is in my opinion particularly adapted for scientists.

However, Mercurial is in a quite bad situation, with a lot of projects switching to Git
and Github, for example PyPy and Firefox. Mercurial project needs to change to continue
to exist as a nice alternative to Git.

Nevertheless Mercurial still has some strengths (for example nice user interface,
collaborative and safe history edition and high performance on large repositories), which
have to be put forward. Some weaknesses also need to be tackled.

## Mercurial user experience

### Native support for Git server

Git/Github and Gitlab are so central nowadays in programming that any versioning tool has
to be highly compatible with Git. The two recent projects [Sapling] and [Jujutsu]
(also called `jj`) are both "Git compatible".

- Sapling website says that it "integrate\[s\] with Git":

  "Sapling client also supports cloning and interacting with Git repositories and can be
  used by individual developers to work with GitHub and other Git hosting services."

- Jujutsu describes itself as "A Git-compatible VCS that is both simple and powerful":

  "Today, we use Git repositories as a storage layer to serve and track content, making
  it compatible with many of your favorite Git-based tools, right now! All core
  developers use Jujutsu to develop Jujutsu, right here on GitHub."

The situation is much worse for Mercurial. There are currently two extensions to interact
with a Git repo:

- the core extension
  [hgext.git](https://foss.heptapod.net/mercurial/mercurial-devel/-/tree/branch/default/hgext/git)
  ("core" means provided with the Mercurial package).

  For this extension, one needs to clone, pull and push using `git`. Mercurial interacts
  with the local Git repo using [pygit2]. Changes have recently been done in `hgext.git`.
  However, it is still not compatible with recent version of [pygit2] and very
  limited, for example `hg st` is not implemented!

- the external extension [hg-git](https://foss.heptapod.net/mercurial/hg-git).

  I use [hg-git] for all my interactions with Git repos and it works well.

  In this case, Mercurial maintains with [Dulwich] a Git repo in the directory `.hg`.
  Therefore, hg-git works with two local repositories (one in Mercurial and another in
  Git), which is clearly not optimal in terms of memory and performance (especially for
  cloning). The other main limitation of hg-git is that it is not compatible with modern
  Mercurial using the topic and evolve extension. Git branches are represented with
  Mercurial bookmarks. These are indeed the closest equivalent to Git branches but the
  user experience is much less nice than with Mercurial topics, which is the modern
  Mercurial object to represent "feature branches".

  Another issue with hg-git is that the result of the conversion obtained after a pull is
  not stable, which highly limits the possibility that several people collaborate on the
  Mercurial side. However, this could change with the upcoming hg-git 1.2.

Therefore, there is nowadays no perfect solutions to interact with a remote Git
repository with modern Mercurial. However, another better strategy could be used.
Mercurial could consume and create on-the-fly the main data structure used by Git
(packfiles). This could be quite efficient and could be to some extend compatible with
modern Mercurial. We need to build a proof of concept to better evaluate this possible
solution. It could be done during an internship.

### Simple installation and setup

- [x] Wheels are available and it is possible to install with tools like [pipx], [UV],
  [Pixi] and [conda-app].

- [ ] Optional dependencies (like `pipx install mercurial[full]`)
  [!1153](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1153).

- [ ] Fix Windows wheels and conda-forge packages (with
  [!1193](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1193) and
  [!1206](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1206)).

- [ ] Unix wheels with the Rust extension (and document environment variable to
  activate it).

- [ ] rhg in Unix wheels (and communicate on how it can be used)?

- [ ] Modern
  [native Windows application and installer]({filename}/2025/about-windows-apps.md)
  (built from wheels) with a signed `hg.exe` and with ability to install/manage
  extensions (Python packages).

- [x] We now have a new tool [hg-setup](https://foss.heptapod.net/fluiddyn/hg-setup).

- [ ] Finalize core command for completion
  ([!1163](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1163)).

- [ ] Finalize core command to change configuration value
  ([!1165](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1165)).

- [ ] Document hg-setup in the official Mercurial website?

## Communication about Mercurial

We need to present the advantages over Git. To show that Mercurial is still modern in
2025 and than it can be useful for some use cases, in particular teaching (a nicer Git
client) and for big repos. To present hosting solutions.

We would need to organize a presence in social medias, conferences and journals.

### New websites

The current website https://www.mercurial-scm.org/ is very old and do not present modern
Mercurial.

- [x] propose a new website based on Sphinx (see
  https://foss.heptapod.net/mercurial/hg-website/-/tree/topic/default/with-sphinx and
  https://mercurial.pages.heptapod.net/hg-website/topic/default/with-sphinx).

- [x] finalize the new website and publish it on https://www.mercurial-scm.org.

## Mercurial development

### New bug tracker

The current bug tracker (https://bz.mercurial-scm.org/) is not user friendly (cannot
correct messages, cannot use Markdown, etc.) and does not integrate well with Heptapod.

It would be much better (i.e. nicer for contributors and bug reports) to use
https://foss.heptapod.net/mercurial/mercurial-devel/-/issues and
https://foss.heptapod.net/mercurial/mercurial-devel/-/milestones.

- [ ] Migrate to Heptapod issues?

- [ ] Use Heptapod milestones?

### Modernize Mercurial setup.py

- [x] `run-tests.py` based on virtual envs instead of `PYTHONPATH`
  ([!1117](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1117) and
  [!1188](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1188))

- [x] Use setuptools and no longer distutils (PEP 517/518)
  ([!1131](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1131))

- [x] Setuptools-scm
  ([!1207](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1207))

- [x] Static MANIFEST.in ([!1216])

- [ ] About doc (build, in sdist?, ...)

- [x] Fix `mercurial/__modulepolicy__.py`
  ([!1210](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1210))

- [ ] Fix conda-forge package
  ([!1193](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1193))

- [ ] Standard console-script entry point for hg
  ([!1206](https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1206))

- [ ] Move packages and module in a directory src (avoid warning "Please be careful
  with folders in your working directory \[...\]")

- [ ] Adopt modern Python development tools?

  - [ ] Nox ([!1218])
  - [ ] PDM

- [ ] Clean up Makefile and remove all direct calls of setup.py ([!1216] and [!1218])

- [ ] About generation of `hgext/__index__.py`

- [ ] No py2exe in setup.py?

- [ ] Use https://pypi.org/project/setuptools-rust/?

[!1216]: https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1216
[!1218]: https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1218
[conda-app]: https://pypi.org/project/conda-app/
[dulwich]: https://pypi.org/project/dulwich
[hg-git]: https://wiki.mercurial-scm.org/HgGit
[jujutsu]: https://github.com/jj-vcs/jj
[pipx]: https://github.com/pypa/pipx
[pixi]: https://pixi.sh
[pygit2]: https://pypi.org/project/pygit2/
[sapling]: https://sapling-scm.com/
[uv]: https://github.com/astral-sh/uv
