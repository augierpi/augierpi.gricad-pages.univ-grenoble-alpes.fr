---
Title: Mercurial and its near future
Date: 2024-11-10
Modified: 2024-11-10
Category: Numerics
summary: |-
  Mercurial is great but could be improved...
---

Mercurial is a distributed version control system, similar to Git but easier and for some
aspect more powerful. Version control systems should be used by all persons working with
code, like scientists. Mercurial is particularly adapted for scientists.

We use Mercurial for most of the Fluiddyn packages (hosted on
<https://foss.heptapod.net/>, Heptapod being a Gitlab fork supporting Mercurial) and I
use it on a daily basis to interact with Git projects hosted on Github and Gitlab. I also
use Mercurial to teach version control to students and colleagues.

Mercurial is a Python application which works with extensions which are Python packages.
With the hg-git extension (<https://foss.heptapod.net/mercurial/hg-git>), Mercurial can
be used as a Git client, to interact with projects hosted on Github and Gitlab. I see
that Mecurial is *really* nicer than the official Git client.

The Git client is at the same time too complex (with notions like staging area, blobs,
trees, branches, HEAD, ... and complicated commands) and not good regarding history
edition (need of force push). With Mercurial simple things are simple and more advanced
tasks (often related to history edition) are simple and safe.

Mercurial is still an active projects with professional developers working on its
development. However, Mercurial, which is a Python application, did not follow Python
improvements in terms of packaging. For example, Mercurial release manager does not yet
upload wheels on pypi.org, so that `pip install mercurial` trigger local compilation.
Moreover, the Rust extension is not compiled by default.

Tools like [pipx], [uv] and [conda-app] should be perfect to install and manage
Mercurial. One should be able to install and setup a good Mercurial environment with
something like:

```sh
uv tool install mercurial
echo 'eval "$(hg generate-shell-completion bash)"' >> ~/.bashrc
hg admin-extension --enable topic evolve absorb rebase hg-git
hg config --set ui.name "my_login <toto@univ-grenoble-alpes.fr>"
hg config --set ui.tweakdefaults True
hg config --set alias.lg "log -G"
```

I opened few issues about these needs:
<https://bz.mercurial-scm.org/show_bug.cgi?id=6923>

I worked on [a demonstrator](https://github.com/paugier/mercurial-ga-ci-cache-repo) that
uses Github Actions to build Mercurial wheels.

I worked on [conda-app] so that students using [miniforge] can easily get a good
Mercurial on all mainstream platforms (including Windows and macOS).

```sh
conda activate base
pip install conda-app
conda-app install mercurial
```

I also invested some time on two merge requests on Mercurial main development repository
<https://foss.heptapod.net/mercurial/mercurial-devel/>:

- <https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1009> (use modern
  setuptools for pep 517)
- <https://foss.heptapod.net/mercurial/mercurial-devel/-/merge_requests/1022> (clean up
  setup.py)

[conda-app]: https://pypi.org/project/conda-app/
[miniforge]: https://github.com/conda-forge/miniforge
[pipx]: https://github.com/pypa/pipx
[uv]: https://github.com/astral-sh/uv
