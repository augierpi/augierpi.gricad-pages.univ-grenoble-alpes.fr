---
Title: Nouveau modules Python au LEGI et site fluidhowto
Date: 2024-06-14
Modified: 2024-06-14
Category: Projects
summary: |-
  Sur la façon d'utiliser Python au LEGI et ailleurs...
---

Dans mon laboratoire le LEGI, deux nouveaux modules Python sont accessibles. Comme je
suis largement responsable de ces changements, j'explique dans cette note ce que sont ces
modules et ce qui change.

La commande `module` permet de gérer des variables d'environnement pour utiliser des
programmes et libraries. Les administrateurs peuvent fournir un module que les
utilisateurs peuvent charger avec une commande comme `module load python/3.11.2`. Cette
méthode est très utilisée pour des clusters, par exemple les clusters nationaux du CINES,
mais aussi dans certains laboratoires comme au LEGI.

J'avais il y quelques années travaillé sur une méthode pour créer le module Python basée
sur la compilation à partir du code source de CPython, ce qui permettait de fabriquer des
module pour différentes versions de Python et notamment des versions plus à jour que le
Python du système fourni par Debian. C'est cette méthode qui était utilisée par Cyrille,
un de nos ingénieurs informatiques, pour produire les modules Python du labo et cela
demandait un peu de travail à la main.

De plus, il n'y avait aucune notion de reproductibilité, c'est-à-dire que cette méthode
peut produire des modules différents même en faisant exactement les mêmes commandes.

Pour modifier un des modules Python, Cyrille pouvait installer à la main un nouveau
paquet, ce qui pouvait changer les versions des autres modules sans aucun contrôle. On ne
pouvait pas non plus reproduire exactement une version du module ayant existé à une date
donnée.

Un autre problème était la possibilité pour un utilisateur de modifier le module en
faisant des `pip install --user`, ce qui pouvait en plus changer le comportement des
applications Python du système. Chaque utilisateur des modules ayant une fois utilisé
`pip install --user` se retrouve avec une version différente du module, donc avec
potentiellement des comportements différents (et des bugs, car avec cette méthodes on n'a
aucune certitude sur la compatibilité des paquets installés).

Je n'étais pas enthousiaste mais comme moi et les personnes travaillant avec moi, nous
n'utilisions pas les modules, je n'avais pas trop réfléchi à cette problématique des
modules Python du LEGI.

Ces dernières années, les procédures d'installations de paquets Python se sont beaucoup
améliorées et les versions de Python du système pour les versions de Debian utilisées au
LEGI sont maintenant raisonnables pour une utilisation en science. Debian 11 (Bulleye,
actuellement "old stable") utilise Python 3.9 et Debian 12 (Bookworm, actuellement
"stable") Python 3.11. Récemment, j'ai avancé dans le développement des paquets du projet
Fluiddyn et notamment de Fluidimage, ce qui fait que le module Python devient un outil
intéressant pour permettre une utilisation facile de ces outils au LEGI.

Donc il était temps de trouver une meilleure solution, avec la condition qu'elle ne
perturbe pas les utilisateurs actuels des modules.

Je ne rentre pas dans le détail mais les modules `python/3.9.2` sur Debian 11 et
`python/3.11.2` sur Debian 12 sont maintenant des environnements virtuels "read-only"
utilisant le Python du système. C'est très raisonnable pour ce pourquoi ces modules sont
faits (utilisations hyper simples, sans installation de paquets).

De plus la fabrication et les potentielles mises à jour de ces modules sont maintenant
automatisées (avec des tests), contrôlées (avec [PDM] et un lock file) et ne nécessitent
plus d'intervention de Cyrille. La définition des modules ne se fait plus seulement par
notre service info mais par les personnes intéressées sur les sites suivant :

- <https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/softsync-python-debian11>
- <https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/softsync-python-debian12>

Avec cette méthode, il est aussi très simple de recréer un environnement que le module a
fourni à une date antérieure.

Pour l'instant, il y a 2 modules Python au LEGI utilisant cette nouvelle stratégie :
`python/3.9.2` sur Debian 11 et `python/3.11.2` sur Debian 12.

Sur Debian 11 (actuellement sur les clusters), il y a de grandes chances que vous
n'utilisiez pas déjà ce module, car les deux modules historiques (3.9.7 et 3.12.0) n'ont
pas été supprimés. Il faut donc taper explicitement `module load python/3.9.2` pour
utiliser le nouveau module. Par contre, sur Debian 12 (pour l'instant installé sur
quelques PC de bureau et portables au LEGI), il n'y a que ce module, donc l'ancienne
méthode devrait être abandonnée lorsqu'on arrêtera d'utiliser Debian 11 (typiquement fin
2024).

Les conséquences directes pour les utilisateurs sont

- tous les utilisateurs des modules Python utilisent exactement le même environnement
  (pas de "bug surprise")
- personne ne peut faire de `pip install` depuis ce module, même avec `--user`.

Si quelqu'un fait un `pip install` depuis un module, une erreur

```text
ERROR: Could not install packages due to an OSError: [Errno 13] Permission non accordée:
```

est affichée. Cela indique que l'utilisateur n'a pas les droits pour faire ça. C'est pour
moi une bonne chose, car du moment où on doit faire un `pip install`, il faut réfléchir à
où on le fait. J'espère que l'utilisateur aura la bonne idée discuter avec des membres du
laboratoire ou de taper `module help python/3.11.2`, qui affiche un texte expliquant ce
qu'il y a à faire en cas de problème avec le module, i.e. lire
<https://fluidhowto.readthedocs.io/python> et potentiellement écrire un message sur
l'issue tracker de
<https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/softsync-python-debian12>.

Pour d'autres besoins des utilisateurs (installation de paquets particuliers,
utilisations de versions et/ou d'implémentations de Python particulières), d'autres
méthodes que les modules doivent être utilisées (environnements virtuels, conda-forge,
etc.). J'ai écrit un petit site expliquant différentes solutions pour différents besoins
: <https://fluidhowto.readthedocs.io/python>. Le site
[Fluidhowto](https://fluidhowto.readthedocs.io) est il me semble utile bien au delà du
LEGI.

Pour conclure, les modules Python sont un bon outil pour un utilisation très simple et
rapide sans installation de paquets. Ils permettent notamment à un stagiaire d'utiliser
Python et les paquets utiles à l'étude de la mécanique des fluides sans perdre de temps
avec la gestion de Python. Nous avons notablement amélioré la méthode de création de ces
modules et le résultat. Il faut bien noter que ces modules sont maintenant immutables,
dans le sens où l'utilisateurs ne peut pas installer ou désinstaller de paquets. Il est
aussi intéressant de voir que ces modules et cette méthode sont facilement réutilisables
dans d'autres laboratoires/institutions/entreprises.

Une dernière petite remarque plus personnelle : je pense ce type de travail de fond utile
à mon labo et à la communauté. Malheureusement, je réalise aussi que c'est objectivement
une perte de temps pour moi et que j'aurais bien plus intérêt à ne pas m'investir dans ce
type de projets.

[pdm]: https://pdm-project.org
