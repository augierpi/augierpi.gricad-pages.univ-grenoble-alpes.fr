---
Title: Presentation on Fluidsimfoam at LEGI
Date: 2024-01-22
Modified: 2024-01-22
Category: Talks
summary: |-
  I will present our new Python package Fluidsimfoam in my lab.
---

I will present our new Python package [Fluidsimfoam] in my lab. My slides are available
[here](./docs/2024/fluidsimfoam-legi/pres.html).

[fluidsimfoam]: https://fluidsimfoam.readthedocs.io
