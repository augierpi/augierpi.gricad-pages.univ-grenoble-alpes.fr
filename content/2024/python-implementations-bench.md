---
Title: 'Notes on Python evolutions: mono or multi implementations? Benchmarks?'
Date: 2024-11-18
Modified: 2024-11-18
Status: draft
Category: Numerics
summary: |-
  Python is changing. Where does it go?
---

Python ecosystem is highly dominated by its main reference implementation CPython, in
particular for data and computational science. However, there are other very interesting
implementations, for examples:

- [micro-Python]: "An implementation of Python that includes a small subset of the Python
  standard library and is optimised to run on microcontrollers and in constrained
  environments."

- [PyPy]: "A fast, compliant alternative implementation of Python", written in Python.

- [GraalPy]: A high-performance implementation of Python for the JVM (Java Virtual
  Machine) built on [GraalVM].

In this note, I'm going to focus on the two later, since they both target high
performance and could be used for scientific/data Python.

With investments on CPython by big companies like Microsoft (financing the Faster CPython
project) and Meta (financing a team working on Meta and the free-threaded mode, PEP 703),
CPython is improving and will continue to improve in the next few years.

CPython 3.13 (https://docs.python.org/3/whatsnew/3.13.html) has "an experimental support
for running in a free-threaded mode ([PEP 703](https://peps.python.org/pep-0703)), and a
Just-In-Time compiler ([PEP 744](https://peps.python.org/pep-0744))"

CPython with its free-threaded mode and its new JIT could become in the next few years
even more dominating that what it is now.

Already the ecosystem has become less friendly for alternative implementations with the
[sunsetting of building packages for PyPy with conda-forge](https://pypy.org/posts/2024/08/conda-forge-proposes-dropping-support-for-pypy.html).

...

I think that we have an issue about the communication on the performance comparison
between different Python implementations, which is bad for the global understanding of
the community that users would benefit to have an ecosystem more open to alternative
implementations. First, there are different websites for the benchmarks of the different
implementations with (slightly) different benchmarks, methodologies and bases for
comparison:

- https://pypy.org/ and https://speed.pypy.org/ for PyPy (based on PyPy benchmarks,
  compatible with Python 2.7)
- https://github.com/faster-cpython/benchmarking-public for CPython (based on
  https://github.com/python/pyperformance)
- A figure on https://github.com/oracle/graalpython for GraalPy (based on
  https://github.com/python/pyperformance)
- https://speed.python.org/ (not up-to-date, complicated and not very readable figures)

Second, it is very difficult with what we have to go beyond comparisons with 1 number "X
is N.NN faster than Y", especially for people that do not know the details of
pyperformance benchmarks. It would be much better to be able to present something much
more quantitative with few numbers associated with groups of benchmarks for different
types of applications (for example, Pure Python, numerics with C extensions, startup, web
servers, ...). For some cases, it would also be useful to have simple comparisons with
other technologies (based on Python or on another language).

...

[graalpy]: https://github.com/oracle/graalpython
[graalvm]: https://www.graalvm.org/
[micro-python]: https://micropython.org/
[pypy]: https://pypy.org/
