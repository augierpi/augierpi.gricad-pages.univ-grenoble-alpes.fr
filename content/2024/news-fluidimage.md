---
Title: A much better Fluidimage
Date: 2024-04-03
Modified: 2024-04-03
Category: Projects
summary: |-
  Fluidimage reached a satisfactory state.
---

I recently dedicated a lot of my time (February to May 2024) in working on Fluidimage.
The [release notes page](https://fluidimage.readthedocs.io) lists the different changes
and improvements.
