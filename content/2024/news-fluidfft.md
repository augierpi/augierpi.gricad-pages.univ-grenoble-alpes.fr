---
Title: New Fluidfft with plugins
Date: 2024-04-02
Modified: 2024-04-02
Category: Projects
summary: |-
  Fluidfft is now modular and works with plugins.
---

In January and February I worked on a new version of Fluidfft labelled 0.4.0 (released in
2024-02-11).

For this version, we setup what I described in the previous post on
[Fluiddyn's news]({filename}./news-fluiddyn.md).

- Build and upload wheels on PyPI with Github Actions!

- Much better CI in foss.heptapod.net and Github Actions.

- Use the [Meson build system](https://mesonbuild.com) via
  [meson-python](https://github.com/mesonbuild/meson-python).

- Development: use PDM, Nox and Pixi.

Moreover, we reorganized the package with a new modular architecture using plugins:

- Configuration file deprecated.
- New functions `fluidfft.get_plugins` and `fluidfft.get_methods`.
- Base C++ code in `fluidfft-builder`
- [Plugins](#plugins) `f"fluidfft-{s}"` for `s` in `fftw`, `mpi_with_fftw`, `fftwmpi`,
  `pfft` and `p3dfft`.
