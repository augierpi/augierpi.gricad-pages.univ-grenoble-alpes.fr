---
Title: Article on internal gravity wave turbulence published in PRF
Date: 2024-02-24
Modified: 2024-02-24
Category: Articles
summary: |-
  Our paper describing a study on the internal gravity wave turbulence
  has been published in the journal Physical Review Fluids.
---

Good news, our article "Internal gravity waves in stratified flows with and without
vortical modes" by Vincent Labarre, Giorgio Krstulovic, Sergey Nazarenko and myself has
been published in the journal Physical Review Fluids.

- DOI: <https://doi.org/10.1103/PhysRevFluids.9.024604>
- HAL: <https://hal.science/hal-04793229>
- PDF on ArXiv: <https://arxiv.org/pdf/2303.01570>

**Abstract:**

The comprehension of stratified flows is important for geophysical and astrophysical
applications. The weak wave turbulence theory aims to provide a statistical description
of internal gravity waves propagating in the bulk of such flows. However, internal
gravity waves are usually perturbed by other structures present in stratified flow,
namely the shear modes and the vortical modes. In order to check whether a weak internal
gravity wave turbulence regime can occur, we perform direct numerical simulations of
stratified turbulence without shear modes and with or without vortical modes at various
Froude and buoyancy Reynolds numbers. We observe that removing vortical modes naturally
helps to have a better overall balance between poloidal kinetic energy, involved in
internal gravity waves, and potential energy. However, conversion between kinetic energy
and potential energy does not necessarily show fluctuations around zero in our
simulations, as we would expect for a system of weak waves. A spatiotemporal analysis
reveals that removing vortical modes helps to concentrate the energy around the wave
frequency, but it is not enough to observe a weak wave turbulence regime. Yet we observe
that internal gravity waves whose frequency are large compared to the eddy turnover time
are present, and we also find evidences for slow internal gravity waves interacting by
triadic resonance instabilities in our strongly stratified flows simulations. Finally, we
propose conditions that should be fulfilled in order to observe a weak internal gravity
wave turbulence regime in real flows.
