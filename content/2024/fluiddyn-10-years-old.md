---
Title: 'Fluiddyn: a decade of open-source development in fluid dynamics'
Date: 2024-04-04
Modified: 2024-04-04
Category: Projects
summary: |-
  Let's present it, see what have been built and how it can be useful for people.
---

# .

The [Fluiddyn project] was started approximately ten years ago! It is therefore a good
time to look at what we built and evaluate the state of the project.

I would like to present Fluiddyn during two GDR meetings in Nantes and Grenoble.

## Abstract

This presentation assesses the evolution of Fluiddyn, a project initiated a decade ago to
advance research in fluid dynamics through open-source methodologies. The objective was
to develop a suite of open-source Python packages to facilitate both personal research
and broader community engagement.

Several specialized packages have emerged from this initiative, comprising foundational
tools such as the core Fluiddyn package, Transonic for Python code acceleration, and
Fluidfft for FFT operations. Additional packages cater to diverse needs, including
numerical simulations (Fluidsim, Fluidsim-core, Snek5000, Fluidsimfoam), laboratory
experiment control (Fluidlab), and image processing (Fluidimage). We will present how
these packages could serve as valuable tools for researchers in fluid dynamics.

Technical advancements have been substantial, with Fluiddyn packages demonstrating robust
software engineering principles and expanded functionality. However, these achievements
have been accompanied by significant personal investment, raising concerns about
sustainability and community engagement.

Despite technical successes, the project's impact remains constrained by limited
community involvement. Efforts to cultivate a user and contributor base have yet to yield
the desired collective effect.

This presentation offers reflections on the journey of Fluiddyn, highlighting
achievements, challenges, and prospects for future growth and community engagement.

## Fluiddyn, Python packages for the study of fluid dynamics

Let us first recall what is Fluiddyn. The project is to foster open-source for the study
of fluid dynamics by building few Python packages specialized in different tasks, in
particular simulations with Fluidsim, image processing with Fluidimage and control of
laboratory experiments with Fluidlab.

We also built two more fundamental Python libraries to (i) accelerate Python code with
Transonic and (ii) performed FFT in parallel with Fluidfft. Fluidfft is itself organized
in a base package and few plugins to perform FFT with different methods and libraries.

In Fluidsim, we define the concept of Fluidsim solver, which are Python packages to
handle a set of potential similar simulations (both in terms of numerical methods and
equations solved). Fluidsim is organized in a base package containing common code
(fluidsim-core) and the main package (Fluidsim) where some specific solvers are
described, in particular using Fluidfft. Beside, we developed two packages to defined
Fluidsim solvers with popular CFD programs, Snek5000 for Nek5000 and FLuidsimfoam for
OpenFOAM.

## Assessment after 10 years

The global assessment is contrasted. On the one side, few good quality packages reaching
technical maturity. One the other side, tiny user base and nearly no open-source
dynamics. Let us look at that in more details.

### Positive outcomes

#### Few useful packages

- Transonic

- Fluiddyn

- Fluidsim

- Snek5000 and Fluidsimfoam

- Fluidfft

- Fluidimage

- Fluidlab

- Formattex and Formatbibtex

#### Used of Python: good technological choice

We based our development on the scientific Python ecosystem. This was not a evident
choice at the time we started Fluiddyn. Python was not so popular, in particular in the
field of fluid mechanics, where Matlab was much more used. Students did not study Python
and the Python scientific ecosystem was not as mature as nowadays. The evolution of the
situation of Python for sciences is a chance for Fluiddyn.

### Mercurial: a questionable but interesting technological choice

Ten years ago, Git was not yet so popular and we choose to use Mercurial for its
simplicity. Nowadays, a open-source code used for science has to be on Github to have a
chance to be used and Git became the standard for versioning.

We did not switch from Mercurial to Git (like PyPy) and found a satisfactory solution
with https://foss.heptapod.net/ and mirrors on Github. Therefore, we are among one of the
few projects which continue to resist to the Github monopoly. This situation is
interesting in itself but it could curb community engagement.

### Issues

Besides these interesting achievements, we have to admit that this project failed to
gather external users and developers. Hence, there is no open-source dynamics which has a
huge impact on the quality of the packages. Since the open-source aspect was so central
for the project, this failure is global and clearly put it in danger. The bus factor is
one for all the Fluiddyn packages.

The only package which starts to gather a small open-source community is fluidfoam, which
is lead by colleagues at LEGI. Fluidfoam get some inspiration from Fluiddyn project and
it's nice to see that it is somehow successful.

Anyway, we will have to seriously analyse the reasons of this failure to have a chance to
get better results.

[fluiddyn project]: https://fluiddyn.readthedocs.io
