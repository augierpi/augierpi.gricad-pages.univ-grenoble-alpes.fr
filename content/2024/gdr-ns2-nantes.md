---
Title: Participation to GDR Navier-Stokes 2.0 in Nantes
Date: 2024-06-11
Modified: 2024-06-11
Category: Talks
summary: |-
  I gave a presentation on Fluiddyn project at the GDR Navier-Stokes 2.0.
---

I gave a presentation on Fluiddyn project at a
[meeting of the GDR Navier-Stokes 2.0](https://gdr-turbulence.universite-lyon.fr/reunion-2024-du-gdr-ns2-00-337055.kjsp).

The slides are available [here](./docs/2024/2024-06-11_pres_fluiddyn_gdr.pdf).
