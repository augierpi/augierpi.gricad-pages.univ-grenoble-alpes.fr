---
Title: Python 3.13 and related work
Date: 2024-11-12
Modified: 2024-11-12
Category: Numerics
summary: |-
  Python 3.13 is released: exciting news... and work needed to be able to use it.
---

# .

## New CPython version with exciting news for long term

Python 3.13.0 was released on October 7, 2024 and will be officially supported until
October 2029. The full article presenting the new features is
[here](https://docs.python.org/3/whatsnew/3.13.html). This version contains very exciting
foundational changes which could have positive impacts for users in few years, namely
"experimental support for running in a free-threaded mode
([PEP 703](https://peps.python.org/pep-0703)), and a Just-In-Time compiler
([PEP 744](https://peps.python.org/pep-0744))".

However, in 3.13, the effect of the new JIT is yet very small (see
<https://github.com/faster-cpython/benchmarking-public>). There are plans to improve the
JIT for CPython 3.14 and 3.15 (<https://github.com/faster-cpython/ideas/issues/701>) so
we will see in 2025 and 2026.

Note that we can expect that PyPy and GraalPy will stay much faster Python interpreters
than CPython so that the scientific Python ecosystem would have interest to also invest
on [HPy].

The free-threaded mode is about getting rid of the infamous "GIL" (Global Interpreter
Lock). [PEP 703](https://peps.python.org/pep-0703) describes how impacting it could be.
Beware that using it in practice will require adaptation and new builds of most packages,
so this is not ready yet for standard users. The website
<https://py-free-threading.github.io/> provides some information on how to use the
free-threaded mode and a
[compatibility status tracking](https://py-free-threading.github.io/tracking/).

Note that the free-threaded mode and the CPython JIT are the results of investments of
Meta (Facebook, Pytorch) and Microsoft in CPython. It will be interesting the see how it
will impact Python users and Python global ecosystem in the next few years.

## New versions to support Python 3.13

After a new Python release and since we do not use HPy, wheels and conda packages have to
be built for the new version even if the package do not need changes to support the new
Python version. I worked on new version of pyFFTW (0.15), fluidimage (0.5.4), fluidfft
(0.4.3) and fluidsim (0.8.4). Plus a lot of conda-forge builds!

Currently (2024-11-12), fluidsim can be used with Python 3.13! In contrast, scikit-image
and opencv are not yet ready (see
<https://conda-forge.org/status/migration/?name=python313>) so we have to wait for
fluidimage.

[hpy]: https://github.com/hpyproject/hpy
