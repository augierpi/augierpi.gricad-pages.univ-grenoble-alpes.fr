---
Title: Meson and MPI
Date: 2024-11-11
Modified: 2024-11-11
Category: Numerics
summary: |-
  Meson is a nice build system but it did not support well MPI
---

Meson is a nice build system used by many packages of the scientific Python ecosystem
(Numpy, Scipy, scikit-image, ...) and by few Fluiddyn packages (fluidimage, fluidfft,
fluidsim and other fluidfft plugins).

Unfortunately, it is not yet used a lot for software linking with MPI library and it was
actually very weak for MPI detection.

So Meson did not support using MPICH or Intel MPI with GCC! Unfortunately, this is a
common setup on some supercomputers, which should be used for example for Adastra (CINES,
the most powerfull supercomputer in France in 2024).

I had to work on this subject in this pull request
<https://github.com/mesonbuild/meson/pull/13619> which solved the following issues:

- <https://github.com/mesonbuild/meson/issues/7045>
- <https://github.com/mesonbuild/meson/issues/9637>
- <https://github.com/mesonbuild/meson/issues/13615>

I started to work on this subject on the end of August and it was merged on the end of
September. Meson 1.6 was finally released on 20/10/2014. I activated builds with MPICH
for `fluidfft-mpi_with_fftw` and `fluidfft-fftwmpi` and... it worked!

One can now in less than 1 minute install with conda-forge fluidsim with fluidfft plugins
supporting MPI linked with MPICH with:

```sh
conda create -n env-fluidsim-mpich fluidsim mpich fluidfft-mpi_with_fftw fluidfft-fftwmpi
```
