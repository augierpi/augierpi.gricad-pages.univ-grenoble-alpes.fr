---
Title: Work on pyFFTW leading to new version 0.14.0
Date: 2024-07-11
Modified: 2024-07-11
Category: Numerics
summary: |-
  We need pyFFTW in a good shape so I invest some time in this project.
---

pyFFTW is a pythonic wrapper around [FFTW], a very popular Fast Fourier Transform
library.

pyFFTW is used for several applications and in particular in packages of the [Fluiddyn]
project. For example, pyFFTW is used by Fluidsim for the pseudo-spectral algorithm and by
Fluidimage to compute image correlation.

Unfortunately, these last months, not enough work was put in pyFFTW and it started to
lead to issues. Some broken conda packages were available. Python 3.12 was not supported
9 months after its release. There was no wheel for MacOS ARM. Moreover, pyFFTW was
incompatible with Cython 3 and Numpy 2.0. Finally, the CI on Github Actions was broken so
it became even impossible to improve pyFFTW code.

I waited for a long time seeing this situation but nobody was available to fix these
issues. Finally, I decided to invest some time to fix pyFFTW. After
[few pull requests](https://github.com/pyFFTW/pyFFTW/pulls?q=is%3Apr+is%3Aclosed+author%3Apaugier),
I think the situation is now much better. pyFFTW 0.14.0 was released in July 2024 with
good compatibility with recent software and hardware.

Due to this work and to the need of a new maintainer for pyFFTW, I became officially one
of the maintainer of this project.

I need to mention that this new responsability is twofold. On the one hand, it is nice to
be able to help the community and it represents a recognition of helpful and useful work
spent on the project. On the other hand, this is a real long term responsability which
won't be rewarded by anyone and in particular by my employer, the CNRS.

Of course, this is not particular to me or this project pyFFTW. The open-source
ecosystems depend on several projects maintained by volonters. This is a global political
issue. Open-source ecosystems are used by companies and public institutions. They are in
some ways useful for humanity. We should invent robust mechanisms to support important
open-source projects and their maintainers.

[fftw]: https://www.fftw.org/
[fluiddyn]: https://fluiddyn.readthedocs.io
