---
Title: Issues on pip install --no-binary
Date: 2024-11-21
Modified: 2024-11-21
Category: Numerics
summary: |-
  I detected a new problem with the pip option `--no-binary` and created few issues on pip issue tracker.
---

While working on installation of Fluidsim on a cluster, I detected a new problem with the
pip option `--no-binary`, which is commonly used to require a new build from source of a
package. For example, one can run

```sh
pip install mpi4py --no-binary mpi4py
```

I created a first issue on pip issue tracker
([pypa/pip#12954](https://github.com/pypa/pip/issues/12954)). After some discussion it
was understood that it was not a proper bug but actually a breaking change documented in
the release notes of pip 23.1 (<https://pip.pypa.io/en/stable/news/#v23-1>). To achieve
the wanted result, one now needs to run commands like:

```sh
pip cache remove mpi4py; pip install mpi4py --no-binary mpi4py
```

I created four distinct and tractable issues about this problem:

- [pypa/pip#13087](https://github.com/pypa/pip/issues/): Doc for option --no-binary is
  wrong
- [pypa/pip#13086](https://github.com/pypa/pip/issues/): pip cache remove should also
  work with a package name
- [pypa/pip#13090](https://github.com/pypa/pip/issues/): Rename --no-binary as
  --no-download-binary so that --no-binary can emit a warning
- [pypa/pip#13088](https://github.com/pypa/pip/issues/): Add new option --rebuild (or
  --never-binary) to the install command
