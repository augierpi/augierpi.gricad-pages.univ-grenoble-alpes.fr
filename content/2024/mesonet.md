---
Title: MesoNET for teaching HPC
Date: 2024-11-19
Modified: 2024-11-19
Status: draft
Category: Numerics
summary: |-
  MesoNET is ...
---

[MesoNET] est une fédération de mésocentres de calcul. Les mésocentres sont des
structures publiques (souvent des Unités Mixte de Service) dédiées à fournir des moyens
de calculs tels que des clusters de calcul.

Je prépare un cours/TP et une école d'été basés sur un cluster (Zen) de cette plateforme.

- https://master-tma.gricad-pages.univ-grenoble-alpes.fr/num-methods-turb/part0/mesonet.html

- https://extrem-gs.gricad-pages.univ-grenoble-alpes.fr/school-num/intro.html

[mesonet]: https://www.mesonet.fr/
