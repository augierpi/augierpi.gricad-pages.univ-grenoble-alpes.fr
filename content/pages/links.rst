Links
=====

:date: 2015-01-24
:modified: 2024-12-09

Fluiddyn
--------

- `Fluiddyn project <https://fluiddyn.readthedocs.io>`_

- `Fluidhowto website <https://fluidhowto.readthedocs.io>`_

Research
--------

- `CNRS <http://www.cnrs.fr>`_

- `Université Grenoble Alpes <http://www.univ-grenoble-alpes.fr/>`_

- My laboratory: `LEGI <http://www.legi.grenoble-inp.fr>`_

- `The Coriolis platform
  <http://www.legi.grenoble-inp.fr/web/spip.php?article757>`_

Other places where I worked
---------------------------

- `LadHyX <http://www.ladhyx.polytechnique.fr/>`_

- `KTH <http://www.mech.kth.se/mech/>`_

- `DAMTP <http://www.damtp.cam.ac.uk/>`_

Open-source
-----------

I would like to thank and promote open-source organizations and projects:

- `Python.org <http://python.org>`_, `Scipy <http://www.scipy.org>`_, `Pythran
  <https://github.com/serge-sans-paille/pythran>`_, ...

- `The PySciDataGre group <https://python.univ-grenoble-alpes.fr/>`_

- `The Free Software Foundation (FSF) <http://www.fsf.org>`_ and `GNU
  <https://www.gnu.org>`_

- `Debian <https://www.debian.org/>`_,
  `Linux Mint Debian Edition <https://www.linuxmint.com/download_lmde.php>`_
  and `Ubuntu <https://www.ubuntu.com/>`_

- `Pelican <http://getpelican.com/>`_ (the program used to make this site)

Useful
------

- `Weather forecast Grenoble by Caplain <http://mto38.free.fr/>`_
