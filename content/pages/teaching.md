---
Title: Teaching
Date: 2015-01-24
Modified: 2022-09-26
---

Some of my activities in teaching:

## Fluid Mechanics

- Course *Instabilities and turbulence* (see the
  [sources](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence)
  and the
  [website](https://augierpi.gricad-pages.univ-grenoble-alpes.fr/coursem1_pa_instabilities_turbulence/)),
  [Master 1 Applied Mechanics](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-mecanique-IAQK579W/parcours-applied-mechanics-1re-annee-KVQRVL29.html)

- Course *Introduction to scientific computing* (see the
  [sources](https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm)
  and
  [website](https://meige-legi.gricad-pages.univ-grenoble-alpes.fr/scientific-computing-m2-efm))
  [Master 2 EFM](http://master-efm.legi.grenoble-inp.fr/).

- Course *Méthodes numériques avancées pour la turbulence* (see the
  [sources](https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma/num-methods-turb) and
  [website](https://master-tma.gricad-pages.univ-grenoble-alpes.fr/num-methods-turb))
  [Master 2 TMA](https://brunch.gricad-pages.univ-grenoble-alpes.fr/master-tma-turbulences-methodes-et-applications)

- Course *Mécanique des fluides 2* at
  [IUT GTE Grenoble](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/but-bachelor-universitaire-de-technologie-CBB/but-genie-thermique-et-energie-KI4YHUKA.html)
  ([formula](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/docs/ipynb/20190610_mecaflu2_iut_gte/pres.slides.html)).

- Project with students of ENSE3
  ([general presentation of the course](http://ense3.grenoble-inp.fr/cursus-ingenieurs/team-project-toutes-fili-egrave-res-m1-sgb-m1-hce-4eus4pro-647177.kjsp)
  and
  [what the team of students working with me produced in 2017 on Coriofoam](http://coriofoam.readthedocs.io/en/latest/))

- Travaux pratiques de Mécanique des fluides en licence 2.

## Development and High Performance Computing

- [Python training at UGA](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017)

- [HPC with Python training at UGA](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc)

- Summer school of the gradual school Extrème
  ([sources](https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num) and
  [website](https://extrem-gs.gricad-pages.univ-grenoble-alpes.fr/school-num/intro.html))
