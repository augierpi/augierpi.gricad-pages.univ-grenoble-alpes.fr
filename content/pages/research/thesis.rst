PhD thesis
==========

:date: 2015-01-24
:modified: 2015-01-24
:URL: pages/research/thesis.html
:Save_as: pages/research/thesis.html


From 2008 to 2011, I was a PhD student under the supervision of Paul Billant
and Jean-Marc Chomaz at the LadHyX, the hydrodynamical lab of Ecole
Polytechnique (France). You can download my `PhD manuscript
<{static}/docs/P.Augier.PhD.pdf>`__ (it should also be `here
<http://tel.archives-ouvertes.fr/tel-00697245>`__), defended November 21th 2011
(Ecole Polytechnique PhD thesis award 2012).

.. figure:: {static}/images/photo_exp_these.jpg
   :width: 50 %
   :alt: an experiment
   :align: right

   Top view of the experiment and PIV measurements.

During my thesis, dealing with turbulence in stratified fluids, we
have characterized the statistics and dynamics of disordered and fully
turbulent stratified flows. We have also studied the transition to
turbulence of a simple flow in order to better understand the basic
physical mechanisms involved in stratified turbulence.


Studies of stratified turbulence forced with columnar dipoles
-------------------------------------------------------------

A novel experiment of forced strongly stratified turbulence
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We have set-up a novel experiment of continuously forced, stratified
disordered flows.  This set-up differs from previous experiments of
stratified turbulence since the forcing consists in vertically
invariant columnar vortex pairs generated intermittently.  After an
initial spontaneous three-dimensionalisation of the flow, a
statistically stationary disordered state is reached.

For weak forcing, the horizontal flows corresponding to different
layers are quasi two-dimensional and the structures are smooth
suggesting that these experiments are in the viscosity affected
stratified regime.  In contrast, for strong forcing, we observe small
scale structures superimposed on the large scale horizontal layers,
indicating that these experiments approach the strongly stratified
turbulent regime.

Numerical study of strongly stratified turbulence
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The experimental results have been supported and extended by numerical
simulations.  The flow is forced similarly like in the experiments
with coherent columnar dipoles.  For the experimental parameters,
numerical and experimental results are in good agreements.  In
particular, we observe when the buoyancy Reynolds number is increased
a similar transition between a viscously affected regime and a
strongly stratified nearly turbulent regime with overturning at small
scales.  When the buoyancy Reynolds number is further increased,
strongly stratified turbulence is obtained.

Secondary instabilities on the zigzag instability
-------------------------------------------------

.. figure:: {static}/images/transition_turb_zigzag.jpg
   :width: 50 %
   :alt: an experiment
   :align: right

   Iso-vorticity surfaces.

Onset of secondary instabilities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I have investigated by means of numerical simulations the dynamics at
high Reynolds number of two counter-rotating vortices in a stratified
fluid. We have demonstrated that two types of secondary instabilities
develop simultaneously but in different regions of the dipole: a shear
instability develops in the most bent region of the vortex cores
whereas a gravitational instability develops in the middle between the
two vortices.

Spectral analysis of the transition to turbulence
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This transition to turbulence has been further investigated by a
spectral analysis.  We have shown that the characteristic length scale
of the Kelvin-Helmholtz billows is of the order of the buoyancy scale.
Both instabilities lead to a transition to turbulence which exhibits
anisotropic spectra similar to those associated to fully developed
strongly stratified turbulence.  For the first time, a return to
isotropy is observed for scales smaller than the Ozmidov length scale.


Kolmogorov laws for stratified turbulence
-----------------------------------------

Following the Kolmogorov technique, an exact relation for a vector
third-order moment J is derived for three-dimensional incompressible
stably stratified turbulence under the Boussinesq approximation.  An
integration of the exact relation under this hypothesis leads to a
generalized Kolmogorov law which depends on the intensity of
anisotropy parameterized by a single coefficient. By using a scaling
relation between large horizontal and vertical length scales we fix
this coefficient and propose a unique law.
