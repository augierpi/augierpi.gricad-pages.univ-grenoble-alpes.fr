FluidDyn project
================

:date: 2015-10-01
:modified: 2015-10-01

.. image:: ../images/logo-fluiddyn.jpg
   :width: 49%
   :alt: FluidDyn logo.
   :align: left
.. image:: https://www.python.org/static/opengraph-icon-200x200.png
   :width: 30%
   :alt: Python
   :align: right

Open-source programs and methods play an important role in my
work. Unfortunately, the community of fluid dynamics uses these tools only
marginally. I launched a project called FluidDyn to foster open-source coding
and open-science in fluid dynamics research and teaching.

Right now, FluidDyn is mainly a set of Python packages that I used in
my research, in particular

- the base package `fluiddyn
  <http://fluiddyn.readthedocs.org>`_,

- a package to make Python code run very fast (now, using Pythran) `transonic
  <http://transonic.readthedocs.org>`_,

- a package to do experiments
  `fluidlab <http://fluidlab.readthedocs.org>`_,

- a package to do numerical simulations `fluidsim
  <http://fluidsim.readthedocs.org>`_,

- a package to compute FFT `fluidfft <http://fluidfft.readthedocs.org>`_,

- a package to analyze images of fluids `fluidimage
  <http://fluidimage.readthedocs.org>`_,

- a package for the Coriolis platform `fluidcoriolis
  <http://fluidcoriolis.readthedocs.org>`_.

We try to gather information on how to use computers in the
website `Fluidhowto <https://fluidhowto.readthedocs.io>`_.

The main developers of these packages are Cyrille Bonamy, Antoine Campagne,
Julien Salort (ENS Lyon), Ashwin Vishnu Mohanan (KTH, Stockholm) and me.

Three companion metapapers on three FluidDyn packages have been accepted in the
`Journal of Open Research Software (JORS)
<https://openresearchsoftware.metajnl.com/>`_:

1. `FluidDyn <http://fluiddyn.readthedocs.io/>`__: a python open-source
   framework for research and teaching in fluid dynamics by simulations,
   experiments and data processing (`pdf
   <{static}/docs/fluiddyn_metapaper.pdf>`__)

2. `FluidFFT <http://fluidfft.readthedocs.io/>`__: common API (C++ and Python) for
   Fast Fourier Transform HPC libraries (`pdf
   <{static}/docs/fluidfft_paper.pdf>`__)

3. `FluidSim <http://fluidsim.readthedocs.io/>`__: modular, object-oriented Python
   package for high-performance CFD simulations (`pdf
   <{static}/docs/fluidsim_paper.pdf>`__)

The source code of the papers is in `this repository
<https://foss.heptapod.net/fluiddyn/fluiddyn_paper>`_.
