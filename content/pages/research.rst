Research
========

:date: 2015-01-24
:modified: 2015-01-24

My main research interests concern fluid dynamics and turbulence,
mainly applied to geophysical flows. In my work, a combination of lab
experiments, numerical calculations, theoratical derivations and data
analysis are used in order to investigate instabilities and turbulence
in stratified and rotating fluids.

I am specialist in the dynamics of flows influenced by a stable density
stratification and rotation. I did a `PhD at LadHyX on turbulence and
instabilities in stratified fluids
<|filename|/pages/research/thesis.rst>`_. Now, I continue to work on this
subject in particular with experiments in the `Coriolis platform
<http://www.legi.grenoble-inp.fr/web/spip.php?article757>`_. For
example, I was co-manager with Erik Lindborg of a `big project called MILESTONE
<{filename}/2016/milestone.rst>`_ of experiments of stratified and
rotating grid turbulence.

I also worked at KTH (Stockholm, Sweden) on the analyze of General Circulation
Models (GCM).

I also worked on simulations of simple 2D models (like the shallow water
model).

I also worked on a stratified Taylor-Couette experiment in the project `MUST
<http://damtp.cam.ac.uk/research/env/must>`_ at DAMPT (Cambrige, UK)

I also work to promote open-science and open-source with Python in particular
with the project `FluidDyn <|filename|/pages/fluiddyn.rst>`_

For more information about my work, have a look at my `news <../archives.html>`_.
