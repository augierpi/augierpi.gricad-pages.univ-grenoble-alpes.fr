CV
==

:date: 2015-01-24
:modified: 2015-01-24


Here, you can download my `CV in pdf <{static}/docs/cv_PierreAugier.pdf>`_.

Education
---------

:2008-2011: Thèse de Doctorat au Laboratoire d'Hydrodynamique de
            l'Ecole Polytechnique (LadHyX)

:2003-2008: Elève fonctionnaire à l'Ecole Normale Supérieure de Lyon
            (ENS Lyon)

:2008: Master 2 de Physique, ENS Lyon, option Physique hors équilibre

:2007: Agrégation de sciences physiques, option Physique

:2004: Licence de sciences de la matière, ENS Lyon, option Physique

:2001-2003: DEUG de sciences de la matière, Université de La Rochelle,
  option Physique

:2001: Baccalauréat, Lycée Valin, La Rochelle, Série S

|

Research experience
-------------------

:2013-2014: Post-doctorat au DAMTP avec Paul Linden, Colm-cille
  Caulfield, Stuart Dalziel sur le projet `Mathematical underpinnings
  of stratified turbulence <http://damtp.cam.ac.uk/research/env/must/>`_.

:2011-2013: Post-doctorat dans l'équipe turbulence géophysique
  d'Erik Lindborg, KTH, Department of Mechanics
  (Stockholm). Traitement de données de Modèles de Circulation Globale
  (GCM) haute résolution. Bilan spectral en énergie et enstrophie, et
  détection d'ondes internes.

:2008-2011: Thèse sous la direction de Paul Billant et Jean-Marc
  Chomaz, LadHyX, Ecole Polytechnique. Turbulence en milieu stratifié,
  étude des mécanismes de la cascade.

:2006: Stage de 6 mois sous la direction de Jean-Francois Pinton,
  Laboratoire de Physique de l'ENS Lyon. Dynamo Bullard-von Kármán
  gallium, une dynamo fluide à mouvement non-contraint et turbulent.

:2005: Stage de 3 mois sous la direction de Stephan Llewellyn Smith,
  University of California San Diego (UCSD). Conversion of the
  barotropic tide over a three dimensional steep ridge.

:2004: Stage de 2 mois sous la direction de Anne Davaille, Institut de
  Physique du Globe de Paris (IPGP). Etude expérimentale de la
  convection thermique à deux couches dans les fluides visqueux.
