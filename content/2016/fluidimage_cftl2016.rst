FluidImage at Toulouse, CFTL 2016
=================================

:date: 2016-09-13
:modified: 2016-11-04
:tags: Open, Python, fluiddyn, fluidimage
:category: Talks

A first version of `FluidImage <https://foss.heptapod.net/fluiddyn/fluidimage>`_ is
working. Everyone can use it to compute PIV fields from images.

I attended a conference in Toulouse (Congrès Francophone de Techniques Laser,
CFTL) to present this new software developed by Cyrille Bonamy, Antoine
Campagne, Ashwin Vishnu and I.

My Poster can be downloaded `here in HAL
<https://hal.archives-ouvertes.fr/hal-01396688v1>`_.
