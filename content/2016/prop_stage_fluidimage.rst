Proposition de stage FluidImage
===============================

:date: 2016-01-21
:modified: 2015-01-21
:tags: stages
:category: Thesis-stages

Cyrille Bonamy (ingénieur calcul scientifique au LEGI) et moi proposons un
stage sur un projet sympa :

Développement d’une boite à outils libre de traitement d’images de fluide par GPU
---------------------------------------------------------------------------------

**Laboratoire** : LEGI, CNRS-UMR5519, UGA

**Localisation** : Grenoble (campus de Saint-Martin d’Hères)

**Profil recherché** : Ouvert, de préférence stage de fin d’étude Master 2 en
informatique

**Durée** : 4 à 6 mois

Nous souhaitons développer un environnement logiciel libre de traitement
asynchrone et distribué de flux d’images de fluide, structuré autour d’une
librairie, de traitements et d’une interface graphique. Un noyau
majoritairement en Python orienté objet est `en cours de développement
<https://foss.heptapod.net/fluiddyn/fluidimage>`_ en se basant sur des outils
libres (NumPy, SciPy, Scikit-image, PyQt pour l’interface graphique...) et sur
du code de haute qualité (test unitaire, documentation).  Les traitements
utiliseront des algorithmes éprouvés ou innovants, tels que PyCUDA pour les
calculs GPU. L’environnement de développement permettra la mise en place simple
de nouvelles méthodes ainsi qu’une haute scalabilité. Ainsi tous les
traitements pourront à terme, soit être appelé dans un notebook Python,
soit être chaînés via des pipes (type pipexec), soit être distribués sur un
cluster (par exemple STORM). Ce projet s’intègre ainsi dans une action
inter-équipes du laboratoire sur le traitement distribué de données
expérimentales, menée notamment au travers du projet STREAM (collaboration
LEGI-LIG). Le travail profitera rapidement à l’ensemble du laboratoire. En
particulier, les traitements pourront aussi être lancés de manière transparente
depuis la boite à outils Matlab UVMAT.

Le stage sera encadré par l’ingénieur calcul scientifique du LEGI (Cyrille
BONAMY) avec la participation de plusieurs chercheurs (dont Pierre AUGIER). Le
but de ce stage est de produire un environnement logiciel libre suffisamment
mature et souple pour (i) être utilisé pour des calculs simples, (ii) passer
à l’échelle et (iii) être investi en terme de développement participatif par la
communauté de mécanique des fluides.

Le ou la stagiaire pourra s’impliquer en particulier dans les calculs sur GPU
et clusters, la conception générale du code, la documentation, les tests
unitaires et sur la réalisation de l’interface graphique en Qt.

**Compétences souhaitées**

• Bases de programmation en Python
• Connaissances en programmation scientifique, HPC et/ou sur accélérateurs (GPU)
• Connaissances en traitement d’images appréciées

**Contacts**

• Cyrille.Bonamy [at] legi.cnrs.fr - 04.56.52.86.09
• Pierre.Augier [at] legi.cnrs.fr

`Le sujet en pdf
<{static}/docs/prop_stage_fluidimage_2016-01-21.pdf>`_.

N'hésitez pas à nous contacter pour plus de détails!
