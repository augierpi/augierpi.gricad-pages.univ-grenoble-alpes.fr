A new carriage in the Coriolis platform
=======================================

:date: 2016-06-30
:modified: 2016-11-04
:category: Methods

We built a new 4-m-wide carriage in the Coriolis platform. It is self
propelled and can move along its 13-m-long tracks.

.. image:: ./images/carriage/carriage_main.jpg
  :width: 80%
  :alt: Carriage in the Coriolis platform.
  :align: center

There was also a carriage in the old Coriolis platform but it was not kept
because it was not very satisfactory. We decided to build a new carriage for
the `Milestone project <https://www.euhit.org/projects/MILESTONE>`_ (financed
by `Euhit <https://www.euhit.org>`_). Such carriage is a particular piece. We
need to control its movement quite precisely and it has to be very soft and
regular, without vibration. The carriage has to laid on very smooth, flat and
horizontal tracks of 13 m but such carriage can not be very light. Therefore,
designing and building such big object is difficult.

We decided that we do most of the work internally at LEGI, with technicians and
researchers of the lab so we control everything and learn.  It was an
impressive work and I would like to thank a lot Julie Germinario, Samuel
Viboud, Thomas Valran, Mile Kusulja, Joseph Virone, Vincent Govart, Stéphane
Pioz-Marchand, Tristan Vandenberghe, Nicolas Mordant, Joel Sommeria, Antoine
Campagne and Rémi Chassagne. You can see the result in these photographies.

.. image:: ./images/carriage/carriage_fromfront.jpg
  :width: 49%
  :alt: Carriage from front.
  :align: center

.. image:: ./images/carriage/carriage_bottom.jpg
  :width: 49%
  :alt: Carriage from bottom.
  :align: center

Below, you can see a position sensor by cable, which gives the position of the
carriage with an accuracy of approximately 1 mm over 13 m!

.. image:: ./images/carriage/carriage_positionsensor.jpg
  :width: 49%
  :alt: Position sensor.
  :align: center
.. image:: ./images/carriage/carriage_cable.jpg
  :width: 49%
  :alt: Cable.
  :align: center

We spent approximately 17 k€ in total, including 9 k€ for the tracks and 2 k€
for the motor.

We decided to control the carriage with an open-source software that we
developed ourselves.  We control the motor (Unidrive SP, Leroy Somer)
by Modbus TCP and a position sensor (Micro-epsilon) which sends quadrature
signals read with a Labjack acquisition card.

We didn't use the close-source commercial program Labview and we develop
everything with Python. Our code is available and open-source (see the
repositories `fluidlab <https://foss.heptapod.net/fluiddyn/fluidlab>`_ and
`fluidcoriolis <https://foss.heptapod.net/fluiddyn/fluidcoriolis>`_).

Since we need to have a software easily usable by people coming from outside
LEGI to work with the Coriolis platform, we develop a nice graphical user
interface (GUI) with the Qt framework (Q designer and PyQt/Pyside).

Do not hesitate to contact me if you would like to do something similar with
Python and fluidlab.
