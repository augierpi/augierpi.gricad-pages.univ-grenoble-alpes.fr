Presentation on 2D internal wave turbulence at EFMC 12
======================================================

:date: 2018-09-10
:modified: 2018-09-10
:tags: fluiddyn, fluidsim
:category: Conference

`Miguel Calpe Linares
<http://www.legi.grenoble-inp.fr/web/spip.php?auteur328>`_ attended the 12th
European Fluid Mechanics Conference in Vienna and presented our work on
`Two-dimensional numerical simulations of stratified turbulence
<https://az659834.vo.msecnd.net/eventsairwesteuprod/production-interconvention-public/261f540cfc114d3698785094f6876176>`_

The simulations were performed with the Python CFD framework `FluidSim
<https://foss.heptapod.net/fluiddyn/fluidsim>`_.

You can check out the `pdf of his presentation
<{static}/docs/2018/efmc18_oral.pdf>`_.