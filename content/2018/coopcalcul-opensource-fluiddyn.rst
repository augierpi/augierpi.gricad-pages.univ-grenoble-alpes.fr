Présentation CoopCalcul LEGI open-source et FluidDyn
====================================================

:date: 2018-01-28
:modified: 2018-01-28
:tags: fluiddyn, fluidfft, fluidsim, fluidlab, fluidimage
:category: Talks

Je vais donner une présentation au CoopCalcul jeudi 1er février.

Titre
-----

Dynamique open-source et conséquences sur la recherche et l'enseignement en
mécanique des fluides

Résumé
------

Trois articles concernant le projet `FluidDyn
<http://fluiddyn.readthedocs.io/>`_ sont en cours d'écriture. Je voudrais
profiter d'un CoopCalcul pour présenter et discuter :

- une analyse sur des dynamiques actuelles en technologies informatiques et
  leurs implications pour la mécanique des fluides

- le projet d'open-science FluidDyn

.. image:: ./images/logo-fluiddyn.jpg
   :width: 49%
   :alt: FluidDyn logo.
   :align: left
.. image:: https://www.python.org/static/opengraph-icon-200x200.png
   :width: 30%
   :alt: Python
   :align: right

- une proposition (mon idée) pour rééquilibrer les financements communs du
  laboratoire entre les entreprises fournissant des codes propriétaires et les
  projets open-source utiles pour le laboratoire.

Et voilà les `slides de ma présentation
<{static}/docs/ipynb/20180201coopcalcul/presentation20180201coopcalcul.slides.html>`_!

Ma présentation se termine par `une grosse pub pour une journée "Python
scientique à Grenoble" le 8 mars
<{static}/docs/ipynb/20180201coopcalcul/presentation20180201coopcalcul.slides.html#/13>`_. organisée
pour le lancement d'un groupe "Python for science and data analysis in
Grenoble".
