Three FluidDyn articles submitted to JORS
=========================================

:date: 2018-07-03
:modified: 2019-02-27
:tags: fluiddyn, fluidfft, fluidsim, fluidlab, fluidimage
:category: Articles

The `FluidDyn project <http://fluiddyn.readthedocs.io/>`_ aims at promoting the
use of open-source Python software in research in fluid dynamics.

.. image:: ./images/logo-fluiddyn.jpg
   :width: 40%
   :alt: FluidDyn logo.
   :align: left
.. image:: https://www.python.org/static/opengraph-icon-200x200.png
   :width: 25%
   :alt: Python
   :align: right

We submitted to the `Journal of Open Research Software (JORS)
<https://openresearchsoftware.metajnl.com/>`_ (and in `arXiv
<https://arxiv.org>`_) three companion metapapers on FluidDyn packages:

1. `FluidDyn <http://fluiddyn.readthedocs.io/>`_: a Python open-source framework
   for research and teaching in fluid dynamics (`pdf
   <{static}/docs/fluiddyn_metapaper.pdf>`__)

2. `FluidFFT <http://fluidfft.readthedocs.io/>`_: common API (C++ and Python) for
   Fast Fourier Transform HPC libraries (`pdf
   <{static}/docs/fluidfft_paper.pdf>`__)

3. `FluidSim <http://fluidsim.readthedocs.io/>`_: modular, object-oriented Python
   package for high-performance CFD simulations (`pdf
   <{static}/docs/fluidsim_paper.pdf>`__)

Note: the 3 papers are now accepted.

The source code of the papers is in `this repository
<https://foss.heptapod.net/fluiddyn/fluiddyn_paper>`_.
