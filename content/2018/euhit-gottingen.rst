Euhit 2 at Göttingen
====================

:date: 2018-01-25
:modified: 2018-01-28
:tags: Euhit, Coriolis, fluiddyn, fluidlab, fluid image
:category: Conference

I attended a two-day meeting at Göttingen (25-26 Jan. 2018) where people from
all over Europe gathered to discuss about the new `Euhit
<https://www.euhit.org/>`_ proposal.

We presented with `Jan-Bert Flór
<http://www.legi.grenoble-inp.fr/people/Jan-Bert.Flor/>`_ the `Coriolis
platform <http://www.legi.grenoble-inp.fr/web/spip.php?article757>`_ and a
project to develop tools to do simultaneous T-LIF / PIV and LIF / PIV with
fluidlab and fluidimage.

Good opportunity to listen to one of Barbara's song Göttingen!

.. raw:: html

   <iframe width="560" height="315" 
           src="https://www.youtube.com/embed/t0sNy1xOhRc?rel=0" 
           frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
   </iframe>

"Et lorsque sonnerait l'alarme, 
S'il fallait reprendre les armes, 
Mon coeur verserait une larme, 
Pour Göttingen, Pour Göttingen..."
