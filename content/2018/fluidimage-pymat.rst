Fluidimage / Pivmat
===================

:date: 2018-09-27
:modified: 2018-09-27
:tags: fluiddyn, fluidimage, python, open
:category: Methods
:status: draft

Utilisateurs de Python pour la PIV ont besoin d'un bon équilalent de [Pivmat](https://www.mathworks.com/matlabcentral/fileexchange/10902-pivmat-4-10).

Fluidimage

https://github.com/OpenPIV/pivpy

code collaboratif, moins centralisé.
