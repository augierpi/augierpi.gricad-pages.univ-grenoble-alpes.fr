FluidPythran 0.1.5 -> Transonic !
=================================

:date: 2018-12-12
:modified: 2019-01-29
:tags: fluiddyn, python
:category: Methods


Edit about Transonic
--------------------

We decided to stop the development for FluidPythran and to start a very similar
but wider project called Transonic! Have a look at the `repository
<https://foss.heptapod.net/fluiddyn/transonic>`_!

------------------------------------------------------------------------------

Few weeks ago, I wrote `a blog post
<http://www.legi.grenoble-inp.fr/people/Pierre.Augier/broadcasting-numpy-abstraction-cython-pythran-fluidpythran.html>`_
on issues of Cython and Pythran. A section was called **Can we do better ?
Science fiction !** Finally, it is no longer science fiction and these thoughts
and work lead to the creation of a package I am quite proud of :-)

I put a lot of effort in the development of a hopefully nice and useful tool
for open-source scientific computing:
`FluidPythran <https://heptapod.host/meige/fluidpythran>`_.

Scientists and data analysts using Python need good tools for very efficient
computations with Python code. Among many projects addressing this issue, I
think `Pythran <https://github.com/serge-sans-paille/pythran>`_ is really very
interesting.

However, `using Pythran in real code is not always simple and nice
<https://fluidpythran.readthedocs.io/en/latest/#overview>`_.

`FluidPythran <https://heptapod.host/meige/fluidpythran>`_ addresses some of
these issues. This release (`version 0.1.5
<https://pypi.org/project/fluidpythran/0.1.5/>`_) is important. FluidPythran is
now usable. It does what it has to do, with a good battery of tests and
`a serious documentation <https://fluidpythran.readthedocs.io>`_.

With Pythran and FluidPythran, writting elegant and very efficient Python
scientific libraries and applications becomes really easy. I hope it is going
to be used elsewhere than in FluidDyn packages!

.. image:: ./images/logo_Pythran.jpeg
  :width: 35%
  :alt: group
  :align: left
.. image:: ./images/logo-fluiddyn.jpg
  :width: 40%
  :alt: guys
  :align: right
