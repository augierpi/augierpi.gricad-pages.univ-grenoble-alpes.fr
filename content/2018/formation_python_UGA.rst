Formation Python UGA nov. 2018
==============================

:date: 2018-11-08
:modified: 2018-11-08
:tags: Python
:category: Talks

J'ai animé avec Eric Maldonado une formation Python initiation sur 3 jours (5-7
nov. 2018) organisée par le service formation permanente de l'UGA. C'était bien
et on a eu de très bon retours! On a aussi bien amélioré les supports de
formation, disponible sur `ce dépôt
<https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017/>`_.

.. image:: https://www.python.org/static/opengraph-icon-200x200.png
   :width: 30%
   :alt: Python
   :align: center
