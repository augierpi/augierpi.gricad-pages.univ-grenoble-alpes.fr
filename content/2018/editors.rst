Advice for Python users in 2018
===============================

:date: 2018-09-25
:modified: 2018-09-25
:category: Methods

When I look how scientists use Python, I often see bad habits and problems:

- not fully switching to Python >= 3.6,

- difficulties with using the system Python (so one needs ``sudo`` to install
  Python packages) and/or virtual environments,

- not using IPython,

- using only Jupyter and notebooks (see the presentation `"I Don't Like
  Notebooks"
  <https://docs.google.com/presentation/d/1n2RlMdmv1p25Xy5thJUhkKGvjtV-dkAIsUXP-AL4ffI/edit#slide=id.g362da58057_0_1>`_.

- using a bad Python editor or good editors badly configured (for example with
  Vim or Emacs),

Still using Python 2.7 in 2018!
-------------------------------

The first issue of Unix users still using Python 2.7 without any good reasons
is mainly related to `PEP 394 <https://legacy.python.org/dev/peps/pep-0394/>`_
(the system ``python`` command points towards ``python2``), which starts to be
outdated.

My advice would be to NEVER use Python 2.7 for scientific code today, to stop
to write code compatible with Python 2.7 and even, to use `f-strings
<https://docs.python.org/3/whatsnew/3.6.html#whatsnew36-pep498>`_.

If you encounter a "Python 2.7" problem in one old script (for example a
``print`` without parenthesis), put as soon as possible a little bit of energy
to update the code [#]_.

.. [#] Of course, there are some cases where it is more complicated than
   that...


For simple cases, no virtual environments
-----------------------------------------

The problems with using the system Python or virtual environments can be solved
by using `pyenv <https://github.com/pyenv/pyenv>`_ and/or `miniconda
<https://conda.io/miniconda.html>`_.

For simple usage, I would recommend NOT using virtual environments, which are
complicated and error-prone. See `this tutorial on how to setup Ubuntu 18.04 to
use Python like this
<https://fluiddyn.readthedocs.io/en/latest/setup_ubuntu1804.html>`_.

For advanced users, tools like ``virtualenv``, ``pipenv`` and ``conda env`` are
of course very useful.

Use IPython!
------------

When you try code that you've just written, launch your scripts in IPython with
the magic command ``%run``.

.. code ::

   pip install ipython
   ipython
   run path/towards/myscript.py

It is then possible to interact with the objects and also to use the magic
command ``%debug`` if you get an error.

Editors
-------

One must use a good editor to write Python code! Coding in Python with a bad
editor is just a nightmare. Don't do that!

There are `many editors which somehow support Python coding
<https://wiki.python.org/moin/PythonEditors>`_ and I'm going to recommend just
few editors/IDEs.

Visual Studio Code
~~~~~~~~~~~~~~~~~~

I used to use Emacs (with `this configuration
<https://foss.heptapod.net/fluiddyn/fluid_emacs.d>`__) and few weeks ago, I started
to use more and more `Visual Studio Code with its Python extension
<https://code.visualstudio.com/docs/languages/python>`_ (with `this
configuration
<https://gist.github.com/paugier/33a0f0777c348638ff17636489399e04>`_).

For developping a Python package, it is indeed very good and it really improves
my productivity and even the quality of the code, see for example this `blog
post by Kenneth Reitz
<https://www.kennethreitz.org/essays/why-you-should-use-vs-code-if-youre-a-python-developer>`_

On Unix, one may want to avoid using a binary build by Microsoft so we can use
`vscodium <https://github.com/VSCodium/vscodium>`_.

Let's say that there are of course things that could be improved in VSCode, as
for example `the behavior of the tab key
<https://github.com/Microsoft/vscode-python/issues/2680>`_.

Simple IDEs for scientists
~~~~~~~~~~~~~~~~~~~~~~~~~~

For scientists and for simpler programming (only scripts), other tools, as
`Spyder <https://github.com/spyder-ide/spyder>`_, `Pyzo
<http://www.pyzo.org/>`_ or `Eric <https://eric-ide.python-projects.org/>`_
should be more adapted.

Jupyter-lab
~~~~~~~~~~~

For interactive data processing (and `not for everything
<https://docs.google.com/presentation/d/1n2RlMdmv1p25Xy5thJUhkKGvjtV-dkAIsUXP-AL4ffI/edit#slide=id.g362da58057_0_1>`_),
`JupyterLab <https://github.com/jupyterlab/jupyterlab>`_ starts to be a great
tool.
