Why do I foster using open-source and Python for science?
=========================================================

:date: 2018-02-02
:modified: 2018-02-02
:tags: fluiddyn, python, open
:category: Methods
:status: draft

J'ai donné une présentation sur "Dynamique open-source et conséquences sur la
recherche et l'enseignement en mécanique des fluides".

Des retours...

Trois idées sur lesquels je peux apporter des éclaircissements et présenter des données concrètes.:

- J'aurais une sorte de lubie pour Python qui ne serait qu'un petit langage de
  script pas plus utilisé et important qu'un autre...

- Python n'est pas une affaire d'amateurs...

- Mais pourquoi je communique sur l'open-source et Python en science?

Beaucoup de collègues ne comprennent pas pourquoi je fais ça...

"Je ne suis pas à même de juger des qualités de ce langage, et encore une fois,
je ne remets pas en question ton avis ni ton enthousiasme quant à sa puissance
et son intérêt. Mais ce que je comprends mal, c'est ce qui te motive à essayer
de convaincre les gens de l'utiliser aussi."


Python un grand langage pour la science (autant que Fortran, C++)
-----------------------------------------------------------------

Python a récemment gagné ses galons de grand langage main stream pour la
science, au même titre que Fortran ou C++.

Indices
~~~~~~~

https://spectrum.ieee.org/computing/software/the-2017-top-programming-languages

Langage de programmation main stream (with C, Java, C++).


http://pypl.github.io/PYPL.html

Langage dominant pour la data science et le machine learning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Langage qui profite le plus de la vague "data science".

https://fossbytes.com/popular-top-programming-languages-machine-learning-data-science/

.. image:: https://fossbytes.com/wp-content/uploads/2016/12/machine-learning-data-science-programming-language.png
   :width: 80%
   :alt: machine-learning-data-science-programming-language.png
   :align: center


Contenus sur le web
~~~~~~~~~~~~~~~~~~~

Recherches Google (associé avec data science).

- https://www.google.com/search?q=python+data+science

  About 4,570,000 results

- https://www.google.com/search?q=matlab+data+science

  About 1,450,000 results

- https://www.google.com/search?q=fortran+data+science

  About 545,000 results

Second main languages on Github
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

https://octoverse.github.com/

"Python replaced Java as the second-most popular language on GitHub, with 40
percent more pull requests opened this year than last."

Stackoverflow
~~~~~~~~~~~~~

https://stackoverflow.blog/2017/09/06/incredible-growth-python/

https://stackoverflow.com/tags

python, matlab, numpy, pandas


Python n'est pas une affaire d'amateurs...
------------------------------------------

Pourquoi je communique sur l'open-source et Python en science?
--------------------------------------------------------------
