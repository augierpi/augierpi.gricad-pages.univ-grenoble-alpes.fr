Python Science Data Grenoble launching day
==========================================

:date: 2018-03-08
:modified: 2018-03-08
:tags: fluiddyn, fluidfft, fluidsim, fluidlab, fluidimage
:category: Talks

We launch our group "Python for science and data analysis in Grenoble" Thurday
8 of March with `a full day of presentations
<https://python-uga.sciencesconf.org/resource/page/id/6>`_.

Since we are lucky at LEGI, we had also a first presentation by Alexandre
Gramfort (scikit-learn) on `an introduction to Machine Learning
<http://www.legi.grenoble-inp.fr/web/spip.php?article1337>`_.

I'm going to give a presentation on `"Laboratory experiments in the Coriolis
platform with Python" (and also on the FluidDyn project!)
<{static}/docs/ipynb/201803pygre/pres201803pygre.slides.html>`_.

.. image:: ./images/logo-fluiddyn.jpg
   :width: 49%
   :alt: FluidDyn logo.
   :align: left
.. image:: https://www.python.org/static/opengraph-icon-200x200.png
   :width: 30%
   :alt: Python
   :align: right
