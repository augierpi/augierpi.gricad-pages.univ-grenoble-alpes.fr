Presentation at "AlpesVIEW"
===========================

:date: 2015-12-05
:modified: 2015-12-05
:tags: Python, FluidDyn, FluidLab
:category: Talks

I participated to the `AlpesVIEW day
<https://lpsc.in2p3.fr/Indico/internalPage.py?pageId=0&confId=1125>`_,
a day of presentations and of formation on the use of Labview (a
proprietary software by National Instruments) to control laboratory
experiments.

It was in particular interesting to see how is organized
the "Labview ecosystem" in which interact the main company National
Instruments, other companies working with Labview and a community of
passionate scientists paid by public universities and research
institutes. In my opinion, the lack of equivalent ecosystems for
Python for research and education in my field is a big problem that
needs to be tackled.

I gave a short presentation on how we overcame difficulties of
communication with a motor drive with Python and `FluidLab
<http://fluidlab.readthedocs.org>`_. The pdf of my slides can be
download `here
<{static}/docs/talks/talk_PA_Labview_LEGI_2015-12-03.pdf>`_.
