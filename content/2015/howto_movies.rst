How to: make short demo movies
==============================

:date: 2015-09-30
:modified: 2015-09-30
:tags: Python, howto, FluidDyn, FluidLab
:category: Methods

For a presentation on FluidLab, I wanted to make a short video. I used:

- Recordmydesktop to make a movie of the screen and ffmpeg for a
  conversion ogv to mp4 (next time I will try Kazam).
  
- Openshoot,

  I exported the video in mp4 with the codec libx264 at a bit rate of
  384 kb/s.
    
- and Dailymotion for web access...

This gives

.. raw:: html

   <iframe frameborder="0" width="480" height="270"
           src="http://www.dailymotion.com/embed/video/x387j3r"
           allowfullscreen>
   </iframe>

The video demonstrates how a motor can be controlled with Python and
the package fluidlab.
