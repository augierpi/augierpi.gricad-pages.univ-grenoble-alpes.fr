Proposition of internship: experiments and simulations of stratified turbulence
===============================================================================

:date: 2015-10-27
:modified: 2015-10-27
:tags: stages,
:category: Thesis-stages

If you are a student (for example in master 1 or 2) and that you'd
like to do an internship in advanced fluid dynamics applied to
geophysical flows, you can consider a subject that I propose:

*"Study of geophysical flows by experiments in the Coriolis platform
and numerical simulations of stratified turbulence forced by waves and
vortices"*

For more details see
`the description of the subject in pdf
<{static}/docs/prop_stage_LEGI_Greboble_PierreAugier.pdf>`_.

Do not hesitate to contact me. We can organize a visit of my lab and
of the `Coriolis platform
<http://www.legi.grenoble-inp.fr/web/spip.php?article757&lang=fr>`_.
