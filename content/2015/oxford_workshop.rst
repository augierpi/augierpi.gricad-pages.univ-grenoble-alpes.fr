Leverhulme workshop in Oxford
=============================

:date: 2015-09-08
:modified: 2015-09-08
:tags: Turbulence, Stratified flows
:category: Talks

I attended the Leverhulme workshop in Oxford on Waves and Turbulence
in Rotating, Stratified and Electrically Conducting Fluids (organized
by Peter Read and Peter Davidson). Here is `the full program
<http://www-thphys.physics.ox.ac.uk/research/plasma/leverhulme2015.pdf>`_.

The title of my talk was *"Can we study strongly stratified turbulence in a
laboratory experiment?"*. The pdf of my slides can be downloaded `here
<{static}/docs/talks/talk_PA_Oxford_2015-09-01.pdf>`_.

It was in particular the occasion to work with Fachreddin
Tabataba-Vakili and Alexandru Mihai Valeanu (2 PhD students working
with Peter Read) on the use of the formulation of the spectral energy
budget of the atmosphere developed in `Augier & Linborg (2013)
<http://dx.doi.org/10.1175/JAS-D-12-0281.1>`_ for other planets than
the Earth. We decided to write a proper open-source Python package,
spectratmo (see `the public repository
<https://foss.heptapod.net/fluiddyn/spectratmo>`_).
