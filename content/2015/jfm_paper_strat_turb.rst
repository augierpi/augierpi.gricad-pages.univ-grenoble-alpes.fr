Stratified turbulence forced with columnar dipoles (new JFM paper)
==================================================================

:date: 2015-04-13
:modified: 2015-04-13
:tags: Turbulence, Stratified flows, Numerics
:category: Articles

A paper entitled `"Stratified turbulence forced with columnar dipoles:
numerical study" <http://dx.doi.org/10.1017/jfm.2015.76>`_ written by
Paul Billant, Jean-Marc Chomaz and myself has just been published in
the Journal of Fluid Mechanics.

.. figure:: {static}/images/cover_FLM769.jpg
   :width: 20 %
   :alt: a cover
   :align: right

   The cover of the volume 769 of JFM.

In this study, we reproduce experimental results obtained during my
PhD thesis with a relatively small experiment and show that we would
need a larger experiment (~ 10 m) to produce strongly stratified
turbulence as it should exist in the atmosphere and in the oceans.

I would also like to highlight another study: `High-resolution
large-eddy simulations of stably stratified flows: application to
subkilometer-scale turbulence in the upper troposphere–lower
stratosphere
<http://www.atmos-chem-phys.net/14/5037/2014/acp-14-5037-2014.html>`_
recently published in the journal Atmospheric Chemistry and
Physics. This article has been written by scientists working at Meteo
france in Toulouse. Since these researchers are specialized in
atmospheric sciences, the simulations presented in this study are be
more realistic than ours in terms of dynamics of the atmosphere. In
particular, an atmospheric code called Meso-NH was used whereas we
used a pseudo-spectral code for theoretical fluid mechanics called
ns3d. It is very encouraging that despite the differences in terms of
methods and points of views of the researchers, the results are quite
similar and our interpretations are consistent.
