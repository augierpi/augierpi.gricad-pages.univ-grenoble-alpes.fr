Ateliers d'initiation Python au LEGI
====================================

:date: 2015-02-11
:modified: 2015-02-11
:tags: Python
:category: Numerics

J'organise 2 séances d'initiation par la pratique à Python
scientifique, dans la salle de réunion A128 :

- mercredi 11/02, 10h30 - 12h
- jeudi 12/02, 14h - 15h30

J'ai fait 2 Ipython notebooks pour les deux ateliers que vous pouvez
télécharger dans `cette archive
<{static}/docs/Docs_ateliers_Python_LEGI.zip>`_ et utiliser avec la
commande "ipython notebook". Vous pouvez aussi les visualiser
directement (mais sans interaction) en utilisant le site
`nbviewer.ipython.org <http://nbviewer.ipython.org/>`_:

- `notebook atelier 1 vu avec nbviewer
  <http://nbviewer.ipython.org/url/www.legi.grenoble-inp.fr/people/Pierre.Augier/docs/python_LEGI_1.ipynb>`_

- `notebook atelier 2 vu avec nbviewer
  <http://nbviewer.ipython.org/url/www.legi.grenoble-inp.fr/people/Pierre.Augier/docs/python_LEGI_2.ipynb>`_
