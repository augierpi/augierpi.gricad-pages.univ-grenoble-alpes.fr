Open access
===========

:date: 2015-12-08
:modified: 2015-12-08
:tags: Open
:category: Methods

Let's advertise some very interesting `posters on open access
publication <http://oam.biu-montpellier.fr/?page_id=738>`_. As
researchers, we have to care about the issue of how we publish our
results.

One simple thing that we have to do is to deposit as soon as possible
our articles on an open access server, as for example in France the
open archive `HAL <https://hal.archives-ouvertes.fr/>`_.


