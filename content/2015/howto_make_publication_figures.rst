How to: Make publication figures
================================

:date: 2015-06-10
:modified: 2015-06-10
:tags: Python, howto
:category: Methods

I organized a discussion between colleagues on your methods to produce
figures for articles and presentations.

Tobit Caudwell presented how He produces his figures with Matlab and Latex.

The pdf of my presentation on how I produce figures with Python and
Matplotlib can be found `here
<{static}/docs/talks/talk_howto_figures_2015-06-10.pdf>`_.

The code used to produce the figures and the presentation can be
downloaded with Mercurial by running:

.. code:: bash

   hg clone https://heptapod.host/meige/figures_articles

If you just want to learn more on how to produce figures with
`Matplolib <http://matplotlib.org/>`_, I'd advice to start by trying
some `examples scripts <http://matplotlib.org/gallery.html>`_.
