Prix de la fondation Del Duca
=============================

:date: 2015-05-06
:modified: 2016-11-04
:category: Projects

Nous avons obtenu avec Nicolas Mordant et Bruno Voisin le `prix de la fondation
Del Duca (Institut de France)
<http://www.fondation-del-duca.fr/subventions-scientifiques>`_ pour un projet
d'étude de la turbulence d'ondes internes de gravité.

Ce prix est d'un montant de 75000 € + un an de post-doctorat. Il nous a permis
de faire travailler Antoine Campagne pendant 1 ans (maintenant payé sur budget
de `WATU, l'ERC de Nicolas Mordant
<http://nicolas.mordant.free.fr/watu.html>`_) et de financer les stages de Rémi
Chassagne et Miguel Calpe Linares (qui continue avec nous avec une thèse).

Nous avons notamment monté une expérience dans une petite cuve de 1.5 m x 1.5 m
x 0.8 m où des ondes sont forcées par l'oscillation de grosses demi-sphères à
la surface d'un fluide stratifié.
