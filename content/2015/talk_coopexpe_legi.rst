Presentation for Coop Expé at LEGI
==================================

:date: 2015-10-12
:modified: 2015-10-12
:tags: Python, FluidDyn, FluidLab
:category: Talks

"Coop Expé" meetings are organized in my laboratory LEGI to foster
cooperation between people working on experiments. After a general
discussion, one person gives a short seminar on an subject related to
experimental work.

For the "Coop Expé" meeting of October 2015, I gave a presentation on
the control of experiments by computers and on the Python package
`FluidLab <http://fluidlab.readthedocs.org>`_.

The pdf of my slides can be download `here
<{static}/docs/talks/talk_PA_coopexp_2015-10-12.pdf>`_.
