Petite liste ordonnée des mots clés, types et fonctions "builtin" Python
========================================================================

:date: 2015-01-31
:modified: 2015-01-31
:tags: Python
:category: Numerics
:status: draft

Je me demandais quels étaient les mots clés, types et fonctions
"builtin" dans Python. Ce n'est pas super clair dans la doc officielle
https://docs.python.org/2.7/library/index.html. J'ai fait cette petite
liste un peu ordonnée. C'est pour Python 2.

Mots clés
---------

Voir https://docs.python.org/2.7/library/keyword.html.

Mots clés de conditions
~~~~~~~~~~~~~~~~~~~~~~~
- if, elif, else
- not, and, or
- in
- is
- pass

Mots clés de boucles
~~~~~~~~~~~~~~~~~~~~
- while
- for
- break, continue
- in (déjà listé dans mots clés de conditions)
- pass (déjà listé dans mots clés de conditions)

Mots clés de définitions
~~~~~~~~~~~~~~~~~~~~~~~~
- def
- return
- yield
- class
- lambda
- pass (déjà listé dans mots clés de conditions)

Mots clés pour les imports
~~~~~~~~~~~~~~~~~~~~~~~~~~
- import
- from
- as

Mots clés pour les exceptions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- raise
- try, except, finally
- assert
- as (déjà listé dans mots clés pour les imports)

Autres mots clés
~~~~~~~~~~~~~~~~
- print
- with
- del
- global
- exec


Builtin types and functions
----------------------------

Voir https://docs.python.org/3/library/functions.html.

.. code:: python

  try:
      import builtins
  except ImportError:  # For Python 2
      import __builtin__ as builtins

  [s for s in dir(builtins) if s[0].islower() ]

['abs', 'all', 'any', 'apply', 'basestring', 'bin', 'bool', 'buffer',
'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'cmp',
'coerce', 'compile', 'complex', 'copyright', 'credits', 'delattr',
'dict', 'dir', 'divmod', 'dreload', 'enumerate', 'eval', 'execfile',
'file', 'filter', 'float', 'format', 'frozenset', 'get_ipython',
'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id',
'input', 'int', 'intern', 'isinstance', 'issubclass', 'iter', 'len',
'license', 'list', 'locals', 'long', 'map', 'max', 'memoryview',
'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print',
'property', 'range', 'raw_input', 'reduce', 'reload', 'repr',
'reversed', 'round', 'set', 'setattr', 'slice', 'sorted',
'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'unichr',
'unicode', 'vars', 'xrange', 'zip']

.. code:: python

  [s for s in dir(builtins) if s[0].isupper() ]

['ArithmeticError', 'AssertionError', 'AttributeError',
'BaseException', 'BufferError', 'BytesWarning', 'DeprecationWarning',
'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False',
'FloatingPointError', 'FutureWarning', 'GeneratorExit', 'IOError',
'ImportError', 'ImportWarning', 'IndentationError', 'IndexError',
'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError',
'NameError', 'None', 'NotImplemented', 'NotImplementedError',
'OSError', 'OverflowError', 'PendingDeprecationWarning',
'ReferenceError', 'RuntimeError', 'RuntimeWarning', 'StandardError',
'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError',
'SystemExit', 'TabError', 'True', 'TypeError', 'UnboundLocalError',
'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError',
'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning',
'ValueError', 'Warning', 'ZeroDivisionError']
