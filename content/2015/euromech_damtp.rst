Euromech colloquium 567
=======================

:date: 2015-03-24
:modified: 2015-02-11
:tags: Turbulence, Stratified flows
:category: Talks

I attended the `Euromech colloquium 567 <http://567.euromech.org/>`_
"Turbulent mixing in stratified flows" in Cambridge. I gave a talk on
*"A new Taylor-Couette apparatus to study turbulence in stratified
fluids"*. The pdf of my slides can be downloaded `here
<{static}/docs/talks/talk_PA_Euromech_2015-03-24.pdf>`_.
