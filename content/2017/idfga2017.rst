Conference IDFGA 2017
=====================

:date: 2017-10-18
:modified: 2017-10-18
:tags: Turbulence, Stratified flows, Python
:category: Conference

I attended the CNRS workshop IGAFD (`Interdisciplinary Geo-Astro Fluid Dynamics
<http://lseet.univ-tln.fr/L7/squel.php?content=idfga.php>`_)

Here are the `slides of my presentation
<{static}/docs/ipynb/idfga20171018/presentationidfga20171018.slides.html#/>`_
(made with jupyter) [reload the page with F5 if the latex equations do not load
correctly].
