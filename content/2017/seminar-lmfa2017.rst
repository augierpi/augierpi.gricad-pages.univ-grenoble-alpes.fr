Seminar at LMFA (Lyon)
======================

:date: 2017-09-08
:modified: 2017-09-08
:tags: Turbulence, Stratified flows, Python
:category: Talks

I was invited to give a seminar at `LMFA
<http://lmfa.ec-lyon.fr/spip.php?article858>`_ (Ecole Centrale Lyon).

I gave a presentation on *"Studying mixing efficiency and stratified turbulence
with experiments and open-science"*.

Here are the `slides of my presentation
<{static}/docs/ipynb/lmfa20170908/presentationLMFA201709.slides.html#/>`_
(made with jupyter, the sources are `here
<https://heptapod.host/meige/talk_lmfa_2017-09>`_) [reload the page with F5
if the latex equations do not load correctly].
