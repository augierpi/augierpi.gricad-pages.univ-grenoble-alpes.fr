Sondage Python au LEGI 2017
===========================

:date: 2017-01-23
:modified: 2017-01-23
:tags: Python, Teaching
:category: Methods

Nous organisons avec des collègues de l'UGA une formation débutants et
intermédiaires à Python. Dans ce cadre, j'ai fait un `petit sondage
<https://framaforms.org/sondage-python-legi-2017-1484310919>`_ dans mon
laboratoire le LEGI (le sondage est maintenant fermé). J'ai utilisé le service
libre `framaform <https://framaforms.org>`_ qui a été parfait pour cette
utilisation très simple (merci à l'association `framasoft
<https://framasoft.org/>`_).

Une rapide analyse des résultats du sondage est présentée dans cette `petite
présentation <{static}/docs/ipynb/sondage_LEGI_2017.slides.html>`_
(faîtes avec jupyter).

`test (short charset)
<{static}/docs/ipynb/sondage_LEGI_2017.slides_try.html>`_
