Proposition engineering project Ense3: Simulation wake of smooth topography with Coriofoam
==========================================================================================

:date: 2017-10-19
:modified: 2017-10-19
:tags: Turbulence, Stratified flows, Python, OpenFOAM
:category: Thesis-stages

Last year, we worked with 5 students of ENSE3 on the development of an OpenFOAM
solvers to simulate flows in the `Coriolis platform
<http://www.legi.grenoble-inp.fr/web/spip.php?article757>`_, the big rotating
tank of the LEGI laboratory. This work produces a solver (see the `repository
<https://heptapod.host/meige/coriofoam>`_ and the `documentation
<http://coriofoam.readthedocs.io>`_). We learned how to use OpenFoam to solve
the Navier-Stokes equations under the Boussinesq approximation in a cylindrical
geometry. We are now ready to address more difficult problems, i.e. to simulate
real flows studied in the Coriolis platform.

For this research project, we propose to simulate the wake of a smooth 3D
submarine mountain in a stratified and or rotating fluid. This configuration is
interesting because:

- It can be studied experimentally so we can compare our results to
  experimental results.

- Production of internal gravity waves in a fluid flowing over a topography is
  a very important aspect of the oceanic and atmospheric dynamics. This problem
  has been extensively studied but mainly under the assumption of 2D mountains
  (ridges). Therefor the parametrizations are well adapted to ridges. However,
  it is clear that real mountains are not 2D and that the 2D and 3D cases are
  very different since the flow can get around a 3D mountain.

The students will learn a lot on numerical simulations of turbulent flows and
on great tools widely used in research and industry: OpenFoam, Linux and
Python.

Students will be supervised by Cyrille Bonamy (Ingénieur de Recherche Calcul at
LEGI) and Pierre Augier (Chargé de Recherche).  Depending on the participants
of the project, we will communicate in English or/and in French.

.. raw:: html

  <div >
    <img alt="Mesh representing a smooth mountain"
         src="./images/mesh_mountain.png" style="width: 60%; display:block; margin-left: auto; margin-right: auto;">
    <p class="caption">Example of a mesh representing a smooth 3D mountain.</p>
  </div>
