Milestone 2017
==============

:date: 2017-07-31
:modified: 2017-07-31
:category: Projects

During June and July, we used the Coriolis platform for other experiments of
the project `MILESTONE <{filename}/2016/milestone.rst>`_ (MIxing and LEngth
Scales in Stratified Turbulence). Antoine Campagne, Miguel Calpe-Linares,
Samuel Viboud and Thomas Valran participated to `this experimental campaign
<http://www.legi.grenoble-inp.fr/web/spip.php?article1290>`_.

This time, we focused on the measurement of the mixing efficiency in the regime
of strongly stratified turbulence. We also tried to force internal waves by
dragging long horizontal cylinders.
