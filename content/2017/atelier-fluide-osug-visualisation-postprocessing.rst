Atelier fluide OSUG Visualisation et postprocessing
===================================================

:date: 2017-06-06
:modified: 2017-06-06
:tags: Open, Python, fluiddyn, fluidlab, fluidimage
:category: Methods

J'ai participé à la `Journée de l'atelier Fluides de l'OSUG sur le thème
"Visualisation et Post-processing"
<http://www.osug.fr/la-recherche/ateliers-transversaux/fluides/journee-de-atelier-fluides-visualisation-et-post-processing-mardi-6-juin?lang=fr>`_.

Cette journée d'échange et de formation avait pour but de partager les
pratiques des laboratoires de l'OSUG concernant la visualisation tant sur des
applications numériques qu'expérimentales. La matinée a été dédiée à des
présentations de cas concrets mettant en avant les outils utilisés (programmes
"maisons", logiciels libres ou commerciaux, ...). Pendant l'après midi, nous
avons fait un atelier sur le logiciel Paraview animé par un formateur de la
société Kitware .

J'ai donnée une présentation intitulée: Logiciels libres et Python pour
l'acquisition, la gestion et le traitement de grosses données expérimentales,
exemple d'une campagne sur la plate-forme Coriolis.
