Conference Pyparis
==================

:date: 2017-06-16
:modified: 2017-06-16
:tags: Python
:category: Methods

I attended the conference `Pyparis <http://pyparis.org/>`_.  Here are the
`slides of my presentation
<{static}/docs/ipynb/pyparis2017/fluiddyn.slides.html>`_ (made with
jupyter, the sources are `here
<https://heptapod.host/meige/fluiddyn_pyparis2017>`_).

I gave a similar presentation at a workshop `Visualization and post-processing
<http://www.osug.fr/la-recherche/ateliers-transversaux/fluides/journee-de-atelier-fluides-visualisation-et-post-processing-mardi-6-juin>`_
organized in my university (UGA, OSUG).

Ashwin Vishnu, who work on fluidsim, nicely improved these slides for `a
presentation at KTH (Stockholm)
<https://gfdyn.bitbucket.io/talks/phd_summer_seminar.slides.html?transition=fade#/>`_.

If you are interested by the subject of open-science, I also advice to watch a
very interesting keynote conference given at PyCon 2017 by Jake VanderPlas (one
of the developer of astropy): `The Unexpected Effectiveness of Python in
Science <https://www.youtube.com/watch?v=ZyjCqQEUa8o>`_.
