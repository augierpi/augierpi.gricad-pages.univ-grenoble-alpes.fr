Proposition de stage sur Fluidimage au LEGI (CNRS, UGA, Grenoble)
=================================================================

:date: 2017-10-27
:modified: 2018-01-12
:tags: python, numerics, open, stage, fluiddyn, fluidimage
:category: Thesis-stages

Le profil recherché est un-e geek aimant coder en Python, en particulier quand
ça devient un peu compliqué (POO, HPC multi-architecture, GPU, calculs
asynchrones et distribués).

Le stage proposé porte sur le programme `Fluidimage
<http://fluidimage.readthedocs.org/>`_. Fluidimage est un code de traitement
d'images scientifiques de mécanique des fluides. Depuis quelques dizaines
d'années, le traitement d'images est particulièrement utilisé dans le domaine
de la mécanique des fluides.  Aujourd'hui, il y a deux évolutions importantes
qui nous poussent à développer un code open-source en Python :

1. Les techniques expérimentales actuelles génèrent de plus en plus de données.
   Il est donc primordial de pouvoir traiter de très gros volumes de données
   dans un temps raisonnable. Les codes actuels (propriétaires ou non) n'étant
   pas pensés pour les architecture de calcul d'aujourd'hui et de demain
   (cluster HPC, Big Data, GPU), il nous apparaît important de développer
   un produit qui répondra à ce besoin, d'où `Fluidimage
   <http://fluidimage.readthedocs.org/>`_.

2. Les méthodes actuelles de l'open-source ont ouvert pour la recherche
   scientifique des opportunités encore très peu explorées. On peut en effet
   imaginer une recherche où les développements logiciels seraient de qualité,
   partagés, collaboratifs et valorisés.  Nous avons entre autres besoin
   d'environnements logiciels spécialisés et perennes permettant d'accueillir
   des contributions de scientifiques.

Fluidimage a été écrit pour répondre à ces deux enjeux. Il constitue désormais
un cadre de travail pour développer des traitements scientifiques de grandes
séries d'images. Actuellement utilisé en production pour traiter des images
obtenues dans la plateforme Coriolis, ses performances sont déjà très
satisfaisantes. Étant encore jeune, il reste beaucoup à faire afin de
convaincre la communauté et de devenir une référence en la matière.

Un des composants de base de Fluidimage est son moteur asynchrone d'exécution
de tâches en parallèle. Il est aujourd'hui basé sur des threads et processus
(avec multiprocessing) mais on pourrait faire bien mieux en considérant une
architecture de microservices (avec possible utilisation de `zeromq
<http://zeromq.org/bindings:python>`_, `mpi4py
<http://mpi4py.readthedocs.io>`_, `rpyc <https://rpyc.readthedocs.io>`_ et/ou
`dispel4py <https://github.com/dispel4py/dispel4py>`_) et en utilisant `asyncio
<https://docs.python.org/3/library/asyncio.html>`_ et les mots clés async et
await pour les tâches d'entrées/sorties.  Une autre stratégie envisagée serait
d'utiliser un système de calcul distribué type "big data" (`Storm
<http://storm.apache.org/>`_, `Spark <https://spark.apache.org/>`_ ou `Dask
<https://dask.pydata.org/>`_).

L'amélioration du moteur asynchrone de Fluidimage est un des nombreux points
sur lesquels nous voulons avancer.  D'autres objectifs d'évolution du logiciel
concernent les aspects traitement d'images et notamment l'utilisation
d'accélérateurs (GPU ou encore Xeon Phi).  Le contenu précis du stage sera
ajusté en fonction des compétences et envies du stagiaire.

Le stage sera encadré par `Cyrille Bonamy
<http://www.legi.grenoble-inp.fr/web/spip.php?auteur223>`_ (IR calcul CNRS) et
`Pierre Augier <http://www.legi.grenoble-inp.fr/people/Pierre.Augier>`_ (CR
CNRS) et se fera en interaction avec plusieurs chercheurs du laboratoire LEGI
(Grenoble) travaillant avec Fluidimage et sur diverses techniques de
traitements d'images. Au vu du projet envisagé, nous considérons que le stage
devra se dérouler sur au moins 3 mois.
