Proposition research project Ense3: Simulation evolution of a denser region with Coriofoam and Fluidsim
=======================================================================================================

:date: 2017-09-29
:modified: 2017-09-29
:tags: Turbulence, Stratified flows, Python, OpenFOAM
:category: Thesis-stages

Context and objectives
----------------------

Last year, we worked with 5 students of ENSE3 on the development of an OpenFOAM
solvers to simulate flows in the `Coriolis platform
<http://www.legi.grenoble-inp.fr/web/spip.php?article757>`_, the big rotating
tank of the LEGI laboratory. This work produces a solver (see the `repository
<https://heptapod.host/meige/coriofoam>`_ and the `documentation
<http://coriofoam.readthedocs.io>`_). We learned how to use OpenFoam to solve
the Navier-Stokes equations under the Boussinesq approximation in a cylindrical
geometry. We are now ready to address more difficult problems, i.e. to simulate
real flows studied in the Coriolis platform.

For this research project, we propose to simulate the evolution of a blob of
denser fluid initially at rest at the center of the Coriolis platform when the
Coriolis platform in rotating. This configuration is interesting because:

- it can be studied experimentally so we can compare our results to
  experimental results.

- the initial state and the short-term dynamics are conceptually quite simple.

- the long-term dynamics is potentially very interesting. It involves
  complicated instabilities and turbulence, in particular for moderate rotation
  rate.

- the dynamic is not dominated by the boundary layers, so we should be able to
  reproduce the main characteristic of the flows with simulations at moderate
  resolution.

Description of the work
-----------------------

We propose to simulate this flow with the solver Coriofoam and with an
efficient pseudo-spectral code written mainly in Python, `fluidsim
<http://fluidsim.readthedocs.io>`_ in order to validate the two solvers and to
study different aspects of the physical problem.

The students will have to setup the simulations and carry them out on the LEGI
clusters. They will have to visualize and postprocess the data with Paraview
and Python.

The students will learn a lot on numerical simulations of turbulent flows and
on great tools widely used in research and industry: OpenFoam, Linux and
Python.

Supervisory staff
-----------------

Students will be supervised by Cyrille Bonamy (Ingénieur de Recherche Calcul at
LEGI) and Pierre Augier (Chargé de Recherche specialist in stratified and
rotating turbulence).  Depending on the participants of the project, we will
communicate in English or/and in French.
