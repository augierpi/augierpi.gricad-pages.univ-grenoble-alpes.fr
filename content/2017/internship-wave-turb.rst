Proposition internship simulation wave turbulence 
=================================================

:date: 2017-09-29
:modified: 2017-09-29
:tags: Turbulence, Stratified flows, Python
:category: Thesis-stages

Weak wave turbulence has not yet been observed and studied in experiments in
stably stratified fluids.  Within the `ERC project WATU
<http://nicolas.mordant.free.fr/watu.html>`__, which is dedicated to study wave
turbulence experimentally, we plan to produce internal gravity wave turbulence
in the Coriolis platform with large plates oscillating around a horizontal
axis.

We propose for this internship to perform bidimentional simulations of flows
forced similarly as in the planed experiments.  The goal is to check whether
such forcing can produce 2d weak wave turbulence and to optimize the
experimental setup.

We will use the solver `ns2d.strat
<http://fluidsim.readthedocs.io/en/latest/generated/fluidsim.solvers.ns2d.strat.html>`_
of the CFD code `fluidsim <http://fluidsim.readthedocs.io>`_.  To setup the
numerical case, we will have to implement an oscillating forcing scheme, which
will involve only simple Python coding and should be very simple.

The student will have to interact with the experimental group (in particular
Nicolas Mordant and Antoine Campagne) and with Miguel Calpe Linares, a PhD
student also working with the solver `ns2d.strat
<http://fluidsim.readthedocs.io/en/latest/generated/fluidsim.solvers.ns2d.strat.html>`_.
Knowledge of stratified fluids and turbulence and competences in Python
and Linux will be very useful.

.. image:: ./images/fig_prop_2d_wave_turb.jpg
   :width: 100%
   :alt: Image internal gravity wave turbulence 
   :align: center
