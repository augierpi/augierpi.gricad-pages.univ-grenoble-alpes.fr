AGU Fall Meeting 2017 in New Orleans
====================================

:date: 2017-12-07
:modified: 2018-01-28
:tags: Milestone, Open, Python, fluiddyn, fluidlab, fluidimage
:category: Conference

I attended the `AGU Fall Meeting in New Orleans
<https://fallmeeting.agu.org/2017/>`_ (11-15 Dec. 2017).

I presented a `poster on "Measuring mixing efficiency in experiments of
strongly stratified turbulence"
<{static}/docs/poster_PierreAugier_AGU2017.pdf>`_

The Latex sources of the poster are available `here
<https://heptapod.host/meige/poster-agu2017>`_ and there is also a simpler
`template <https://heptapod.host/meige/template_poster_latex>`_.

I also cheered up Ashwin Vishnu Mohanan who will present a nice study on
"Modifying shallow-water equations as a model for wave-vortex turbulence". You
can have a look at the `slides of his presentation
<https://gfdyn.bitbucket.io/talks/agu_fallmeeting2017.slides.html#/>`_.

More on the study I will present
--------------------------------

Title: `Measuring mixing efficiency in experiments of strongly stratified turbulence <{static}/docs/poster_PierreAugier_AGU2017.pdf>`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Abstract
~~~~~~~~

Oceanic and atmospheric models need better parameterization of the mixing
efficiency. Therefore, we need to measure this quantity for flows
representative of geophysical flows, both in terms of types of flows (with
vortices and/or waves) and of dynamical regimes. In order to reach sufficiently
large Reynolds number even for strongly stratified flows, experiments for which
salt is used to produce the stratification have to be carried out in a very
large rotating platform of at least 10-meter diameter.

We present new experiments done in summer 2017 to study experimentally strongly
stratified turbulence and mixing efficiency in the Coriolis platform. The flow
is forced by a slow periodic movement of an array of large vertical or
horizontal cylinders. The velocity field is measured by 3D-2C scanned
horizontal particles image velocimetry (PIV) and 2D vertical PIV. Six
density-temperature probes are used to measure vertical and horizontal profiles
and signals at fixed positions.

We will show how we rely heavily on open-science methods for this study. Our
new results on the mixing efficiency will be presented and discussed in terms
of mixing parameterization.

Authors
~~~~~~~

Antoine Campagne, Ashwin Vishnu Mohanan, Diane Micard, Antonio Segalini, Rémi
Chassagne, Nicolas Mordant, Joël Sommeria, Erik Lindborg, Pierre Augier.

Other information
~~~~~~~~~~~~~~~~~

- Abstract ID: 223929
- Final Paper Number: NG21A-0127
- Presentation Type: Poster
- Session Date and Time: Tuesday, 12 December 2017; 08:00 - 12:20
- Session Number and Title: NG21A: Geophysical Fluid Dynamics IV Posters
- Location: New Orleans Ernest N. Morial Convention Center; Poster Hall D-F
