Workshop Traitement de Données pour la Mécanique des Fluides
============================================================

:date: 2017-11-29
:modified: 2017-11-29
:tags: Open, Python, fluiddyn, fluidlab, fluidimage
:category: Conference

Du 29 novembre au 1er décembre 2017, je vais participer au workshop `Traitement
de Données pour la Mécanique des Fluides
<https://indico.lal.in2p3.fr/event/4644/>`_ à Orsay.

Here are the `slides of my presentation
<{static}/docs/ipynb/20171130tdmf/presentation20171130tdmf.slides.html#/>`_
(made with jupyter) [reload the page with F5 if the latex equations do not load
correctly].

Titre de ma présentation
------------------------

Logiciels libres et Python pour l'acquisition, la gestion et le traitement de
grosses données expérimentales, exemple d'une campagne sur la plate-forme
Coriolis.

Résumé de ma présentation
-------------------------

Les techniques de mesure de pointe en mécanique des fluides expérimentale
impliquent souvent la gestion de grande quantité de données. Je présenterai le
cas d'une série d'expériences avec PIV scannée sur la plate-forme Coriolis.

D'un autre côté, les méthodes de l'open-source d'aujourd'hui sont pour la
recherche une opportunité encore inexploitée.  On peut aujourd'hui imaginer une
recherche où les développements logiciels sont partagés, pris au sérieux et de
qualité.

Je montrerais comment les logiciels libres et Python ont été utilisés pour le
contrôle des expériences et l'acquisition, la gestion et le traitement des
données. Ces méthodes nous ont permis d'utiliser efficacement les clusters du
labo et de développer dans le cadre du `projet Fluiddyn
<http://www.legi.grenoble-inp.fr/people/Pierre.Augier/pages/fluiddyn-project.html>`_
des codes Python libres et de bonne qualité utilisables par toute la communauté
de mécanique des fluides expérimentale.
