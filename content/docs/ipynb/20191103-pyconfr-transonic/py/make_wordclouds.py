from pathlib import Path

from wordcloud import WordCloud
import matplotlib.pyplot as plt

words = {
    "Pythran": 5,
    "Cython": 7,
    "Numba": 5,
    "PyPy": 5,
    "Nuitka": 3,
    "Numexp": 3,
    "Cupy": 2,
    "PyTorch": 2,
    "JAX": 2,
    "Weld": 2,
    "Pyccel": 2,
    "Uarray": 2,
}
w = 800
h = 350
size = 4

wordcloud = WordCloud(width=w, height=h, margin=3, background_color="white")
wordcloud.generate_from_frequencies(words)

fig = plt.figure(figsize=(size * w / h, size * h / h))
ax = fig.add_axes([0, 0, 1, 1])
ax.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")

here = Path(__file__).parent.absolute()

tmp = here.parent

tmp.mkdir(exist_ok=True)

fig.savefig(tmp / "tag_clouds.png")
