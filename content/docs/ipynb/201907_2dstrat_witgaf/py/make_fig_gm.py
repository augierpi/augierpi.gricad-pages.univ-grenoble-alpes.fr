
import numpy as np
import matplotlib.pyplot as plt

from util import save, show

plt.xkcd(scale=1, length=100, randomness=2)

fig, ax = plt.subplots()

a = 0.1
b = 1
c = 5
d = 50

kz = np.array([a, b])
norm = kz**(5/3)
ax.loglog(kz, kz**(-2) * norm, "k")

kz = np.array([b, c])
norm = kz**(5/3)
ax.loglog(kz, kz**(-3.) * norm, "k")

m = min(kz**(-3.) * norm)

kz = np.array([c, d])
norm = kz**(5/3)
ax.loglog(kz, m / max(kz**(-5/3.) * norm) * kz**(-5/3.) * norm, "k")

ax.set_xlabel(r"$k_z / k_b$", size=20)
ax.set_ylabel(r"$E(k_z) {k_z}^{5/3}$", size=20)

ax.text(3e-1, 1.6, r"${k_z}^{-2}$", size=20)
ax.text(2.5, 3.5e-1, r"$N^2 {k_z}^{-3}$", size=20)
ax.text(1e1, 1.3e-1, r"${k_z}^{-5/3}$", size=20)

fig.tight_layout()

save(fig, "fig_schetch_gm.png")
show()