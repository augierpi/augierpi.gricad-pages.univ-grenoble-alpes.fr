"""
plot_spectra_gamma.py
======================
Plots the spectra for different gamma.


"""
from pathlib import Path
from util import plot_energy_spectra, save, get_tmin_from_path, show
import matplotlib.pyplot as plt

from fluidsim import load_state_phys_file


def plot_fig(gamma=8.0, directory="march19_sim3840", both=True, ylim=None):

    # Choose a list of times (tmin, tmax) to average the energy spectra.
    # For example
    # times = [(200, None), (400, None), (600, None)]

    # Path
    root_path = Path("/fsnet/project/meige/2015/15DELDUCA/DataSim/")

    path_simulations = sorted((root_path / directory).glob("NS2D.*"))

    for ipath, path in enumerate(path_simulations):
        if not float(path.name.split("_gamma")[1].split("_")[0]) == gamma:
            continue
        else:
            ii = 0
            sim = load_state_phys_file(path)
            tmin = get_tmin_from_path(path)
            fig = plot_energy_spectra(sim, tmin=tmin, tmax=None, both=both)
            ii += 1

    ax = fig.axes[0]
    if ylim is not None:
        ax.set_ylim(ylim)

    path = Path(sim.output.path_run)
    tmp = path.name.split("_gamma")[1].split("_")[0].split(".")
    gamma_not = tmp[0] + tmp[1]
    if "sim3840" in directory:
        name = f"spectra_gamma{gamma_not}.png"
    else:
        name = f"spectra_gamma{gamma_not}_nx{int(sim.params.oper.nx)}.png"
    save(fig, name)

    show()

    return fig
