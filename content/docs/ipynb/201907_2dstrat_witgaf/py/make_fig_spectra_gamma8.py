

from plot_spectra_gamma import plot_fig

gamma = 8.0
plot_fig(gamma=gamma, directory="march19_sim3840", both=True, ylim=(1e-2, 2e1))
