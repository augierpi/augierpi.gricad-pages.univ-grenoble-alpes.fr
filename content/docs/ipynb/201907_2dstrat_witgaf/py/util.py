"""
util.py
=======
"""
import sys
from pathlib import Path

import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from mpl_toolkits.axes_grid1 import make_axes_locatable
from fluidsim.base.output.spect_energy_budget import cumsum_inv
from fluidsim import load_state_phys_file, load_sim_for_plot, load_params_simul

from fluiddyn.output.rcparams import set_rcparams


def get_tmin_from_path(path):
    """Gives tmin given a value of gamma.

    Arguments:
        path {string} -- Path simulation

    Returns:
        tmin {float} -- Value tmin
    """
    gamma = float(path.name.split("gamma")[1].split("_")[0])

    if "march19_sim3840" in path.as_posix():
        if gamma <= 1.0:
            tmin = 100
        elif gamma == 2.0:
            tmin = 400
        elif gamma == 3.0:
            tmin = 600
        elif gamma == 4.0:
            tmin = 700
        elif gamma == 5.0:
            tmin = 500
        elif gamma == 6.0:
            tmin = 500
        elif gamma == 7.0:
            tmin = 500
        elif gamma == 8.0:
            tmin = 500
        elif gamma in [12.0, 16.0]:
            tmin = 100
    elif "may19_sim960" in path.as_posix():
        tmin = 500
    else:
        tmin = 500
    return tmin


def _compute_epsilon_from_path(path_simulation, tmin=None):
    """
    Computes the mean dissipation from tmin
    """
    # Load data dissipation
    path_simulation = Path(path_simulation)
    with open(path_simulation / "spatial_means.txt", "r") as f:
        lines = f.readlines()

    lines_t = []
    lines_epsK = []

    for il, line in enumerate(lines):
        if line.startswith("time ="):
            lines_t.append(line)
        if line.startswith("epsK ="):
            lines_epsK.append(line)

    nt = len(lines_t)
    t = np.empty(nt)
    epsK = np.empty(nt)

    for il in range(nt):
        line = lines_t[il]
        words = line.split()
        t[il] = float(words[2])

        line = lines_epsK[il]
        words = line.split()
        epsK[il] = float(words[2])

    # Compute start time average itmin
    dt_spatial = np.median(np.diff(t))

    if not tmin:
        nb_files = 100
        tmin = np.max(t) - (dt_spatial * nb_files)

    itmin = np.argmin((abs(t - tmin)))

    return np.mean(epsK[itmin:], axis=0)


def _compute_lx_lz_from_path(path_simulation, tmin=None, m=-1):
    """
    Compute horizontal length from path using appendix B. Brethouwer 2007
    """

    # Load parameters from simulation
    params = load_params_simul(path_simulation)

    path_simulation = Path(path_simulation)
    with h5py.File(path_simulation / "spectra1D.h5", "r") as f:
        times_spectra = f["times"].value
        kx = f["kxE"].value
        ky = f["kyE"].value
        spectrum1Dkx_EK = f["spectrum1Dkx_EK"].value
        spectrum1Dky_EK = f["spectrum1Dky_EK"].value

    # Compute time average spectra
    dt = np.median(np.diff(times_spectra))

    if not tmin:
        nb_files = 100
        tmin = np.max(times_spectra) - (dt * nb_files)
    itmin = np.argmin(abs(times_spectra - tmin))

    spectrum1Dkx_EK = np.mean(spectrum1Dkx_EK[-itmin:, :], axis=0)
    spectrum1Dky_EK = np.mean(spectrum1Dky_EK[-itmin:, :], axis=0)

    # Remove modes dealiased
    ikxmax = np.argmin(abs(kx - (np.max(kx) * params.oper.coef_dealiasing)))
    ikymax = np.argmin(abs(ky - (np.max(ky) * params.oper.coef_dealiasing)))

    kx = kx[1:ikxmax]
    ky = ky[1:ikymax]
    spectrum1Dkx_EK = spectrum1Dkx_EK[1:ikxmax]
    spectrum1Dky_EK = spectrum1Dky_EK[1:ikymax]

    delta_kx = kx[1] - kx[0]
    delta_ky = ky[1] - ky[0]

    # Compute horizontal length scale Brethouwer 2007
    lx = (
        np.sum(spectrum1Dkx_EK * delta_kx)
        / np.sum((kx ** (m)) * spectrum1Dkx_EK * delta_kx)
    ) ** (m)

    lz = (
        np.sum(spectrum1Dky_EK * delta_ky)
        / np.sum((ky ** (m)) * spectrum1Dky_EK * delta_ky)
    ) ** (m)

    return lx, lz


def sort_paths_from_gamma(list_simulations):
    """
    Sort list of simulations by the value of gamma.

    Arguments:
        list_simulations {list} -- List of paths. The paths are simulations.
    """
    # Make a list of tuples (gamma, posix path)
    dic = []
    for path in list_simulations:
        gamma = float(path.as_posix().split("gamma")[1].split("_")[0])
        dic.append((gamma, path))

    # Sort by gamma
    dic_new = sorted(dic, key=lambda dic: dic[0])

    # Make a new list
    list_simulations_new = []
    for index, item in enumerate(dic_new):
        list_simulations_new.append(item[1])

    return list_simulations_new


def compute_buoyancy_reynolds(sim, tmin=None):
    """
    Compute the buoyancy Reynolds number.
    """
    params = sim.params
    epsK = _compute_epsilon_from_path(sim.output.path_run, tmin=tmin)
    lx, lz = _compute_lx_lz_from_path(sim.output.path_run, tmin=tmin)

    # F_h = ((epsK / lx ** 2) ** (1 / 3)) * (1 / params.N)
    F_h, F_h_err = compute_horizontal_froude(sim, tmin=None)

    eta_8 = (params.nu_8 ** 3 / epsK) ** (1 / 22)
    Re_8 = (lx / eta_8) ** (22 / 3)

    R_b = Re_8 * F_h ** 8
    return F_h, Re_8, R_b


def compute_horizontal_froude(sim, tmin=None, for_final_plot=False):
    """
    Computes the horizontal Froude number.
    """
    params = sim.params

    # Computation froude Fht = P / (Ux**2 * N)
    P = params.forcing.forcing_rate
    N = params.N
    urms, u_err = _compute_urms_from_simulation(sim, tmin=tmin)
    Fh_err = 2 * u_err

    # Other way to compute Fh
    lx, lz = _compute_lx_lz_from_path(sim.output.path_run, tmin=tmin, m=1)

    # Two ways to compute Reynolds
    Fh = urms ** 2 / (lx * N)
    if for_final_plot:
        Fh = P / (urms ** 2 * N)

    return Fh, Fh_err


def _compute_urms_from_simulation(sim, tmin=None):
    """
    Computes rms of the horizontal velocity from a simulation.

    Arguments:
        path_simulation {Posix path} -- Path of the simulation

    Keyword Arguments:
        tmin {float} -- Minimum time to compute the average.
    """

    # Load spatial means file and kinetic energy
    dict_spatial = sim.output.spatial_means.load()
    EK = dict_spatial["EK"]
    times = dict_spatial["t"]

    if tmin is None:
        itmin = 10  # 10 last files
    else:
        itmin = np.argmin(abs(times - tmin))

    # Compute error
    u_err = np.std(EK[itmin:] ** (1 / 2))
    # u_err = np.sqrt(np.mean(EK[itmin:]))

    # Compute urms
    urms = np.sqrt(np.mean(EK[itmin:], axis=0))

    return urms, u_err


def plot_buoyancy_field(path, time_plot=None, clim=None, sec="stratification"):

    if time_plot is None:
        time_plot = 1e4

    # Load simulation
    sim = load_sim_for_plot(path)

    # Create list with times
    path_fields = sorted(path.glob("state*"))
    times = np.empty(len(path_fields))
    for i, path in enumerate(path_fields):
        time_str = path.as_posix().split("state_phys_t")[1]
        if "it=" in time_str:
            pass
        else:
            times[i] = float(time_str.split(".nc")[0])

    # Index close to time_plot
    index_plot = np.argmin(abs(times - time_plot))

    # Parameters figure
    set_rcparams(fontsize=14, for_article=True)

    # plt.rcParams["text.usetex"] = True
    # plt.rcParams["font.family"] = "sans-serif"
    # plt.rcParams["font.size"] = 16.0

    fig, ax = plt.subplots()
    ax.set_xlabel("$x$", fontsize=16)
    ax.set_ylabel("$z$", fontsize=16)
    ax.tick_params(axis="x", labelsize=16)
    ax.tick_params(axis="y", labelsize=16)

    if sec in ["occigen", "zoom_occigen"]:
        from matplotlib.ticker import FormatStrFormatter

        ax.yaxis.set_major_formatter(FormatStrFormatter("%.3f"))

    # I do not put tick labels when sec==reynolds.
    # Because the figure with factor_domain=16 is smaller than the others.
    if sec == "reynolds":
        ax.set_yticklabels([])

    cmap = "magma"

    with h5py.File(path_fields[index_plot], "r") as f:
        field = f["state_phys"]["b"].value
        ux = f["state_phys"]["ux"].value
        uy = f["state_phys"]["uy"].value

    x_seq = np.arange(
        0, sim.params.oper.Lx, sim.params.oper.Lx / sim.params.oper.nx
    )
    y_seq = np.arange(
        0, sim.params.oper.Ly, sim.params.oper.Ly / sim.params.oper.ny
    )

    [XX_seq, YY_seq] = np.meshgrid(x_seq, y_seq)

    if sec in ["stratification", "reynolds", "occigen"]:
        pc = ax.pcolormesh(
            XX_seq,
            YY_seq,
            field / sim.params.N,
            cmap=cmap,
            clim=clim,
            vmin=clim[0],
            vmax=clim[1],
        )
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cax.tick_params(labelsize=14)
    ax.text(6.42, -0.4, r"$\frac{b}{N}$", fontsize=16)
    ax.set_aspect("equal")
    if clim is None:
        clim = (-10, 10)
    if sec in ["stratification", "reynolds", "occigen"]:
        fig.colorbar(pc, cax=cax, format="%.1f", ticks=[clim[0], clim[1]])
        pc.set_clim(clim[0], clim[1])

    # Text box with the value of gamma
    if sec in ["stratification", "occigen"]:
        text = f"$\\gamma = {sim.output.ratio_omegas:.1f}$"
        props = dict(boxstyle="square", facecolor="white", alpha=1)
        ax.text(0.02 * sim.oper.Lx, 0.8 * sim.oper.Ly, text, bbox=props)

    elif sec == "reynolds":
        nu_8 = sim.params.nu_8
        forcing_rate = sim.params.forcing.forcing_rate
        # Plot forcing wave-number k_f
        nkmax = sim.params.forcing.nkmax_forcing
        nkmin = sim.params.forcing.nkmin_forcing
        kf = ((nkmax + nkmin) / 2) * sim.oper.deltak
        Re8 = ((forcing_rate / kf ** 22) ** (1 / 3)) * (1 / nu_8)
        # text = rf"$\\Re_{{8,f}} = {Re8:.1e}$"
        text = rf"${sim.params.oper.nx}\times{sim.params.oper.ny}$"
        props = dict(boxstyle="square", facecolor="white", alpha=1)
        ax.text(0.02 * sim.oper.Lx, 0.8 * sim.oper.Ly, text, bbox=props)

    if sec in ["stratification", "reynolds"]:
        return fig

    if sec == "occigen":
        # Add zoom 1

        x0 = 2.5
        y0 = 0.1
        xy = (x0, y0)
        width = 1.0
        height = width / 4.0
        p = patches.Rectangle(
            xy, width, height, fill=False, linewidth=1.2, color="white"
        )
        ax.add_patch(p)
        # First zoom
        fig1, ax1 = plt.subplots()
        ax1.set_xlabel("$x$", fontsize=16)
        ax1.set_ylabel("$z$", fontsize=16)
        ax1.tick_params(axis="x", labelsize=16)
        ax1.tick_params(axis="y", labelsize=16)
        ax1.set_xlim(left=x0, right=x0 + width)
        ax1.set_ylim(bottom=y0, top=y0 + height)
        pc1 = ax1.pcolormesh(
            XX_seq,
            YY_seq,
            field / sim.params.N,
            cmap=cmap,
            clim=clim,
            vmin=clim[0],
            vmax=clim[1],
        )
        divider = make_axes_locatable(ax1)
        cax1 = divider.append_axes("right", size="5%", pad=0.05)
        cax1.tick_params(labelsize=14)
        ax1.text(3.52, 0.03, r"$\frac{b}{N}$", fontsize=16)

        ax1.yaxis.set_major_formatter(FormatStrFormatter("%.3f"))

        fig1.colorbar(pc1, cax=cax1, format="%.1f", ticks=[clim[0], clim[1]])
        ax1.set_aspect("equal")

        # Add rectangle
        x0 = 2.8
        y0 = 0.18
        xy = (x0, y0)
        width = 0.5
        height = width / 4.0
        p1 = patches.Rectangle(
            xy, width, height, fill=False, linewidth=1.2, color="white"
        )
        ax1.add_patch(p1)

    if sec == "zoom_occigen":
        x0 = 2.8
        y0 = 0.18
        xy = (x0, y0)
        width = 0.5
        height = width / 4.0
        # First zoom
        fig2, ax2 = plt.subplots()
        ax2.set_xlabel("$x$", fontsize=16)
        ax2.set_ylabel("$z$", fontsize=16)
        ax2.tick_params(axis="x", labelsize=16)
        ax2.tick_params(axis="y", labelsize=16)
        ax2.set_xlim(left=x0, right=x0 + width)
        ax2.set_ylim(bottom=y0, top=y0 + height)
        pc2 = ax2.pcolormesh(
            XX_seq,
            YY_seq,
            field / sim.params.N,
            cmap=cmap,
            clim=clim,
            vmin=clim[0],
            vmax=clim[1],
        )
        divider = make_axes_locatable(ax2)
        cax2 = divider.append_axes("right", size="5%", pad=0.05)
        cax2.tick_params(labelsize=14)
        ax2.text(3.32, 0.14, r"$\frac{b}{N}$", fontsize=16)
        fig2.colorbar(pc2, cax=cax2, format="%.1f", ticks=[clim[0], clim[1]])
        ax2.set_aspect("equal")
        ax2.yaxis.set_major_formatter(FormatStrFormatter("%.3f"))

        # Add rectangle
        x0 = 3
        y0 = 0.2
        xy = (x0, y0)
        width = 0.2
        height = width / 4.0
        p1 = patches.Rectangle(
            xy, width, height, fill=False, linewidth=1.2, color="white"
        )
        ax2.add_patch(p1)

        # First zoom
        fig3, ax3 = plt.subplots()
        ax3.set_xlabel("$x$", fontsize=16)
        ax3.set_ylabel("$z$", fontsize=16)
        ax3.tick_params(axis="x", labelsize=16)
        ax3.tick_params(axis="y", labelsize=16)
        ax3.set_xlim(left=x0, right=x0 + width)
        ax3.set_ylim(bottom=y0, top=y0 + height)
        pc3 = ax3.pcolormesh(
            XX_seq,
            YY_seq,
            field / sim.params.N,
            cmap=cmap,
            clim=clim,
            vmin=clim[0],
            vmax=clim[1],
        )
        divider = make_axes_locatable(ax3)
        cax3 = divider.append_axes("right", size="5%", pad=0.05)
        cax3.tick_params(labelsize=14)
        ax3.text(3.21, 0.185, r"$\frac{b}{N}$", fontsize=16)
        fig3.colorbar(pc3, cax=cax3, format="%.1f", ticks=[clim[0], clim[1]])
        ax3.set_aspect("equal")
        ax3.yaxis.set_major_formatter(FormatStrFormatter("%.3f"))

        # # First zoom
        # fig2, ax2 = plt.subplots()
        # ax2.set_xlabel("$x$", fontsize=16)
        # ax2.set_ylabel("$z$", fontsize=16)
        # ax2.tick_params(axis="x", labelsize=16)
        # ax2.tick_params(axis="y", labelsize=16)
        # ax2.set_xlim(left=x0, right=x0+width)
        # ax2.set_ylim(bottom=y0, top=y0 + height)
        # pc2 = ax1.pcolormesh(XX_seq, YY_seq, field / sim.params.N, cmap=cmap, clim=clim)
        # divider = make_axes_locatable(ax1)
        # cax1 = divider.append_axes("right", size="5%", pad=0.05)
        # cax1.tick_params(labelsize=14)
        # ax1.text(3.52, 0.03, r"$\frac{b}{N}$", fontsize=16)

        # fig1.colorbar(pc1, cax=cax1, format="%.1f", ticks=[clim[0], clim[1]])
        # ax1.set_aspect("equal")
    if sec == "occigen":
        return fig, fig1
    if sec == "zoom_occigen":
        return fig2, fig3

        # # Zoom 2
        # x0 = 3
        # y0 = 0.18
        # xy = (x0,y0)
        # width = 0.5
        # height = width / 16.
        # p1 = patches.Rectangle(xy, width, height, fill=False, linewidth=1.2, color="black")
        # ax1.add_patch(p1)
        # # First zoom
        # fig2, ax2 = plt.subplots()
        # ax2.set_xlabel("$x$", fontsize=16)
        # ax2.set_ylabel("$z$", fontsize=16)
        # ax2.tick_params(axis="x", labelsize=16)
        # ax2.tick_params(axis="y", labelsize=16)
        # ax2.set_xlim(left=x0, right=x0+width)
        # ax2.set_ylim(bottom=y0, top=y0 + height)
        # pc2 = ax2.pcolormesh(XX_seq, YY_seq, field / sim.params.N, cmap=cmap, clim=clim)
        # divider = make_axes_locatable(ax2)
        # cax2 = divider.append_axes("right", size="5%", pad=0.05)
        # cax2.tick_params(labelsize=14)
        # ax2.text(6.42, -0.4, r"$\frac{b}{N}$", fontsize=16)

        # fig2.colorbar(pc2, cax=cax2, format="%.1f", ticks=[clim[0], clim[1]])
        # ax2.set_aspect("equal")

        # fig.tight_layout(pad=0.001)

        # return fig
        return fig, fig1


def plot_energy_spectra(sim, tmin=None, tmax=None, both=True):
    """Plot energy spectra."""

    plt.rcParams["text.usetex"] = True
    plt.rcParams["font.family"] = "sans-serif"
    plt.rcParams["font.size"] = 20.0

    fig3, ax3 = plt.subplots()
    ax3.set_xscale("log")
    ax3.set_yscale("log")
    if both:
        ax3.set_xlabel("$k_x/k_{f,x}, k_z/k_{f,z}$")
    else:
        ax3.set_xlabel("$k_z/k_{f,z}$")
    if both:
        ax3.set_ylabel(r"$E(k)k^{5/3}$")
    else:
        ax3.set_ylabel(r"$E(k_z)k_z^{5/3}$")

    ax3.tick_params(axis="x")
    ax3.tick_params(axis="y")
    ax3.set_ylim(bottom=1e-3, top=7e0)
    ax3.set_xlim(left=4e-2, right=1.5e2)

    if "gamma8.0" in sim.output.path_run:
        ax3.set_ylim(bottom=5e-3, top=5e1)

    # Load energy spectra data
    with h5py.File(sim.output.spectra.path_file1D, "r") as f:
        times = f["times"].value
        if tmin is None:
            tmin = times[0]
        if tmax is None:
            tmax = times[-1]
        imin_plot = np.argmin(abs(times - tmin))
        imax_plot = np.argmin(abs(times - tmax))
        print("imin_plot = ", imin_plot)
        print("imax_plot = ", imax_plot)
        kx = f["kxE"].value
        ky = f["kyE"].value

        dset_spectrum1Dkx_E = f["spectrum1Dkx_E"]
        dset_spectrum1Dky_E = f["spectrum1Dky_E"]
        dset_spectrum1Dkx_EK = f["spectrum1Dkx_EK"]
        dset_spectrum1Dky_EK = f["spectrum1Dky_EK"]
        dset_spectrum1Dkx_EA = f["spectrum1Dkx_EA"]
        dset_spectrum1Dky_EA = f["spectrum1Dky_EA"]

        E_kx = (dset_spectrum1Dkx_E[imin_plot : imax_plot + 1]).mean(0)
        EK_kx = (dset_spectrum1Dkx_EK[imin_plot : imax_plot + 1]).mean(0)
        EA_kx = (dset_spectrum1Dkx_EA[imin_plot : imax_plot + 1]).mean(0)

        E_ky = (dset_spectrum1Dky_E[imin_plot : imax_plot + 1]).mean(0)
        EK_ky = (dset_spectrum1Dky_EK[imin_plot : imax_plot + 1]).mean(0)
        EA_ky = (dset_spectrum1Dky_EA[imin_plot : imax_plot + 1]).mean(0)

    # Remove modes dealiased.
    kxmax_dealiasing = sim.params.oper.coef_dealiasing * np.max(abs(kx))
    kymax_dealiasing = sim.params.oper.coef_dealiasing * np.max(abs(ky))

    id_kx_dealiasing = np.argmin(abs(kx - kxmax_dealiasing)) - 1
    id_ky_dealiasing = np.argmin(abs(ky - kymax_dealiasing)) - 1

    E_kx_plot = E_kx[:id_kx_dealiasing]
    EK_kx_plot = EK_kx[:id_kx_dealiasing]
    EA_kx_plot = EA_kx[:id_kx_dealiasing]
    kx_plot = kx[:id_kx_dealiasing]

    E_ky_plot = E_ky[:id_ky_dealiasing]
    EK_ky_plot = EK_ky[:id_ky_dealiasing]
    EA_ky_plot = EA_ky[:id_ky_dealiasing]
    ky_plot = ky[:id_ky_dealiasing]

    # Remove shear modes if there is no energy on them.
    if sim.params.oper.NO_SHEAR_MODES:
        E_kx_plot = E_kx_plot[1:]
        EK_kx_plot = EK_kx_plot[1:]
        EA_kx_plot = EA_kx_plot[1:]
        kx_plot = kx_plot[1:]

        E_ky_plot = E_ky_plot[1:]
        EK_ky_plot = EK_ky_plot[1:]
        EA_ky_plot = EA_ky_plot[1:]
        ky_plot = ky_plot[1:]

    # Plot forcing wave-number k_f
    nkmax = sim.params.forcing.nkmax_forcing
    nkmin = sim.params.forcing.nkmin_forcing
    k_f = ((nkmax + nkmin) / 2) * sim.oper.deltak
    pforcing = sim.params.forcing
    if pforcing.enable and pforcing.type == "tcrandom_anisotropic":
        angle = pforcing.tcrandom_anisotropic.angle
        try:
            if angle.endswith("°"):
                angle = np.pi / 180 * float(angle[:-1])
        except AttributeError:
            pass
        k_fx = np.sin(angle) * k_f
        k_fy = np.cos(angle) * k_f
        # Band forcing region kx
        k_fxmin = nkmin * sim.oper.deltak * np.sin(angle)
        k_fxmax = nkmax * sim.oper.deltak * np.sin(angle)
        # Band forcing region ky
        k_fymin = nkmin * sim.oper.deltak * np.cos(angle)
        k_fymax = nkmax * sim.oper.deltak * np.cos(angle)

        k_fx = (k_fxmin + k_fxmax) / 2
        k_fz = (k_fymin + k_fymax) / 2

        # Plot forcing band
        ax3.axvspan(k_fxmin / k_fx, k_fxmax / k_fx, alpha=0.15, color="black")
        ax3.axvspan(k_fymin / k_fy, k_fymax / k_fz, alpha=0.15, color="black")

    # Horizontal and vertical
    if both:
        ax3.plot(
            kx_plot / k_fx,
            EK_kx_plot * (kx_plot) ** (5 / 3.0),
            color="orange",
            label=r"$E_{K}(k_x)$",
            linewidth=2,
        )
        ax3.plot(
            kx_plot / k_fx,
            EA_kx_plot * (kx_plot) ** (5 / 3.0),
            color="blue",
            label=r"$E_{A}(k_x)$",
            linewidth=2,
        )

    ax3.plot(
        ky_plot / k_fz,
        EK_ky_plot * (ky_plot) ** (5 / 3.0),
        color="orange",
        label=r"$E_{K}(k_z)$",
        linewidth=2,
        linestyle="--",
    )
    ax3.plot(
        ky_plot / k_fz,
        EA_ky_plot * (ky_plot) ** (5 / 3.0),
        color="blue",
        label=r"$E_{A}(k_z)$",
        linewidth=2,
        linestyle="--",
    )

    # Plot scalings
    kxmin_scaling = 30
    kxmax_scaling = 500
    imin_scale = np.argmin(abs(kx_plot - kxmin_scaling))
    imax_scale = np.argmin(abs(kx_plot - kxmax_scaling))
    ax3.plot(
        kx_plot[imin_scale:imax_scale] / k_fx,
        (kx_plot[imin_scale:imax_scale] / k_fx) ** (-5 / 3.0)
        * (kx_plot[imin_scale:imax_scale] / k_fx) ** (5 / 3.0),
        color="black",
        linestyle="-.",
    )
    ax3.plot(
        kx_plot[imin_scale:imax_scale] / k_fx,
        10
        * (kx_plot[imin_scale:imax_scale] / k_fx) ** (-3.0)
        * (kx_plot[imin_scale:imax_scale] / k_fx) ** (5 / 3.0),
        color="black",
        linestyle="-.",
    )

    ax3.text(20, 1.5, r"$k^{-5/3}$")
    ax3.text(2.4, 3.5, r"$k^{-3}$")
    # ax3.text(6, 0.04, r"$k^{-2}$")

    # Plot the buoyancy scale
    U, uerr = _compute_urms_from_simulation(sim, tmin=tmin)
    k_b = sim.params.N / U
    ax3.axvline(x=k_b / k_fx, color="black", linestyle=":")

    # Plot ozmidov scale
    k_o = (sim.params.N ** 3 / sim.params.forcing.forcing_rate) ** (1 / 2)
    ax3.axvline(x=k_o / k_fx, color="black", linestyle=":")

    # Plot scaling U * N * KZ**{-2}
    ax3.plot(
        ky_plot / k_fz,
        0.02
        * sim.params.N
        * U
        * (ky_plot / k_fz) ** (-2)
        * (ky_plot / k_fz) ** (5 / 3),
        color="red",
        linewidth=2,
        linestyle="--",
    )
    # if "sim3840" in sim.output.path_run and "gamma2.0" in sim.output.path_run:
    if sim.params.oper.nx == 3840:
        if "gamma0.5" in sim.output.path_run:
            ax3.set_ylim(bottom=0.003, top=8)
            ax3.text(0.05, 0.07, r"$C_1UNk_z^{-2}$", color="red")
            ax3.legend(loc=2, fontsize=16)
            ax3.text((k_b / k_fx) + 0.05, 4e-3, r"$k_b$")
            ax3.text((k_o / k_fx) + 0.05, 4e-3, r"$k_o$")
        elif "gamma2.0" in sim.output.path_run:
            ax3.set_ylim(bottom=0.01, top=10)
            ax3.text(0.05, 0.4, r"$C_1UNk_z^{-2}$", color="red")
            ax3.text((k_o / k_fx) + 2, 2e-2, r"$k_o$")
            ax3.text((k_b / k_fx) + 0.05, 2e-2, r"$k_b$")
        elif "gamma8.0" in sim.output.path_run:
            ax3.set_ylim(bottom=0.01, top=15)
            ax3.text(0.05, 2, r"$C_1UNk_z^{-2}$", color="red")
            ax3.text((k_o / k_fx) - 50, 2e-2, r"$k_o$")
            ax3.text((k_b / k_fx) + 0.05, 2e-2, r"$k_b$")

    if sim.params.oper.nx == 3840:
        if not both:
            ax3.set_ylim(bottom=1e-2, top=2e1)
            ax3.text(
                (ky_plot[0] / k_fz) * 0.25,
                0.02
                * sim.params.N
                * U
                * (ky_plot[0] / k_fz) ** (-2)
                * (ky_plot[0] / k_fz) ** (5 / 3),
                r"$C_1UNk_z^{-2}$",
                color="red",
            )
            if not "gamma12.0" in sim.output.path_run:
                ax3.text((k_o / k_fz) + 2, 2e-2, r"$k_o$")
            ax3.text((k_b / k_fz) + 0.05, 2e-2, r"$k_b$")
        else:
            pass

    if sim.params.oper.nx == 7680:
        ax3.text(
            (ky_plot[0] / k_fz) * 0.15,
            0.02
            * sim.params.N
            * U
            * (ky_plot[0] / k_fz) ** (-2)
            * (ky_plot[0] / k_fz) ** (5 / 3),
            r"$C_1UNk_z^{-2}$",
            color="red",
        )
        ax3.text((k_b / k_fx) + 0.05, 2e-2, r"$k_b$")

    if sim.params.oper.nx == 960:
        ax3.text(
            (ky_plot[-1] / k_fz),
            0.02
            * sim.params.N
            * U
            * (ky_plot[-1] / k_fz) ** (-2)
            * (ky_plot[-1] / k_fz) ** (5 / 3),
            r"$C_1UNk_z^{-2}$",
            color="red",
        )
        ax3.text((k_b / k_fx) + 0.05, 2e-2, r"$k_b$")

    if "gamma8.0" in sim.output.path_run and sim.params.oper.nx == 960:
        ax3.legend(loc=2, fontsize=16)

    # Plot characteristic vertical scale
    lx, lz = _compute_lx_lz_from_path(sim.output.path_run, tmin=tmin)
    k_vertical = 1.0 / lz
    ax3.plot(
        [k_vertical / k_fz, k_vertical / k_fz],
        [ax3.get_ylim()[0], ax3.get_ylim()[0] + (ax3.get_ylim()[0] * 0.5)],
        color="green",
        linewidth=4,
    )

    # ax3.scatter(k_vertical / k_fz, ax3.get_ylim()[0], marker="o", color="green")

    fig3.tight_layout()

    return fig3


def plot_flux(sim, tmin=None, tmax=None):
    """Plots spectral energy fluxes."""

    plt.rcParams["text.usetex"] = True
    plt.rcParams["font.family"] = "sans-serif"
    plt.rcParams["font.size"] = 20.0
    fig1, ax1 = plt.subplots()
    ax1.set_xlabel(r"$k_x/k_{f,x}$")
    ax1.set_ylabel(r"$\Pi(k_x)/\epsilon$")
    ax1.set_xscale("log")
    ax1.set_yscale("linear")
    ax1.tick_params(axis="x")
    ax1.tick_params(axis="y")
    ax1.axhline(y=0, color="k", linestyle="--", linewidth=1)

    fig2, ax2 = plt.subplots()
    ax2.set_xlabel(r"$k_z/k_{f,z}$")
    ax2.set_ylabel(r"$\Pi (k_z)/\epsilon$")
    ax2.set_xscale("log")
    ax2.set_yscale("linear")
    ax2.tick_params(axis="x")
    ax2.tick_params(axis="y")
    ax2.axhline(y=0, color="k", linestyle="--", linewidth=1)

    if sim.params.oper.nx == 3840:
        ax1.set_xlim(left=5e-2, right=1e2)
        ax2.set_xlim(left=5e-2, right=1e2)

    else:
        ax1.set_xlim(left=5e-2, right=2e2)
        ax2.set_xlim(left=5e-2, right=2e2)

    with h5py.File(sim.output.spect_energy_budg.path_file, "r") as f:

        times = f["times"].value
        if tmin is None:
            tmin = tmin_spatial = times[0]
        if tmax is None:
            tmax = tmax_spatial = times[-1]
        imin_plot = np.argmin(abs(times - tmin))
        imax_plot = np.argmin(abs(times - tmax))

        kx = f["kxE"].value
        ky = f["kyE"].value

        dset_transferEK_kx = f["transferEK_kx"]
        dset_transferEK_ky = f["transferEK_ky"]
        dset_transferEA_kx = f["transferEA_kx"]
        dset_transferEA_ky = f["transferEA_ky"]

        dset_dissEKu_kx = f["dissEKu_kx"]
        dset_dissEKu_ky = f["dissEKu_ky"]
        dset_dissEKv_kx = f["dissEKv_kx"]
        dset_dissEKv_ky = f["dissEKv_ky"]
        dset_dissEA_kx = f["dissEA_kx"]
        dset_dissEA_ky = f["dissEA_ky"]

        dset_B_kx = f["B_kx"]
        dset_B_ky = f["B_ky"]

        transferEK_kx = dset_transferEK_kx[imin_plot:imax_plot].mean(0)
        transferEA_kx = dset_transferEA_kx[imin_plot:imax_plot].mean(0)
        transferEK_ky = dset_transferEK_ky[imin_plot:imax_plot].mean(0)
        transferEA_ky = dset_transferEA_ky[imin_plot:imax_plot].mean(0)

        dissEK_kx = (
            dset_dissEKu_kx[imin_plot:imax_plot]
            + dset_dissEKv_kx[imin_plot:imax_plot]
        ).mean(0)
        dissEK_ky = (
            dset_dissEKu_ky[imin_plot:imax_plot]
            + dset_dissEKv_ky[imin_plot:imax_plot]
        ).mean(0)
        dissEA_kx = dset_dissEA_kx[imin_plot:imax_plot].mean(0)
        dissEA_ky = dset_dissEA_ky[imin_plot:imax_plot].mean(0)

        b_kx = dset_B_kx[imin_plot:imax_plot].mean(0)
        b_ky = dset_B_ky[imin_plot:imax_plot].mean(0)

        PiEK_kx = cumsum_inv(transferEK_kx) * sim.oper.deltakx
        PiEA_kx = cumsum_inv(transferEA_kx) * sim.oper.deltakx
        PiEK_ky = cumsum_inv(transferEK_ky) * sim.oper.deltaky
        PiEA_ky = cumsum_inv(transferEA_ky) * sim.oper.deltaky

        DEK_kx = dissEK_kx.cumsum() * sim.oper.deltakx
        DEA_kx = dissEA_kx.cumsum() * sim.oper.deltakx
        DEK_ky = dissEK_ky.cumsum() * sim.oper.deltaky
        DEA_ky = dissEA_ky.cumsum() * sim.oper.deltaky

        B_kx = cumsum_inv(b_kx) * sim.oper.deltakx
        B_ky = cumsum_inv(b_ky) * sim.oper.deltaky

    # Compute time average dissipation to normalize fluxes
    dico_results = sim.output.spatial_means.load()

    t = dico_results["t"]
    epsK_tot = dico_results["epsK_tot"]
    epsA_tot = dico_results["epsA_tot"]
    eps_tot = epsK_tot + epsA_tot

    imin_plot_spatial_times = np.argmin(abs(t - tmin))
    imax_plot_spatial_times = np.argmin(abs(t - tmax))

    eps_tot_time_average = eps_tot[
        imin_plot_spatial_times:imax_plot_spatial_times
    ].mean(0)
    print("eps_tot_time_average = {}".format(eps_tot_time_average))

    # Plot forcing wave-number k_f
    nkmax = sim.params.forcing.nkmax_forcing
    nkmin = sim.params.forcing.nkmin_forcing
    k_f = ((nkmax + nkmin) / 2) * sim.oper.deltak
    pforcing = sim.params.forcing
    if pforcing.enable and pforcing.type == "tcrandom_anisotropic":
        angle = pforcing.tcrandom_anisotropic.angle
        try:
            if angle.endswith("°"):
                angle = np.pi / 180 * float(angle[:-1])
        except AttributeError:
            pass
        k_fx = np.sin(angle) * k_f
        k_fy = np.cos(angle) * k_f
        # ax1.axvline(x=k_fx, color="y", linestyle="-.", label="$k_{f,x}$")
        # ax2.axvline(x=k_fy, color="y", linestyle="-.", label="$k_{f,z}$")
        # Band forcing region kx
        k_fxmin = nkmin * sim.oper.deltak * np.sin(angle)
        k_fxmax = nkmax * sim.oper.deltak * np.sin(angle)

        # Band forcing region ky
        k_fymin = nkmin * sim.oper.deltak * np.cos(angle)
        k_fymax = nkmax * sim.oper.deltak * np.cos(angle)

        k_fx = (k_fxmin + k_fxmax) / 2
        k_fz = (k_fymin + k_fymax) / 2

        ax1.axvspan(k_fxmin / k_fx, k_fxmax / k_fx, alpha=0.15, color="black")
        ax2.axvspan(k_fymin / k_fz, k_fymax / k_fz, alpha=0.15, color="black")

    # Plot Horizontal fluxes...
    ax1.plot(
        kx / k_fx,
        (PiEK_kx + PiEA_kx) / eps_tot_time_average,
        label=r"$\Pi/\epsilon$",
        linewidth=2,
        color="black",
        linestyle="--",
    )
    ax1.plot(
        kx / k_fx,
        PiEK_kx / eps_tot_time_average,
        label=r"$\Pi_K/\epsilon$",
        linewidth=2,
        color="orange",
    )
    ax1.plot(
        kx / k_fx,
        PiEA_kx / eps_tot_time_average,
        label=r"$\Pi_A/\epsilon$",
        linewidth=2,
        color="blue",
    )
    ax1.plot(
        kx / k_fx,
        (DEK_kx + DEA_kx) / eps_tot_time_average,
        label=r"$D/\epsilon$",
        linewidth=2,
        color="green",
    )
    if "sim3840" in sim.output.path_run:
        ax1.plot(
            kx / k_fx,
            B_kx / eps_tot_time_average,
            label=r"$B/\epsilon$",
            linewidth=2,
            color="cyan",
        )

    # Plot vertical fluxes...
    ax2.plot(
        ky / k_fz,
        (PiEK_ky + PiEA_ky) / eps_tot_time_average,
        label=r"$\Pi/\epsilon$",
        linewidth=2,
        color="black",
        linestyle="--",
    )
    ax2.plot(
        ky / k_fz,
        PiEK_ky / eps_tot_time_average,
        label=r"$\Pi_K/\epsilon$",
        linewidth=2,
        color="orange",
    )
    ax2.plot(
        ky / k_fz,
        PiEA_ky / eps_tot_time_average,
        label=r"$\Pi_A/\epsilon$",
        linewidth=2,
        color="blue",
    )
    ax2.plot(
        ky / k_fz,
        (DEK_ky + DEA_ky) / eps_tot_time_average,
        label=r"$D/\epsilon$",
        linewidth=2,
        color="green",
    )
    if "sim3840" in sim.output.path_run:
        ax2.plot(
            ky / k_fz,
            B_ky / eps_tot_time_average,
            label=r"$B/\epsilon$",
            linewidth=2,
            color="cyan",
        )

    # Plot the buoyancy scale
    U = np.sqrt(np.mean(abs(sim.state.get_var("ux")) ** 2))
    k_b = sim.params.N / U
    ax1.axvline(x=k_b / k_fx, color="black", linestyle=":")
    ax2.axvline(x=k_b / k_fz, color="black", linestyle=":")

    ax1.text((k_b / k_fx) + 0.008, 1, r"$k_b$")
    ax2.text((k_b / k_fz) + 0.008, 1.2, r"$k_b$")

    # Plot ozmidov scale
    k_o = (sim.params.N ** 3 / sim.params.forcing.forcing_rate) ** (1 / 2)

    # For gamma = 8.0, the ko is on the left of the plot.
    # There is no need to plot it.
    if sim.output.ratio_omegas < 7.5:
        ax1.text((k_o / k_fx) + 0.5, 1, r"$k_o$")
        ax2.text((k_o / k_fz) + 0.5, 1.2, r"$k_o$")
        ax1.axvline(x=k_o / k_fx, color="black", linestyle=":")
        ax2.axvline(x=k_o / k_fz, color="black", linestyle=":")

    ax1.legend(loc=2)
    # ax2.legend(loc=2)

    return fig1, fig2


def add_simulations_occigen(path_simulations):
    """
    Add simulations Occigen into list path_simulations.
    """
    root_path = Path("/fsnet/project/meige/2015/15DELDUCA/")
    directory = "Miguel_OccigenData/Steady"

    for path in sorted((root_path / directory).glob("NS2D.strat_*")):
        path_simulations.append(path)

    return path_simulations


here = Path(__file__).parent.absolute()


def save(fig, name):

    root_save = here.parent / "pyfig"

    if "save" in sys.argv:
        print("Saving figure", name)
        fig.savefig(root_save / name, bbox_inches="tight")


def show():
    if "save" not in sys.argv:
        plt.show()
