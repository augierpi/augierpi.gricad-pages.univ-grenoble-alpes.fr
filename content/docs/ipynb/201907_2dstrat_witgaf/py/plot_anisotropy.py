"""
plot_anisotropy.py
=====================
25 / 04 / 2019

Function to compute the anisotropy of a simulation.

"""

import h5py
import numpy as np
import matplotlib.pyplot as plt

from util import sort_paths_from_gamma, get_tmin_from_path, save, show
from pathlib import Path
from fluidsim import load_params_simul, load_sim_for_plot


def _compute_array_times_from_path(path_simulation):
    """
    Compute array with times from path simulation.

    Parameters
    ----------
    path_simulation : str
      Path of the simulation.
    """
    times_phys_files = []

    paths_phys_files = sorted(path_simulation.glob("state_phys_t*"))
    for path in paths_phys_files:
        path = path.as_posix()
        if not "=" in path.split("state_phys_t")[1].split(".nc")[0]:
            times_phys_files.append(
                float(path.split("state_phys_t")[1].split(".nc")[0])
            )
        else:
            continue
    return np.asarray(times_phys_files)


def compute_isotropy(path_simulation, tmin=None):
    """
    It computes the anisotropy of a simulation.

    The anisotropy is defined as:

    ::math EK_ux / EK

    Parameters
    ----------
    path_simulation : str
      Path of the simulation.

    tmin : float
      Lower limit to compute the time average.
      By default it takes the last 10 files.
    """

    # Print out
    path_simulation = path_simulation.as_posix()
    res_out = float(path_simulation.split("NS2D.strat_")[1].split("x")[0])
    gamma_str = path_simulation.split("_gamma")[1].split("_")[0]

    gamma_out = float(gamma_str)

    print(f"Compute anisotropy nx = {res_out} and gamma {gamma_out}..")

    # Load data energy spectra file.
    with h5py.File(path_simulation + "/spectra1D.h5", "r") as f:
        kx = f["kxE"].value
        times = f["times"].value
        spectrumkx_EK_ux = f["spectrum1Dkx_EK_ux"].value
        spectrumkx_EK = f["spectrum1Dkx_EK"].value
        spectrumkx_EK_uy = f["spectrum1Dkx_EK_uy"].value
        dt_state_phys = np.median(np.diff(times))

    # Compute start time average itmin
    dt = np.median(np.diff(times))
    if not tmin:
        nb_files = 10
        tmin = np.max(times) - (nb_files * dt)
    itmin = np.argmin(abs(times - tmin))

    # Compute delta_kx
    params = load_params_simul(path_simulation)
    delta_kx = 2 * np.pi / params.oper.Lx

    # Compute spatial averaged energy from spectra
    EK_ux = np.sum(np.mean(spectrumkx_EK_ux[itmin:, :], axis=0) * delta_kx)
    EK = np.sum(np.mean(spectrumkx_EK[itmin:, :], axis=0) * delta_kx)

    # Compute error isotropy.
    EK_ux_arr = np.sum(spectrumkx_EK_ux[itmin:, :] * delta_kx, axis=1)
    EK_arr = np.sum(spectrumkx_EK[itmin:, :] * delta_kx, axis=1)

    err_EK_ux = np.std(EK_ux_arr)
    err_EK = np.std(EK_arr)

    # isotropy_err = 2 * (err_EK_ux + err_EK)
    isotropy_err = err_EK
    return 4 * ((EK_ux / EK) - 1) ** 2, isotropy_err


def plot_fig(directories=["march19_sim3840"]):

    # directories = ["march19_sim3840"]
    # directories = ["may19_sim960", "may19_sim1920", "march19_sim3840"]
    ####

    # Root paths
    root_save = Path("/home/users/calpelin7m/Repos/phd_miguelcalpelinares")
    root_path = Path("/fsnet/project/meige/2015/15DELDUCA/DataSim/")

    # Plot parameters
    plt.rcParams["text.usetex"] = True
    plt.rcParams["font.family"] = "sans-serif"
    plt.rcParams["font.size"] = 20.0

    # Parameters figure
    fig, ax = plt.subplots()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$F_h$")
    ax.set_ylabel(r"$\mathcal{I}_E$")

    ax.tick_params(axis="x")
    ax.tick_params(axis="y")

    # Figure legend
    fig_legend = plt.figure(figsize=(16, 2))

    # Define colors and markers
    colors = ["green", "red", "blue", "orange"]
    markers = ["s", "^", "o", "x"]
    if len(directories) == 1:
        colors = ["blue"]
        markers = ["o"]

    # Loop over all directories and simulations
    for idir, directory in enumerate(directories):
        path_simulations_before_sort = sorted(
            (root_path / directory).glob("NS2D.*")
        )
        path_simulations = sort_paths_from_gamma(path_simulations_before_sort)
        # Compute isotropy and forcing horizontal Froude.
        isotropies = []
        errors = []
        Fhf = []
        Fhs = []
        labels = []
        for path in path_simulations:
            sim = load_sim_for_plot(path)
            from util import compute_horizontal_froude

            tmin = get_tmin_from_path(path)
            Fh, Fh_err = compute_horizontal_froude(sim, tmin=tmin)
            Fhs.append(Fh)
            # Fhf.append(sim.output.froude_number / sim.output.ratio_omegas)
            tmin = get_tmin_from_path(path)
            isotropy, isotropy_err = compute_isotropy(path, tmin=tmin)
            isotropies.append(isotropy)
            errors.append(isotropy_err)
            label_gamma = round(sim.output.ratio_omegas, 1)
            labels.append(rf"$\gamma = {label_gamma};$")

        # Only when varying stratification
        if len(directories) > 1:
            ax.plot(
                Fhs,
                isotropies,
                marker=markers[idir],
                color=colors[idir],
                markersize=10,
                linestyle="",
                label=fr"$n_x = {sim.params.oper.nx}$",
            )

        # Only when varying reynolds
        if len(directories) == 1:
            errors = np.asarray(errors)
            lines = []
            for index in range(len(Fhs)):
                line = ax.errorbar(
                    Fhs[index],
                    isotropies[index],
                    yerr=errors[index] * 0.1,
                    fmt="o",
                    capsize=3,
                    markersize=8,
                )

                lines.append(line[0])

            legend = fig_legend.legend(
                lines,
                labels,
                loc="center",
                frameon=True,
                ncol=len(lines) // 2,
                fontsize=18,
                handlelength=0.8,
                columnspacing=0.5,
                handletextpad=0.5,
            )

    if len(directories) > 1:
        ax.legend(fontsize=16, loc=4)
        # ax.set_xlim(left=0.008, right=0.85)
        # ax.set_ylim(bottom=8e-4, top=1.3)

    fig.tight_layout()

    # Saving ...
    root_save = Path("/home/users/calpelin7m/Repos/phd_miguelcalpelinares")

    if len(directories) == 1:
        save(fig, "figures/isotropy.pdf")
        save(fig_legend, "figures/legend_isotropy.pdf")
    elif len(directories) > 1:
        save(fig, "figures/isotropy_reynolds.pdf")
    
    show()
