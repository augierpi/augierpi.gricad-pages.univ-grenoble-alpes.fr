"""
plot_ratio_dissipation.py
============================
Date : 26 / 04 / 2019

Computes the raio dissipation.
"""

import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

from pathlib import Path
from fluidsim import load_params_simul, load_sim_for_plot
from util import (
    compute_horizontal_froude,
    compute_buoyancy_reynolds,
    sort_paths_from_gamma,
    get_tmin_from_path,
    save,
    show,
)
from plot_anisotropy import compute_isotropy

import matplotlib.lines as mlines

from mpl_toolkits.axes_grid1 import make_axes_locatable


def compute_ratio_dissipation(path_simulation, tmin=None):
    """
    Compute ratio dissipation from path simulation.
    """
    # Print out
    path_simulation = path_simulation.as_posix()

    res_out = float(path_simulation.split("NS2D.strat_")[1].split("x")[0])
    gamma_str = path_simulation.split("_gamma")[1].split("_")[0]
    # if gamma_str.startswith("0"):
    #     gamma_out = float(gamma_str[0] + "." + gamma_str[1])
    # else:
    gamma_out = float(gamma_str)

    print(f"Compute dissipation nx = {res_out} and gamma {gamma_out}..")

    # Load object parameters
    params = load_params_simul(path_simulation)

    with h5py.File(path_simulation + "/spect_energy_budg.h5", "r") as f:
        times = f["times"].value
        kx = f["kxE"].value
        kz = f["kyE"].value
        dset_dissEKu_kx = f["dissEKu_kx"]
        dset_dissEKv_kx = f["dissEKv_kx"]
        dset_dissEA_kx = f["dissEA_kx"]
        dset_dissEKu_kz = f["dissEKu_ky"]
        dset_dissEKv_kz = f["dissEKv_ky"]
        dset_dissEA_kz = f["dissEA_ky"]

        # Compute itmin time average
        if not tmin:
            nb_files = 10
            dt = np.median(np.diff(times))
            tmin = np.max(times) - (nb_files * dt)
        itmin = np.argmin(abs(times - tmin))

        # Compute dissipation curve
        delta_kx = np.median(np.diff(kx))
        delta_kz = np.median(np.diff(abs(kz)))

        dissEK_kx = dset_dissEKu_kx[-itmin:] + dset_dissEKv_kx[-itmin:]
        dissEA_kx = dset_dissEA_kx[-itmin:]
        dissE_kx = (dissEK_kx + dissEA_kx).mean(0)

        dissEK_kz = dset_dissEKu_kz[-itmin:] + dset_dissEKv_kz[-itmin:]
        dissEA_kz = dset_dissEA_kz[-itmin:]
        dissE_kz = (dissEK_kz + dissEA_kz).mean(0)

        D_kx = dissE_kx.cumsum() * delta_kx
        D_kz = dissE_kz.cumsum() * delta_kz

    # Compute k_fx
    k_fx = (
        np.sin(params.forcing.tcrandom_anisotropic.angle)
        * params.forcing.nkmax_forcing
        * max(delta_kx, delta_kz)
    )
    ik_fx = np.argmin(abs(kx - k_fx))

    ratio_dissipation_old = D_kx[ik_fx] / D_kx[-1]

    # Compute wave-number for 1/2 dissipation
    ikx_half = np.argmin(abs(D_kx - (np.max(D_kx) / 2)))
    ikz_half = np.argmin(abs(D_kz - (np.max(D_kz) / 2)))

    # return D_kx[ik_half] / D_kx[ik_fx]
    return kx[ikx_half] / k_fx, kz[ikz_half] / k_fx


if __name__ == "__main__":

    # ratio = compute_ratio_dissipation(path_simulation, tmin=None)
    # print(f"Ratio between ik_half / ik_fx = {ratio}")

    root_path = Path("/fsnet/project/meige/2015/15DELDUCA/DataSim/")
    # directories = ["march19_sim3840"]
    directories = [
        "may19_sim960",
        "may19_sim1920",
        "march19_sim3840",
        "sim7680x480",
        "sim15360x960",
    ]

    plt.rcParams["text.usetex"] = True
    plt.rcParams["font.family"] = "sans-serif"
    plt.rcParams["font.size"] = 20.0

    fig, ax = plt.subplots()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$F_h$")
    ax.set_ylabel(r"$\mathcal{I}_d$")
    ax.tick_params(axis="x")
    ax.tick_params(axis="y")
    if len(directories) > 1:
        ax.set_xlim(left=0.04, right=4.3)
        ax.set_ylim(bottom=0.02, top=1.5)
        ax.set_xlabel(r"$F_{h,f}$")

    # else:
    # ax.set_xlim(left=0.04, right=2.1)
    # ax.set_ylim(bottom=5e-2, top=1.4)

    fig1, ax1 = plt.subplots()
    ax1.set_xscale("log")
    ax1.set_yscale("log")
    ax1.set_xlabel(r"$F_{h}$")
    ax1.set_ylabel(r"$\mathcal{D}(k_z) / \mathcal{D}(k_x)$")
    ax1.tick_params(axis="x")
    ax1.tick_params(axis="y")
    # ax1.set_xlim(left=3e-2, right=4e0)
    # ax1.set_ylim(bottom=5e-2, top=1.4)

    fig2, ax2 = plt.subplots()
    ax2.set_xscale("log")
    ax2.set_yscale("log")
    ax2.set_xlabel(r"$\mathcal{R}_8$")
    ax2.set_ylabel(r"$\mathcal{I}_d$")
    ax2.tick_params(axis="x")
    ax2.tick_params(axis="y")
    ax2.set_xlim(left=3e-13, right=6e8)
    ax2.set_ylim(bottom=4e-2, top=1.4)
    # ax.set_ylim(bottom=0, top=1)

    fig3, ax3 = plt.subplots()
    ax3.set_xscale("log")
    ax3.set_yscale("log")
    ax3.set_ylabel(r"$\mathcal{R}_8$")
    ax3.set_xlabel(r"$F_h$")
    ax3.tick_params(axis="x")
    ax3.tick_params(axis="y")
    ax3.set_xlim(left=2e-4, right=3e-1)
    ax3.set_ylim(bottom=4e-12, top=4e9)

    colors = ["green", "red", "blue", "orange", "black"]
    markers = ["s", "^", "o", "x", "d"]
    markers2 = ["s", "p", "o", "x", "d"]
    if len(directories) == 1:
        colors = ["blue"]
        markers = ["o"]

        # Figure legend (only when variation stratification)
        fig_legend = plt.figure(figsize=(16, 2))

    root_occigen = Path("/fsnet/project/meige/2015/15DELDUCA/")
    directory_occigen = "Miguel_OccigenData/Steady"
    path_simulations_occigen = (root_occigen / directory_occigen).glob("NS2D.*")
    nb_sim_occigen = 3

    for idir, directory in enumerate(directories):
        if "sim15360" in directory:
            path_simulations = path_simulations_occigen
        else:
            path_simulations_before_sort = sorted(
                (root_path / directory).glob("NS2D.*")
            )
            path_simulations = sort_paths_from_gamma(path_simulations_before_sort)

        # Compute isotropy and forcing horizontal Froude.
        ratios_kx = []
        ratios_kz = []
        Fhf = []
        Fh = []
        Rb = []
        isotropies = []
        labels = []

        for path in path_simulations:
            sim = load_sim_for_plot(path)
            Fhf.append(sim.output.froude_number / sim.output.ratio_omegas)
            tmin = get_tmin_from_path(path)
            ratio_kx, ratio_kz = compute_ratio_dissipation(path, tmin=tmin)
            ratios_kx.append(1.0 / ratio_kx)
            ratios_kz.append(1.0 / ratio_kz)
            if len(directories) > 1:
                froude, Fh_err = compute_horizontal_froude(
                    sim, tmin=tmin, for_final_plot=True
                )
            else:
                froude, Fh_err = compute_horizontal_froude(sim, tmin=tmin)

            Fh.append(froude)

            label_gamma = round(sim.output.ratio_omegas, 1)
            labels.append(rf"$\gamma = {label_gamma};$")

            F_h, Re_8, R_b = compute_buoyancy_reynolds(sim, tmin=tmin)
            Rb.append(Re_8 * froude ** 8)
            isotropy, isotropy_err = compute_isotropy(path)
            isotropies.append(isotropy)

        scatter = None
        if len(directories) == 1:
            # ax.set_xlim(left=0.035, right=4)
            # ax.set_ylim(top=1.2, bottom=0.18)

            ratios_kx = np.asarray(ratios_kx)
            ratios_kz = np.asarray(ratios_kz)
            lines = []
            for index in range(len(Fhf)):
                # line = ax.scatter(
                #     Fh[index],
                #     ratios_kz[index] / ratios_kx[index],
                #     marker=markers[idir],
                #     s=1e2,
                # )
                line, = ax.plot(
                    Fh[index],
                    ratios_kz[index] / ratios_kx[index],
                    "o",
                    markersize=10,
                )

                lines.append(line)
            import ipdb

            # ipdb.set_trace()
            legend = fig_legend.legend(
                lines,
                labels,
                loc="center",
                frameon=True,
                ncol=len(lines) // 2,
                fontsize=18,
                handlelength=0.8,
                columnspacing=0.5,
                handletextpad=0.5,
            )

        elif len(directories) > 1:

            ratios_kx = np.asarray(ratios_kx)
            ratios_kz = np.asarray(ratios_kz)
            ax.plot(
                Fhf,
                ratios_kz / ratios_kx,
                color=colors[idir],
                marker=markers[idir],
                markersize=10,
                linestyle="",
                label=rf"$n_x={sim.params.oper.nx}$",
            )
            ax1.plot(
                Fh,
                ratios_kz / ratios_kx,
                color=colors[idir],
                marker=markers[idir],
                markersize=10,
                linestyle="",
                label=rf"$n_x={sim.params.oper.nx}$",
            )

            ax2.plot(
                Rb,
                ratios_kz / ratios_kx,
                color=colors[idir],
                marker=markers[idir],
                markersize=10,
                linestyle="",
                label=rf"$n_x={sim.params.oper.nx}$",
            )
            # Vertical lines delimit three regimes
            ax2.axvline(x=1e0, linestyle=":", color="black")

            # ax3.scatter(
            # Fh,
            # Rb,
            # color=colors[idir],
            # marker=markers[idir],
            # s=1e2,
            # label=rf"$n_x={sim.params.oper.nx}$",
            # )

            # Plot...
            diss = []
            for ii, ratio in enumerate(ratios_kx):
                diss.append(ratios_kz[ii] / ratios_kx[ii])

            import matplotlib.cm as cm

            diss = np.asarray(diss)
            isotropies = np.asarray(isotropies)
            print(isotropies)

            # # Add simulations occigen 15360..
            # root_path = Path("/fsnet/project/meige/2015/15DELDUCA")
            # directory = "Miguel_OccigenData/Steady"
            # path_simulations = (root_path / directory).glob("NS2D.*")

            # for path in path_simulations:
            # sim = load_sim_for_plot(path)
            # froude, Fh_err = compute_horizontal_froude(sim, tmin=tmin)
            # Fh.append(froude)

            # Fhf.append(sim.output.froude_number / sim.output.ratio_omegas)
            # tmin = get_tmin_from_path(path)
            # ratio_kx, ratio_kz = compute_ratio_dissipation(path, tmin=tmin)
            # ratios_kx.append(1.0 / ratio_kx)
            # ratios_kz.append(1.0 / ratio_kz)

            # label_gamma = round(sim.output.ratio_omegas, 1)
            # labels.append(rf"$\gamma = {label_gamma};$")

            # F_h, Re_8, R_b = compute_buoyancy_reynolds(sim, tmin=tmin)
            # Rb.append(Re_8 * froude ** 8)

            # isotropy, isotropy_err = compute_isotropy(path)
            # isotropies.append(isotropy)

            scatter = ax3.scatter(
                Fh,
                Rb,
                s=500 * diss,
                c=isotropies,
                marker=markers2[idir],
                cmap="viridis",
                vmin=0,
                vmax=1,
            )
            # for _f, _r, _d, _i in zip(Fh, Rb, diss, isotropies):
            # # scatter = ax3.scatter(
            # # _f,
            # # _r,
            # # s=500 * _d,
            # # c=_i,
            # # vmin=0,
            # # vmax=1,
            # # marker=markedir])
            # print(isotropies)
            # ax3.scatter(
            # _f,
            # _r,
            # s=500 * _d,
            # color=_i,
            # marker=markedir],
            # vmin=0,
            # vmax=1
            # )
            # s=500 * _d,
            # c=_i,
            # marker=markersr])

    #
    # Plot regimes
    ax3.plot(
        [2e-4, 2e-2], [1e0, 1e0], color="black", linestyle=":", linewidth=2
    )
    ax3.plot(
        [2e-2, 2e-2], [1e-13, 1e9], color="black", linestyle=":", linewidth=2
    )

    # # Text regimes
    # ax3.text(5e-2, 1e-5, fr"Regime I", color="black", fontsize=14)
    # ax3.text(3e-3, 1e-10, fr"Regime III", color="black", fontsize=14)
    # ax3.text(4e-4, 1, fr"Regime II", color="black", fontsize=14)

    ax3.scatter(3e-4, 8e8, marker="o", s=500 * 0.1, color="red")
    ax3.scatter(3e-4, 8e6, marker="o", s=500 * 1.0, color="red")
    ax3.text(
        7/5 * 3e-4,
        2e8,
        fr"$\mathcal{{I}}_d = 0.1$ (anisotropic dissipation)",
        color="red",
        fontsize=14,
    )
    ax3.text(
        7/5 * 3e-4,
        2e6,
        fr"$\mathcal{{I}}_d = 1.0$ (isotropic dissipation)",
        color="red",
        fontsize=14,
    )

    ax3.text(3.8e-1, 1e-14, r"$\mathcal{I}_E$")

    # Legend
    square960 = mlines.Line2D(
        [],
        [],
        color="black",
        marker="s",
        linestyle="None",
        markersize=8,
        label=r"$n_x = 960$",
    )
    pentagon1920 = mlines.Line2D(
        [],
        [],
        color="black",
        marker="p",
        linestyle="None",
        markersize=8,
        label=r"$n_x = 1920$",
    )
    dot3840 = mlines.Line2D(
        [],
        [],
        color="black",
        marker="o",
        linestyle="None",
        markersize=8,
        label=r"$n_x = 3840$",
    )
    cross7680 = mlines.Line2D(
        [],
        [],
        color="black",
        marker="x",
        linestyle="None",
        markersize=8,
        label=r"$n_x = 7680$",
    )
    diamond15360 = mlines.Line2D(
        [],
        [],
        color="black",
        marker="d",
        linestyle="None",
        markersize=8,
        label=r"$n_x = 15360$",
    )

    ax3.legend(
        handles=[square960, pentagon1920, dot3840, cross7680, diamond15360],
        loc="upper center",
        bbox_to_anchor=(0.5, 1.12),
        borderaxespad=0.01,
        ncol=len(markers2),
        columnspacing=0.5,
        handletextpad=0.001,
        fontsize=14,
    )
    #
    divider = make_axes_locatable(ax3)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cax.tick_params(labelsize=14)
    if scatter:
        fig3.colorbar(scatter, cax=cax)

    if len(directories) > 1:
        ax.legend()
        ax1.legend()

        # Text three regimes
        # ax2.text(4e4, 0.15, "Regime I")
        # ax2.text(1e-2, 0.15, "Regime II")
        # ax2.text(1e-11, 0.6, "Regime III")

    fig.tight_layout()

    if len(directories) == 1:
        path_save = "dissipation_froude.png"
        save(fig, path_save)
    elif len(directories) > 1:
        path_save_figure1 = "dissipation_froude_reynolds.png"
        path_save_figure2 = "dissipation_buoyancy_reynolds.png"
        path_save_figure4 = "buoyancy_reynolds_froude.png"

        save(fig, path_save_figure1)
        save(fig2, path_save_figure2)
        save(fig3, path_save_figure4)

    show()
