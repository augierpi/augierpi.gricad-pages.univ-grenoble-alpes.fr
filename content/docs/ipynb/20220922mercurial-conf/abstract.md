### Title

Using Mercurial, hg-evolve and hg-git in an academic context

### Summary

I will present how we use Mercurial, topics, evolve and hg-git in an
academic context.

I am a CNRS researcher working on instabilities and turbulence in fluids. In
our group, we base a lot of our work on few Python packages. The development of
these packages involve people with very different experience in coding and in
versioning.

Depending on the software projects, the development is hosted on different
platforms: foss.heptapod.org, a university Gitlab instance and Github. For the
projects hosted on Heptapod, we use a workflow based on Merge Requests (MR),
topics and the evolve extension. This workflow presents great advantages for
us: (i) the code review associated with MR is very efficient to improve skills
and productivity of the students. (ii) Topics and evolve allow the experienced
developers to directly interact on the MR and on their history (in particular
with `rebase`, `amend`, `fold`, `absorb`, ...). The readability of the history
of the repositories is largely improved. (iii) Fairly quickly, students master
at least `amend`, loose the apprehension of making bad commits and tend to
commit small changesets and to push more often. With hg-git, Mercurial can also
be used for projects hosted on Gitlab and Github so students do not need to
directly interact with Git.

### Speaker name, company, contact

Pierre AUGIER

CNRS, UGA, LEGI (Grenoble, France)

pierre.augier@uga-grenoble-alpes.fr

### Duration (10/20 min)

Doesn't matter. I guess 15 min should be fine.
