
import os

from wordcloud import WordCloud
import matplotlib.pyplot as plt

words = {
    'Coriolis platform': 4,
    'open-source': 4,
    'open-science': 4,
    'Experiments': 4,
    'Simulations': 4,
    'FluidDyn project': 6,
    'Fluid dynamics': 5,
    'Python': 6,
    'C++': 4,
    'fluidlab': 4,
    'fluidimage': 4,
    'fluidfft': 4,
    'fluidsim': 4,
    '8 March': 4,
    "Women's rights": 4,
}

w = 800
h = 400
size = 4

wordcloud = WordCloud(width=w, height=h, margin=3, background_color='white')
wordcloud.generate_from_frequencies(words)

fig = plt.figure(figsize=(size*w/h, size*h/h))
ax = fig.add_axes([0, 0, 1, 1])
ax.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")

here = os.path.dirname(__file__)

tmp = os.path.join(here, '../tmp')

if not os.path.exists(tmp):
    os.mkdir(tmp)

fig.savefig(os.path.join(tmp, 'tag_clouds.png'))
