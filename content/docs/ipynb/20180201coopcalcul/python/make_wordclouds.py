
import os

from wordcloud import WordCloud
import matplotlib.pyplot as plt

words = {
    'open-source': 6,
    'open-science': 4,
    'Experiments': 3,
    'Simulations': 3,
    'Python': 5,
    'Matlab': 3,
    'FluidDyn project': 6,
    'fluidlab': 4,
    'fluidimage': 5,
    'fluidfft': 4,
    'fluidsim': 4,
    'fluidfoam': 3}

w = 800
h = 250
size = 4

wordcloud = WordCloud(width=w, height=h, margin=3, background_color='white')
wordcloud.generate_from_frequencies(words)

fig = plt.figure(figsize=(size*w/h, size*h/h))
ax = fig.add_axes([0, 0, 1, 1])
ax.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")

here = os.path.dirname(__file__)

tmp = os.path.join(here, '../tmp')

if not os.path.exists(tmp):
    os.mkdir(tmp)

fig.savefig(os.path.join(tmp, 'tag_clouds.png'))
