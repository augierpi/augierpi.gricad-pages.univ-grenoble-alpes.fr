
import os

from wordcloud import WordCloud
import matplotlib.pyplot as plt

words = {
    'Coriolis platform': 5,
    'Mixing efficiency': 5,
    'open-source': 3,
    'open-science': 3,
    'Stratified turbulence': 6,
    'Rotation': 3,
    'Experiments': 4,
    'Simulations': 3,
    'fluiddyn project': 6,
    'MILESTONE project': 5,
    'fluidlab': 3,
    'fluidimage': 4,
    'fluidfft': 4,
    'fluidsim': 3}
w = 800
h = 250
size = 4

wordcloud = WordCloud(width=w, height=h, margin=3, background_color='white')
wordcloud.generate_from_frequencies(words)

fig = plt.figure(figsize=(size*w/h, size*h/h))
ax = fig.add_axes([0, 0, 1, 1])
ax.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")

here = os.path.dirname(__file__)

tmp = os.path.join(here, '../tmp')

if not os.path.exists(tmp):
    os.mkdir(tmp)

fig.savefig(os.path.join(tmp, 'tag_clouds.png'))
