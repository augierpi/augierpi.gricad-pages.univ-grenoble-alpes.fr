from pathlib import Path
from shutil import rmtree
from subprocess import getoutput, call
from io import StringIO

import pandas as pd
import matplotlib.pyplot as plt

here = Path(__file__).absolute().parent
data_dir = here / "data"
data_dir.mkdir(exist_ok=1)

addresses = [
    "https://github.com/JuliaLang/julia",
    "https://github.com/python/cpython",
    "https://github.com/numpy/numpy",
    "https://github.com/scipy/scipy",
    "https://github.com/pandas-dev/pandas",
    "https://github.com/scikit-learn/scikit-learn",
    "https://github.com/scikit-image/scikit-image",
]

addresses = {add.rsplit("/", 1)[1]: add for add in addresses}

addresses["pypy"] = ""

path_repos = {"pypy": Path.home() / "Dev/pypy"}

def make_file_data_package(package_name):

    path_file = data_dir / (package_name + ".txt")
    if not path_file.exists():
        address = addresses[package_name]

        if package_name in path_repos:
            repo_dir = path_repos[package_name]
            print(repo_dir)
        else:
            repo_dir = data_dir / "tmp_repo"
            retcode = call(f"git clone --depth 1 {address} {repo_dir}".split(" "))
        output = getoutput(f"cloc {repo_dir}")

        if package_name not in path_repos:
            rmtree(repo_dir)
        output = output.split("github.com/AlDanial/cloc")[1].split("\n")[2:]
        output = "\n".join(output)
        with open(path_file, "w") as file:
            file.write(output)
    else:
        with open(path_file) as file:
            output = file.read()

    return pd.read_csv(
        StringIO(output),
        header=0,
        sep="[ ]{2,}",
        comment="-",
        engine="python",
        index_col=0,
    )


languages = ["Python", "C", "C++", "Fortran 77", "Cython", "C/C++ Header", "Julia"]
colors = [f"C{ind}" for ind in range(len(languages))]
languages_legend = set()

fig, ax = plt.subplots()
width = 0.35

for iname, name in enumerate(addresses.keys()):
    df = make_file_data_package(name)
    df = df["code"]
    # total = df["SUM:"]
    last = 0.
    for ilang, language in enumerate(languages):
        if language in df.index:

            if language not in languages_legend:
                label = language
                languages_legend.add(label)
            else:
                label = None

            ax.bar(
                iname, df[language], bottom=last, color=colors[ilang], label=label
            )
            last += df[language]

ax.set_xticklabels(addresses.keys())
ax.set_xticks(range(len(addresses)))
plt.legend()
plt.show()
