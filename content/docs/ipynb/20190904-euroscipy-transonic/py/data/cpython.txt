Language                             files          blank        comment           code
---------------------------------------------------------------------------------------
Python                                1847         117503         142318         539581
C                                      318          51778          56455         308257
C/C++ Header                           380          15350          10038         155814
Bourne Shell                            14           2867           2485          18014
m4                                       4            545            148           5522
C++                                      5            725            256           3228
YAML                                    35            387             74           2006
HTML                                    10            108             11           1869
WiX source                              51            161             41           1714
DOS Batch                               32            349             96           1672
Assembly                                 6            229            384           1365
Windows Module Definition                8             23             14           1364
MSBuild script                          27             40              3            645
Objective C                              7             98             61            635
Lisp                                     1            109             81            502
Pascal                                   3            113            261            349
PowerShell                               6             84            171            345
XML                                     58             72              7            338
Windows Resource File                    7             40             47            295
make                                     3             53             38            222
WiX string localization                 11             28              0            188
JavaScript                               2             18             14            176
INI                                      1             42             27            102
D                                        5              8              1             74
JSON                                     6              3              0             65
Markdown                                 3             19              0             41
IDL                                      2              1              0             35
C Shell                                  1              9              7             21
XSLT                                     1              0              0              5
DTD                                      1              4              0              2
CSS                                      1              2              4              0
Visual Basic                             1              0              1              0
---------------------------------------------------------------------------------------
SUM:                                  2857         190768         213043        1044446
---------------------------------------------------------------------------------------