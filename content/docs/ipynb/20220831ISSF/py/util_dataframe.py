from util import couples320, get_path_finer_resol, get_customized_dataframe

paths = []
for N, Rb in sorted(couples320):
    if N == 2.9:
        continue
    path = get_path_finer_resol(N, Rb)
    if path is not None:
        paths.append(path)

print(f"Using {len(paths)} simulations")

df = get_customized_dataframe(paths)

df["k_max*lambda"] = df["k_max"] * df["lambda"]
