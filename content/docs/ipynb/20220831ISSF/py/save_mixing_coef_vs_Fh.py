import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, params_simuls_regimes, add_letters

from util_dataframe import df

ax = plot(
    df,
    "Fh",
    "Gamma",
    c=np.log10(df["R2"]),
    logy=True,
    vmin=0.3,
    vmax=2,
    s=35,
)

df_tmp = df[["N", "Rb", "Fh", "Gamma"]]
letters = {v: k for k, v in params_simuls_regimes.items()}

for index, (N, Rb, Fh, Gamma) in df_tmp.iterrows():
    if (N, Rb) not in letters:
        continue
    letter = letters[(N, Rb)]
    coef_x = 0.85
    coef_y = 0.7
    if letter == "O":
        coef_x = 0.7
        coef_y = 1.04
    elif letter == "W":
        coef_x = 1.08
        coef_y = 0.95
    elif letter == "L":
        coef_y = 1.2
    elif letter == "P":
        coef_x = 0.95
        coef_y = 1.13

    ax.text(coef_x * Fh, coef_y * Gamma, letter, color="r")

ax.set_xlabel("$F_h$")
ax.set_ylabel(r"$\Gamma=\epsilon_A / \epsilon_K$")

xs = np.linspace(1.5e-1, 5e-1, 4)
ax.plot(xs, 5e-2 * xs**-1)
ax.text(0.16, 0.12, "${F_h}^{-1}$")

xs = np.linspace(8e-1, 4, 4)
ax.plot(xs, 5e-2 * xs**-2)
ax.text(1.2, 0.05, "${F_h}^{-2}$")

fig = ax.figure
add_letters(fig, "a")
fig.tight_layout()
fig.text(0.83, 0.07, r"$\log_{10}(\mathcal{R})$", fontsize=12)

save_fig(fig, "fig_mixing_coef_vs_Fh.png")

if __name__ == "__main__":
    plt.show()
