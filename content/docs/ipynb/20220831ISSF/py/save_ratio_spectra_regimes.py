import sys

import numpy as np
import matplotlib.pyplot as plt

from util_simuls_regimes import get_sim

from util import save_fig, customize, add_letters

print(sys.argv)
letter = sys.argv[-1]

if letter not in "VLOWP":
    letter = "L"

sim = get_sim(letter)

p_oper = sim.params.oper
kmax = p_oper.coef_dealiasing * p_oper.nx * np.pi / p_oper.Lx

t_start, t_last = sim.output.print_stdout.get_times_start_last()
tmin = max(10, min(t_start + 2, t_last - 1))
mean_values = sim.output.get_mean_values(tmin=tmin, customize=customize)

data = sim.output.spectra.load1d_mean(tmin=tmin)
kx = data["kx"]
EA = data["spectra_A_kx"][kx < kmax]
EKhr = data["spectra_Khr_kx"][kx < kmax]
EKhd = data["spectra_Khd_kx"][kx < kmax]
EKz = data["spectra_vz_kx"][kx < kmax]
kx = kx[kx < kmax]

EKp = EKhd + EKz
EK = EKhd + EKhr + EKz

ratio_A = EA / EK
ratio_Kz = 3 * EKz / EK
ratio_polo_over_toro = EKp / EKhr

fig, ax = plt.subplots()

ax.plot(
    kx,
    ratio_A / mean_values["Gamma"],
    label=(
        r"$(E_A \varepsilon_K)/(E_K \varepsilon_A)$, with "
        rf"$\varepsilon_A / \varepsilon_K = {mean_values['Gamma']:.3f}$"
    ),
)
ax.plot(kx, ratio_Kz, label="$3 E_{Kz} / E_K$")
ax.plot(kx, ratio_polo_over_toro, label="polo / toro")

ax.set_xscale("log")
ax.set_yscale("log")

ax.set_xlabel("$k_x$")
ax.set_ylabel("ratio of horizontal spectra")
ax.set_ylim([0.1, 2])
ax.axhline(1, linestyle=":", color="grey")

ax.legend()

# if letter == "W":
#     k = np.array([7, 50])
#     ax.plot(k, 0.03 * k ** (-1.35 + 5 / 3), "k")

# ax.set_ylim(bottom=1e-3)
# ax.set_title("")

add_letters(fig, "b")
fig.tight_layout()
save_fig(fig, f"fig_ratio_spectra_regime_{letter}.png")

if __name__ == "__main__":
    plt.show()
