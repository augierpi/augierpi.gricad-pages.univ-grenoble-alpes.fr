
import matplotlib.pyplot as plt

from util import save_fig
from util_1strongly_strat_sim import sim, tmin

sim.output.spatiotemporal_spectra.plot_temporal_spectra(
    tmin=tmin, coef_compensate=5 / 3, plot_resonant_modes=False
)

fig = plt.gcf()
ax = fig.axes[0]
ax.set_title("")

ax.set_ylabel(r"$E(\omega) \omega^{5/3}$")

fig.tight_layout()

save_fig(fig, "fig_1strongly_strat_spectra_omega.png")

if __name__ == "__main__":
    plt.show()
