from pathlib import Path

# from functools import partial

import matplotlib.pyplot as plt

from fluidsim import load

from util import path_base

path_dir = (
    Path(path_base)
    / "simul_folders"
    / "ns3d.strat_toro_Fh1.250e-02_Rb10_1344x1344x84_V3x3x0.188_N80_2022-04-26_20-26-52"
)

sim = load(path_dir, hide_stdout=1)

tmin = 41

# sim.output.spectra.plot1d(
#     tmin=tmin,
#     coef_compensate=5 / 3,
#     coef_plot_k3=100,
#     coef_plot_k2=2,
#     plot_forcing_region=True,
# )


# plot_temporal_spectra = partial(
#     sim.output.spatiotemporal_spectra.plot_temporal_spectra, tmin=tmin
# )


def plot_kzkhomega(
    key_field, equation, xmax=50, ymax=None, vmin=-9, vmax=-4.5, **kwargs
):
    sim.output.spatiotemporal_spectra.plot_kzkhomega(
        key_field,
        tmin=tmin,
        equation=equation,
        xmax=xmax,
        ymax=ymax,
        vmin=vmin,
        vmax=vmax,
        **kwargs,
    )

    fig = plt.gcf()
    ax = fig.axes[0]

    if equation.startswith("omega"):
        equation = f"$\{equation.replace('*', '')}$"

    ax.set_title(f"{key_field}, {equation}")
    fig.tight_layout()


if __name__ == "__main__":
    # plot_temporal_spectra(coef_compensate=5 / 3, plot_resonant_modes=False)

    plot_kzkhomega("Khr", equation="omega=0.1*N", vmax=-4.5)
    plot_kzkhomega("Kp", equation="omega=0.1*N", vmax=-4.5)

    plot_kzkhomega("Khr", equation="omega=0.6*N", vmax=-6.6)
    plot_kzkhomega("Kp", equation="omega=0.6*N", vmax=-6.6)


    # plot_kzkhomega("Khr", equation="ikz=0", xmax=50, ymax=2)
    # plot_kzkhomega("Khr", equation="ikz=1", xmax=50, ymax=2)
    # plot_kzkhomega("Kp", equation="ikz=1", xmax=50, ymax=2)
    # plot_kzkhomega("Khr", equation="ikz=2", xmax=50, ymax=2)
    # plot_kzkhomega("Kp", equation="ikz=2", xmax=50, ymax=2)

    plt.show()
