import numpy as np
import matplotlib.pyplot as plt

from util import save_fig
from util_1strongly_strat_sim import sim, tmin

sim.output.spectra.plot1d(
    tmin=tmin,
    coef_compensate=5 / 3,
    plot_forcing_region=False,
)

fig = plt.gcf()
ax = fig.axes[0]
ax.set_title("")

k = np.array([8, 30])
ax.plot(k, 1.2 * k**(-2 + 5/3), "k")
ax.text(15, 0.6, "$k^{-2}$")

k = np.array([150, 500])
ax.plot(k, 4e3 * k**(-3 + 5/3), "k")
ax.text(270, 3, "$k^{-3}$")

ax.set_ylim(bottom=1e-2, top=7)

fig.tight_layout()

save_fig(fig, "fig_1strongly_strat_spectra_k.png")

if __name__ == "__main__":
    plt.show()
