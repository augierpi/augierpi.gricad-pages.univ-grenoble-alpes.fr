import sys
from runpy import run_path

import matplotlib.pyplot as plt

from util import has_to_be_made, has_to_save, here, paths_simuls_regimes


def make_fig(name, source=None):
    if source is None:
        source = f"save_{name}.py"
    if has_to_be_made(f"fig_{name}", source):
        # interesting: transonic/inspect bug without str(...)!
        # https://foss.heptapod.net/fluiddyn/transonic/-/merge_requests/104
        run_path(str(here / source))


def make_table(name, source=None):
    if source is None:
        source = f"save_table_{name}.py"
    if has_to_be_made(f"table_{name}.tex", source):
        run_path(str(here / source))


# make_table("methods_1couple")
# make_table("better_simuls")
# make_table("simuls_regimes")

# make_fig("E_vs_time_N40_Ri20")
# make_fig("means_vs_kmaxeta_N40_Ri20")
# make_fig("spectra_1couple")

# make_fig("kmaxeta_vs_FhR")
# make_fig("epsK2overepsK_vs_FhR")

make_fig("isotropy_coef_vs_FhR")
make_fig("isotropy_diss_vs_R")
make_fig("isotropy_diss_vs_Fh")
make_fig("isotropy_velo_vs_R")
make_fig("isotropy_velo_vs_Fh")

make_fig("mixing_coef_vs_FhR")
make_fig("mixing_coef_vs_Fh")

# make_fig("ratio_EA_EK_vs_FhR")
make_fig("ratio_EA_EK_vs_Fh")

# make_fig("ratio_velo_vs_Fh")
# make_fig("ratio_length_scales_vs_Fh")

make_fig("1strongly_strat_spectra_k")
make_fig("1strongly_strat_spectra_omega")
make_fig("1strongly_strat_st_spectra_Khr_omega0.1N", "save_1strongly_strat_st_spectra.py")

sys.argv.append("")

for letter in "LW":
    sys.argv[-1] = letter
    make_fig(f"spectra_regime_{letter}", "save_spectra_regimes.py")
    # make_fig(f"seb_regime_{letter}", "save_seb_regimes.py")
    make_fig(f"ratio_spectra_regime_{letter}", "save_ratio_spectra_regimes.py")

del sys.argv[-1]

# make_fig("spectra_1strat")
# make_fig("spectra_1R")

# make_fig("old_simuls_vs_FhR")

if not has_to_save:
    plt.show()
