import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, add_letters

from util_dataframe import df

ax = plot(
    df, "R2", "I_velocity", c=np.log10(df["Fh"]), vmin=-2, vmax=-1, logy=True
)

ax.set_xlabel(r"$\mathcal{R}$")
ax.set_ylabel("$I_{velo}$")

fig = ax.figure

fig.text(0.84, 0.07, r"$\log_{10}(F_h)$", fontsize=12)

add_letters(fig, "b")
fig.tight_layout()
save_fig(fig, "fig_isotropy_velo_vs_R.png")

if __name__ == "__main__":
    plt.show()
