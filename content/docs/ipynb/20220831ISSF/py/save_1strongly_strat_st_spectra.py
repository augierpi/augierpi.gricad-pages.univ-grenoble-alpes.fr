import matplotlib.pyplot as plt

from util import save_fig
from util_1strongly_strat_sim import plot_kzkhomega


def do(key, equation, vmax):
    plot_kzkhomega(key, equation=equation, vmax=vmax)
    equation = equation.replace("=", "").replace("*", "")
    save_fig(plt.gcf(), f"fig_1strongly_strat_st_spectra_{key}_{equation}.png")


print("hello")


do("Khr", equation="omega=0.1*N", vmax=-4.5)
do("Kp", equation="omega=0.1*N", vmax=-4.5)
do("Khr", equation="omega=0.6*N", vmax=-6.6)
do("Kp", equation="omega=0.6*N", vmax=-6.6)

if __name__ == "__main__":
    plt.show()
