import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, params_simuls_regimes, add_letters

from util_dataframe import df

tmp = df.copy()
tmp["E_A/E_K"] = tmp.EA / (tmp.EKh)
ax = plot(
    tmp,
    "Fh",
    "E_A/E_K",
    c=np.log10(tmp["R2"]),
    vmin=0.5,
    vmax=2,
    logy=True,
    s=35,
)

df_tmp = tmp[["N", "Rb", "Fh", "E_A/E_K"]]
letters = {v: k for k, v in params_simuls_regimes.items()}

for index, (N, Rb, Fh, ratio) in df_tmp.iterrows():
    if (N, Rb) not in letters:
        continue
    letter = letters[(N, Rb)]
    coef_x = 0.85
    coef_y = 0.7
    if letter == "O":
        coef_x = 0.7
        coef_y = 1.04
    elif letter == "W":
        coef_x = 1.09
        coef_y = 0.95
    elif letter == "L":
        coef_y = 1.2
    elif letter == "P":
        coef_x = 0.95
        coef_y = 1.13

    ax.text(coef_x * Fh, coef_y * ratio, letter, color="r")

xs = np.linspace(1.0e-1, 5e-1, 4)
ax.plot(xs, 2e-2 * xs**-1)
ax.text(0.12, 0.06, "${F_h}^{-1}$")

xs = np.linspace(8e-1, 4, 4)
ax.plot(xs, 1.5e-2 * xs**-2)
ax.text(0.8, 0.004, "${F_h}^{-2}$")

ax.set_xlabel("$F_h$")
ax.set_ylabel(r"$E_A / E_K$")

fig = ax.figure

fig.text(0.84, 0.07, r"$\log_{10}(\mathcal{R})$", fontsize=12)

add_letters(fig, "b")
fig.tight_layout()
save_fig(fig, "fig_ratio_EA_EK_vs_Fh.png")

if __name__ == "__main__":
    plt.show()
