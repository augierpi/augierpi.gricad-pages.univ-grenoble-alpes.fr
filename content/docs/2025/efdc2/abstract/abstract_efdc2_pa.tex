\documentclass[a4paper]{article}
\usepackage{graphicx}
\usepackage[top=10mm, bottom=25mm, left=30mm, right=30mm, includehead]{geometry}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{mathptmx}
%\usepackage{parskip}
\usepackage{amsmath}

\pagestyle{fancy}
\fancyhf{}
\date{}
\setlength{\headheight}{23.10004pt}
\setlength{\parindent}{0pt}

\fancypagestyle{plain}{
    \fancyhead[R]{\textit{$2$nd European Fluid Dynamics Conference (EFDC2)\\ $26 - 29$ August $2025$, Dublin, Ireland}}
    \renewcommand{\headrulewidth}{0pt}
    }

\newcommand{\eps}{\varepsilon}


% -------------------------------------------------------------
% ------------------ start your abstract here -----------------
% -------------------------------------------------------------

\title{\bfseries\large{Vectorial energy fluxes in stratified turbulence: testing fundamental exact laws}}

\author{
\underline{Pierre Augier}\footnote{LEGI, Universit'e Grenoble Alpes, CNRS, Grenoble, France}, \hspace{0.05cm}
Wesley Agoua${}^*$ \hspace{0.02cm}
and \hspace{0.02cm} Nicolas Mordant${}^*$ \hspace{0.05cm}
}

\begin{document}
\maketitle

We present a comprehensive numerical study investigating energy transfer mechanisms in
stratified turbulence, building upon and rigorously testing fundamental theoretical
predictions developed over the past decade.
%
While exact results in isotropic homogeneous turbulence are well-established through
classic works like the von Kármán-Howarth equation\footnote{\label{ref_karman} von
Kármán and Howarth, \emph{Proc. R. Soc. London} \textbf{164}, 192 (1938)} and
associated Kolmogorov laws, stratified turbulence is anisotropic and presents more
complex dynamics.
%
For isotropic turbulence, the 4/3rd law writes

\begin{equation}
\langle\delta\mathbf{u(r)}^2\delta
u_{L}(r)\rangle =-\frac{4}{3}\eps_{K}, \label{eq_kolmo}
\end{equation}

where $\delta\mathbf{u(r)}$ is the vectorial velocity increment and $\delta u_{L}$ its
longitudinal component. Augier et al. (2012)\footnote{\label{ref_augier} Augier,
Galtier and Billant, \emph{J. Fluid Mech.} \textbf{709}, 659-670 (2012)} previously
derived extensions of these laws for stratified turbulence, introducing a framework for
studying vectorial anisotropic nonlinear fluxes through equations that generalize
classical turbulence relations. The vectorial fluxes

\begin{equation}
\mathbf{J}(r_{h},r_{v}) = \left\langle\left[ \delta\mathbf{u}^2+\left(\dfrac{\delta
b}{N}\right)^{2}\right] \delta\mathbf{u}\right\rangle, \label{eq_J}
\end{equation}

with $N$ the Brunt-Väisälä frequency and $b$ the buoyancy, is related to the total
energy dissipation rate $\eps_{tot}$ by

\begin{equation}
\nabla\cdot\mathbf{J}= -4\eps_{tot}. \label{eq_div_J}
\end{equation}

Using the Helmholtz decomposition and symmetry arguments, integration of
(\ref{eq_div_J}) yields a generalization of the 4/3rd law (\ref{eq_kolmo}):

\begin{equation}
\mathbf{J}(r_{h},r_{v})=\dfrac{4\epsilon}{n^{2}+2}(r_{h}\mathbf{e_{h}}+n^{2}r_{v}\mathbf{e_{v}}) + \nabla G,
\end{equation}

with $n$ a free parameter and $\nabla^{2}G=0$.

Our research focuses on critically examining these theoretical predictions through
high-resolution direct numerical simulations, addressing a significant gap in numerical
verification that has persisted since these theoretical predictions were first
proposed.

In contrast with Yokoyama and Takaoka (2021)\footnote{\label{ref_yokoyama} Yokoyama and
Takaoka, \emph{J. Fluid Mech.} \textbf{908} (2021)}, who computed vectorial energy
fluxes in spectral space ($\boldsymbol{\Pi}(k_{h},k_{v})$), we compute
$\mathbf{J}(r_{h},r_{v})$, which is the quantity appearing in the generalized
Kármán-Howarth equation (\ref{eq_div_J}). However, following Yokoyama and Takaoka, we
compute the terms in the spectral space during the simulations, allowing us to gather
good statistics and obtained good convergence of these third-order quantities which
computation involves the summation of large positive and negative terms.

Our investigation systematically explores the non-universality of stratified turbulence
by examining how vectorial fluxes depend on different large-scale forcing schemes
(isotropic, in waves or in vertical vorticity) and varying nondimensional parameters.
We aim to determine whether these vectorial energy fluxes can serve as distinctive
indicators for characterizing different turbulent regimes.

Preliminary results suggest that the vectorial fluxes capture energy transfer
mechanisms that traditional scalar measurements might overlook. By presenting detailed
comparisons between theoretical predictions and simulation data, we provide new
insights into the complex energy dynamics of stratified turbulent flows.
%
We will show how this study can easily be reproduced using the open-source CFD
framework Fluidsim\footnote{\label{ref_fluidsim} Mohanan, Bonamy, Linares and Augier,
\emph{J. Open Research Soft.} \textbf{7} (2019)}.

% Figure~\ref{fig:abstract} provides an example for including figures.

% As in the example reference${}^{\ref{note1}}$,

% the title of the reference should not be given,
% the journal name should appear in italic, the volume number in bold, the page number in
% normal text and the year in parenthesis.

% \vspace{2cm}

% \begin{figure}[h]
% \centering
% \includegraphics[width=.99\textwidth]{EFDCLogo_Landscape.png}
% \caption{Caption of this figure.}
% \label{fig:abstract}
% \end{figure}

\end{document}
