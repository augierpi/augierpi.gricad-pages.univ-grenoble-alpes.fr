import time
import sys


def short_calcul(n):
    result = 0
    for i in range(1, n+1):
        result += i
    return result


def long_calcul(num):
    result = 0
    for i in range(num):
        result += short_calcul(i) - short_calcul(i)
    return result


number = 1000
time_total = 8

for _ in range(10):
    long_calcul(number)

n_test = 10
assert short_calcul(n_test) == n_test * (n_test + 1) // 2
assert long_calcul(number) == 0

t_start = time.perf_counter()
long_calcul(number)
t_1_long_calcul = time.perf_counter() - t_start

number_long_calcul = int(time_total / t_1_long_calcul)


t_start = time.perf_counter()
for _ in range(number_long_calcul):
    long_calcul(number)
t_tot = time.perf_counter() - t_start

print(sys.version)
print(f"Number of long_calcul per second: {number_long_calcul/t_tot:.2f}")
