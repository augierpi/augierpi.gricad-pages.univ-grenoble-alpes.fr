
from fluidimage.piv import Work

params = Work.create_default_params()

params.piv0.shape_crop_im0 = 64
params.piv0.grid.overlap = 0.5

params.multipass.number = 3
params.multipass.use_tps = False

params.fix.displacement_max = 3
params.fix.correl_min = 0.1
params.fix.threshold_diff_neighbour = 3

params.series.path = "/data/pivchallenge/PIV2005C/Images/c*.bmp"

work = Work(params=params)

piv = work.process_1_serie()

piv.display(show_interp=False, scale=0.1, show_error=True)
