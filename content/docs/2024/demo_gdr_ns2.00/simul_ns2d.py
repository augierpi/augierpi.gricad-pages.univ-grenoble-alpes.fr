from math import pi

from fluiddyn.util import mpi

from fluidsim.solvers.ns2d.solver import Simul

params = Simul.create_default_params()

params.output.sub_directory = "demo"

params.oper.nx = params.oper.ny = nh = 256
params.oper.Lx = params.oper.Ly = Lh = 2 * pi

params.init_fields.type = "noise"
params.init_fields.noise.velo_max = 0.01

params.time_stepping.t_end = 40.0

params.forcing.enable = True
params.forcing.type = "tcrandom"
params.forcing.forcing_rate = flux_enstropy = 1.0
params.forcing.nkmin_forcing = 8
params.forcing.nkmax_forcing = 10

k_min = 2 * pi / Lh
k_f = params.forcing.nkmin_forcing * k_min

flux_energy = k_f**2 * flux_enstropy

delta_x = Lh / nh
params.nu_8 = 0.2 * flux_enstropy * delta_x**8
params.nu_m4 = 1.0 * k_min**4

params.output.periods_print.print_stdout = 2.5

params.output.periods_save.phys_fields = 2.0
params.output.periods_save.spectra = 0.5
params.output.periods_save.spatial_means = 0.05
params.output.periods_save.spect_energy_budg = 0.5
params.output.periods_save.increments = 0.5

sim = Simul(params)

sim.time_stepping.start()

mpi.printby0(f"fluidsim-ipy-load {sim.output.path_run}")
