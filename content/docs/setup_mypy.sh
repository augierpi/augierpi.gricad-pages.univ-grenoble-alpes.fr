#!/usr/bin/env bash
# We set up a virtualenv. Then the user can easily install packages
# without being root.

distrib_code_name=`lsb_release -c | grep "Codename:"  | awk -F ' ' '{print $2}'`
module load python

python_version=$(python -c "import sys; print(sys.version[:5])")
MYPY=$HOME/useful/save/opt/mypy$python_version_${distrib_code_name}

echo "Setup a virtualenv on $HOSTNAME ($distrib_code_name) based on Python $python_version. 
The virtualenv will be located in
$MYPY
"

if [ ! -d $MYPY ]; then
mkdir -p $MYPY
virtualenv --system-site-packages $MYPY
fi

if [ ! -d $HOME/useful/save/bin ]; then
mkdir -p $HOME/useful/save/bin
fi

echo "
module load python/$python_version
export VIRTUAL_ENV_DISABLE_PROMPT=0
MYPY=\$HOME/useful/save/opt/mypy${python_version}_${distrib_code_name}
source $MYPY/bin/activate
" > $HOME/useful/save/bin/load_mypy${python_version}_${distrib_code_name}.sh

# And add aliases in ~/.bash_aliases
echo "

# lines added by a script that sets up the Python $python_version virtualenv
alias load_mypython='source \$HOME/useful/save/bin/load_mypy${python_version}_${distrib_code_name}.sh'
alias ipython_pylab='ipython --pylab'
alias ipython_notebook='ipython notebook --ip 127.0.0.1'

" >> $HOME/.bash_aliases

echo "
You may want to add these lines at the end of your ~/.bashrc: 

if [ -n "$PS1" ]; then # interactive shell
    load_mypython
fi

"

