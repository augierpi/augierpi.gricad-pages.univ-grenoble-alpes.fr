import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, R2_limit

from util_dataframe import df

ax = plot(
    df,
    "R2",
    "I_dissipation",
    c=np.log10(df["Fh"]),
    vmin=-2,
    vmax=-1,
    logy=True,
    figsize=0.55,
)

ax.axvline(R2_limit, linestyle=":")

ax.set_xlabel(r"$\mathcal{R}$")
ax.set_ylabel("$I_{diss}$")

fig = ax.figure

fig.text(0.82, 0.07, r"$\log_{10}(F_h)$", fontsize=12)


coef_lower = 1.3
fontsize = 11

y = 0.12
ax.text(0.3, y, r"Viscosity", fontsize=fontsize, alpha=0.5)
ax.text(0.3, y/coef_lower, r"affected", fontsize=fontsize, alpha=0.5)

ax.text(100, 0.6, r"Turbulent", fontsize=fontsize, alpha=0.5)

fig.tight_layout()
save_fig(fig, "fig_isotropy_diss_vs_R.png")

if __name__ == "__main__":
    plt.show()
