import sys

from functools import lru_cache

from fluidsim import load

from util import get_path_finer_resol, get_paths_couple


def load_1_sim_from_sys_argv():
    N = int(sys.argv[-2])
    Rb = int(sys.argv[-1])
    return *load_sim(N, Rb), N, Rb


@lru_cache
def load_sim(N, Rb, nx=None):
    if nx is not None:
        paths = get_paths_couple(N, Rb)
        path_dir = [p for p in paths if f"_{nx}x{nx}x" in p.name][-1]
    else:
        path_dir = get_path_finer_resol(N, Rb)

    if path_dir is None:
        raise Exception(f"No simulation found for {N = }, {Rb = }")

    _sim = load(path_dir, hide_stdout=1)

    path_periodogram = sorted(
        path_dir.glob("spatiotemporal/periodogram_temporal*")
    )[-1]

    _tmin = float(path_periodogram.name.split("_")[2])

    return _sim, _tmin


# N = 40
# Rb = 20

# N = 80
# Rb = 10

# N = 80
# Rb = 10
# nx = 1920

if __name__ == "__main__":
    sim, tmin = load_sim(40, 20)