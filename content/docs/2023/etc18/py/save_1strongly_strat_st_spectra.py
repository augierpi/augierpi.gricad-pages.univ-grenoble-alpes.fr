import matplotlib.pyplot as plt

from util import save_fig
from util_1_sim import load_1_sim_from_sys_argv

sim, tmin, N, Rb = load_1_sim_from_sys_argv()


def plot_kzkhomega(
    key_field, equation, xmax=50, ymax=None, vmin=-9, vmax=-4.5, **kwargs
):
    sim.output.spatiotemporal_spectra.plot_kzkhomega(
        key_field,
        tmin=tmin,
        equation=equation,
        xmax=xmax,
        ymax=ymax,
        vmin=vmin,
        vmax=vmax,
        **kwargs,
    )

    fig = plt.gcf()
    ax = fig.axes[0]

    if equation.startswith("omega"):
        equation = f"$\{equation.replace('*', '')}$"

    ax.set_title(f"{key_field}, {equation}")
    fig.tight_layout()


def main(key, equation, vmax):
    plot_kzkhomega(key, equation=equation, vmax=vmax)
    equation = equation.replace("=", "").replace("*", "")
    save_fig(plt.gcf(), f"fig_1strongly_strat_st_spectra_{key}_{equation}.png")


main("Khr", equation="omega=0.1*N", vmax=-4.5)
main("Kp", equation="omega=0.1*N", vmax=-4.5)
main("Khr", equation="omega=0.6*N", vmax=-6.6)
main("Kp", equation="omega=0.6*N", vmax=-6.6)

if __name__ == "__main__":
    plt.show()
