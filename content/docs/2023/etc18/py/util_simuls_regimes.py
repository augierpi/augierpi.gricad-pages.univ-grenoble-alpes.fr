from fluidsim import load

from util import paths_simuls_regimes, params_simuls_regimes

_simuls = {}


def get_sim(letter):

    global _simuls

    if letter in _simuls:
        return _simuls[letter]
    else:
        sim = load(paths_simuls_regimes[letter], hide_stdout=True)
        _simuls[letter] = sim
        return sim
