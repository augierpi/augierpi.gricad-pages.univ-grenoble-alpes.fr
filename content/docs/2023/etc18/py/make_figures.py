import sys
from runpy import run_path

import matplotlib.pyplot as plt

from fluiddyn.util import has_to_be_made as _has_to_be_made

if "SAVE" not in sys.argv:
    sys.argv.append("SAVE")

from util import tmp_dir, has_to_save, here


def has_to_be_made(name, sources: list):
    if not isinstance(name, str):
        names = name
        return any(has_to_be_made(name, sources) for name in names)

    if isinstance(sources, str):
        sources = [sources]

    sources.append("util.py")

    if not any(name.endswith(ext) for ext in (".png", ".tex")):
        name += ".png"

    result = _has_to_be_made(tmp_dir / name, sources, source_dir=here)

    if result:
        print(tmp_dir / name, sources)

    return result


def make_fig(name, source=None):
    if source is None:
        source = f"save_{name}.py"
    if has_to_be_made(f"fig_{name}", source):
        run_path(str(here / source))


def make_table(name, source=None):
    if source is None:
        source = f"save_table_{name}.py"
    if has_to_be_made(f"table_{name}.tex", source):
        run_path(str(here / source))


# make_table("methods_1couple")
# make_table("better_simuls")
# make_table("simuls_regimes")

# make_fig("E_vs_time_N40_Ri20")
# make_fig("means_vs_kmaxeta_N40_Ri20")
# make_fig("spectra_1couple")

make_fig("kmaxeta_vs_FhR")
# make_fig("epsK2overepsK_vs_FhR")

make_fig("isotropy_coef_vs_FhR")
make_fig("isotropy_velo_vs_Fh")
make_fig("isotropy_diss_vs_R")

make_fig("mixing_coef_vs_FhR")
make_fig("mixing_coef_vs_Fh")

make_fig("ratio_EA_EK_vs_Fh")
make_fig("ratio_Etoro_EK_vs_Fh")
make_fig("E_vs_Fh")
make_fig("ratio_Ewaves_vs_Fh")

sys.argv.extend(["", ""])

for N, Rb in ((40, 20), (80, 10)):
    sys.argv[-2] = str(N)
    sys.argv[-1] = str(Rb)

    make_fig(f"spectra_k_N{N}_R{Rb}.png", "save_spectra_k.py")
    make_fig(f"spectra_omega_N{N}_R{Rb}.png", "save_spectra_omega.py")
    # make_fig(
    #     "1strongly_strat_st_spectra_Khr_omega0.1N",
    #     "save_1strongly_strat_st_spectra.py",
    # )

# make_fig("old_simuls_vs_FhR")

if not has_to_save:
    plt.show()
