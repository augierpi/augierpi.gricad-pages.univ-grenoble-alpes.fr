# Presentation at Mercurial conference 2023 in Paris

https://mercurial.paris/events/mercurial-conference-paris-2023/

## Preliminary note

- User feedback in a particular context: academic. Versioning for the dummies
  (research and teaching).

- Using versioning for research/teaching.

- Teaching versioning.

### Context: who I am?

- researcher in fluid mechanics, turbulence & instabilities. Experiments and
  simulations.

- A lot of software development (open-source). Unlike most of my colleagues...

- Learned Mercurial in 2014 (compared to Github in 2008) because (i) I read that
  hg was easier than Git yet powerfull and (ii) I don't like monopoly (so Github).

- I stayed with Mercurial because my first experiences with Git weren't so
  pleasant.

- Bitbucket user before end of support for Mercurial, then adopted Heptapod.

- hg-git user to interact with Git repositories on Github and Gitlab.

- Teach hg and hg-git to post-docs/PhDs and also students

### In my group, we use version control for

- software development

- code related to scientific communications (articles, presentations, notes) and
  few websites...

```{note}

Dropbox like services (Owncloud and Nextcloud) to build/share datasets.

```

### In my group, different workflows

- With Heptapod:

  - topic based workflow with 1 branch and Github mirroring for visibility and for
    some services, like Github Actions

  - Simple 1 branch workflow without MR

- With Github / Gitlab:

  - bookmark based workflow

  - Simple 1 branch workflow without MR

```{note}

- topic based workflow with Heptapod is great, especially to collaborate with
  beginners/students

- Unnamed branches simplify things for beginners. Learning merge is soon
  necessary.

```

### Does it worth it? Versioning and hg

#### Why versioning and web platforms

- saving
- working on different computers
- collaboration
- CI, Merge requests, ...

Evident for programming, less for other activities...

Should command line based versioning tools be used by academics using "code"
(examples: Latex, programming)? Some people prefer more integrated tools (for
example Overleaf, Dropbox, layers between Git and the users...).

#### Advantages of Mercurial over Git?

- Better UI / simpler (???)
- No Git index
- Phases (security/simplicity from the client)
- evolve (collaborative and secure history rewriting)
- Git branches are like bookmarks, i.e. not great

#### Disadvantages of Mercurial compared to Git

- incompatible with many tools in a world where for most people "version control"
  is Git.

- Nothing like Github for Mercurial (mass, Github Actions, integration with other
  tools, ...)

#### Conclusions on does it worth it?

For communities in which people who are not developers need to write and share
code, Mercurial still make sense.

### Github cannot be fully avoided

Github is so widely used. People need to be able to interact with project on
Github.

Which client for scientists? Command line based? Git? Or Mercurial with hg-git as
an alternative Git client?

### Issues with Mercurial

- Not easy to install/setup Mercurial with hg-git + hg-evolve (compared to Git).

- Configuration (slightly harder than with Git)

- Documentation (hg, hg-git, + lack a good doc reader app. (Could use Textual),
  outdated contents on the web)

- University Gitlab instances are not running Heptapod

- Bookmark based hg-git workflow less nice than topic based workflow

- Topic: after a merge on Heptapod,
  `hg pull; hg up; hg up; hg topic new-topic-name`

- Incompatible tools (for example setuptools-scm, MyBender, precommit...).

### Some possible goals for the Mercurial community

- Mercurial as a recognized interesting Git client
- Transition to a multi version control tools world (compatibility)
- University Gitlab instances transition to Heptapod
