
from pathlib import Path
from time import sleep
from subprocess import run

from fluiddyn.util import has_to_be_made

path_input = Path("pres.qmd")
path_output = Path("pres.html")

while True:
    if has_to_be_made(path_output, path_input):
        run("quarto render pres.qmd --to revealjs".split())

    sleep(0.5)
