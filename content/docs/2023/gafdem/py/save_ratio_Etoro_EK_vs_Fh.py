import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, Fh_limit, Fh_passive

from util_dataframe import df

df_tmp = df[df.R2 > 5].copy()
df_tmp["Etoro/E_K"] = 100 * df_tmp.Etoro / (df_tmp.EKh)
ax = plot(
    df_tmp,
    "Fh",
    "Etoro/E_K",
    c=np.log10(df_tmp["R2"]),
    vmin=0.5,
    vmax=2,
    # logy=True,
    s=35,
)

ax.set_xlabel("$F_h$", fontsize=14)
ax.set_ylabel("$100 E_{toro}/E_K$ (%)", fontsize=14)

ax.set_xlim((1e-3, 20))
ax.set_ylim((40, 100))

ax.axvline(Fh_limit, linestyle=":")
ax.axvline(Fh_passive, linestyle=":")


coef_lower = 1.065
fontsize = 11

def text_2_lines(x, y, line0, line1):
    ax.text(x, y, line0, fontsize=fontsize, alpha=0.5)
    ax.text(x, y/coef_lower, line1, fontsize=fontsize, alpha=0.5)


ax.text(4e-3, 80, "LAST", fontsize=fontsize, alpha=0.5)
text_2_lines(0.1, 60, "Weakly", "stratified")
text_2_lines(2.5, 65, "Passive", "scalar")


fig = ax.figure

fig.text(0.82, 7, r"$\log_{10}(\mathcal{R})$", fontsize=14)

fig.tight_layout()
save_fig(fig, "fig_ratio_Etoro_EK_vs_Fh.png")

if __name__ == "__main__":
    plt.show()
