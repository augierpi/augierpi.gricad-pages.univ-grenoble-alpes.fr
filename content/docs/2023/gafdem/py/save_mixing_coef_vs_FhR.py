import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, params_simuls_regimes, Fh_limit, R2_limit, Fh_passive

from util_dataframe import df

ax = plot(
    df,
    "Fh",
    "R2",
    c=df["Gamma"],
    logy=True,
    vmin=0.0,
    vmax=1.0,
    s=35,
)

df_tmp = df[["N", "Rb", "Fh", "R2"]]
letters = {v: k for k, v in params_simuls_regimes.items()}

ax.set_xlim([1e-3, 20])
ax.set_ylim([1e-1, 1e5])


ax.axvline(Fh_limit, linestyle=":")
ax.axvline(Fh_passive, linestyle=":")
Fh_min, Fh_max = ax.get_xlim()
ax.plot([Fh_min, Fh_limit], [R2_limit, R2_limit], linestyle=":")

ax.set_xlabel(r"$F_h$", fontsize=14)
ax.set_ylabel(r"$\mathcal{R}$", fontsize=14)

Fh = np.array([1e-3, 2e1])
ax.plot(Fh, 200 * Fh**2, "k-")

ax.text(2e-3, 1e3, r"Strongly", fontsize=12, alpha=0.5)
ax.text(2e-3, 3e2, r"stratified", fontsize=12, alpha=0.5)
ax.text(2e-3, 1e2, r"(LAST like)", fontsize=12, alpha=0.5)

ax.text(1.0e-1, 1e4, r"Weakly", fontsize=12, alpha=0.5)
ax.text(1.0e-1, 3e3, r"stratified", fontsize=12, alpha=0.5)

ax.text(2.1e0, 3e4, r"Passive", fontsize=12, alpha=0.5)
ax.text(2.1e0, 9e3, r"scalar", fontsize=12, alpha=0.5)

ax.text(2e-3, 1e0, r"Viscosity", fontsize=12, alpha=0.5)
ax.text(2e-3, 3e-1, r"affected", fontsize=12, alpha=0.5)

ax.text(8e-1, 1e1, r"Viscous flows", fontsize=12, alpha=0.5)
ax.text(8e-1, 3e0, r"$Re < 200$", fontsize=12, alpha=0.5)

fig = ax.figure
fig.tight_layout()
fig.text(
    0.85,
    0.07,
    r"$\Gamma=\frac{\varepsilon_{\rm A}}{\varepsilon_{\rm K}}$",
    fontsize=14,
)

save_fig(fig, "fig_mixing_coef_vs_FhR.png")

if __name__ == "__main__":
    plt.show()
