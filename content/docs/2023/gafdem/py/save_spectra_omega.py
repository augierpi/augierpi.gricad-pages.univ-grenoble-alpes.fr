
import numpy as np
import matplotlib.pyplot as plt

from fluidsim.util import ensure_radians

from util import save_fig
from util_1_sim import load_1_sim_from_sys_argv

sim, tmin, N, Rb = load_1_sim_from_sys_argv()

tspectra = sim.output.spatiotemporal_spectra.load_temporal_spectra(tmin=tmin)
omegas = tspectra["omegas"]

coef_compensate = 2

omegas_no_0 = omegas.copy()
omegas_no_0[0] = 1e-15
norm = omegas_no_0**-coef_compensate
norm[0] = np.nan

fig, ax = plt.subplots()

EKp = tspectra["spectrum_Khd"] + 0.5 * tspectra["spectrum_vz"]
EKhr = tspectra["spectrum_Khr"]
EK = EKhr + EKp

N = sim.params.N
omegas = omegas / N

ax.plot(
    omegas,
    EKhr / norm,
    "r--",
    linewidth=1,
    label=r"$E_{K,toro}$",
)

ax.plot(
    omegas,
    EKp / norm,
    "r:",
    linewidth=1,
    label=r"$E_{K,polo}$",
)

EA = tspectra["spectrum_A"]

ax.plot(omegas, EA / norm, "b", linewidth=2, label=r"$E_A$")

# omega^(-5/3) scaling
omegas_scaling = np.arange(0.4, 1 + 1e-15, 0.01)
EK_N = (EK / norm)[abs(omegas - 1).argmin()]  # value at N
scaling_y = 0.3 * EK_N * omegas_scaling ** (-5/3 + coef_compensate)

ax.plot(omegas_scaling, scaling_y, "k-")

ax.text(0.5, 0.5, "$\omega^{-5/3}$")

# eye guide at N
ax.axvline(1, linestyle="dotted")

ax.set_xlabel(r"$\omega/N$")
ax.set_ylabel(rf"$E(\omega) \omega^{coef_compensate}$")

ax.set_xscale("log")
ax.set_yscale("log")

ymin, ymax = ax.get_ybound()
factor = 2
angle = ensure_radians(sim.params.forcing.tcrandom_anisotropic.angle)

delta_angle = sim.params.forcing.tcrandom_anisotropic.delta_angle
delta_angle = ensure_radians(delta_angle)
omega_fmin = N * np.sin(angle - 0.5 * delta_angle)
omega_fmax = N * np.sin(angle + 0.5 * delta_angle)
omegas_f = N * np.logspace(-3, 3, 1000)
where = (omegas_f > omega_fmin) & (omegas_f < omega_fmax)
ax.fill_between(omegas_f / N, ymin, ymax, where=where, alpha=0.5)
omega_tmp = 0.5 * (omega_fmin + omega_fmax) / N

ax.set_ybound(ymin, ymax)
ax.text(
    omega_tmp,
    factor * ymin,
    r"$\omega_{f}/N$",
    ha="center",
    va="center",
    size=10,
)

ax.set_xlim(right=2.0)

ax.legend(loc='lower right')
fig.tight_layout()

save_fig(fig, f"fig_spectra_omega_N{N:.0f}_R{Rb}.png")

if __name__ == "__main__":
    plt.show()
