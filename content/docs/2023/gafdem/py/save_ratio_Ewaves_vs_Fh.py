import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, Fh_limit, Fh_passive

from util_dataframe import df

df_tmp = df[df.R2 > 5].copy()

df_tmp["ratio Ewaves"] = 100 * df_tmp["E_waves"] / df_tmp["E_waves_norm"]

ax = plot(
    df_tmp,
    "Fh",
    "ratio Ewaves",
    c=np.log10(df_tmp["R2"]),
    vmin=0.5,
    vmax=2,
    # logy=True,
    s=35,
)


ax.axvline(Fh_limit, linestyle=":")
ax.axvline(Fh_passive, linestyle=":")

ax.set_xlabel("$F_h$")
ax.set_ylabel(r"$100 E_{waves} / (E_A + E_{polo})$ (%)")

ax.set_xlim(left=1e-3)


coef_lower = 1
fontsize = 11

def text_2_lines(x, y, line0, line1):
    ax.text(x, y, line0, fontsize=fontsize, alpha=0.5)
    ax.text(x, y - coef_lower, line1, fontsize=fontsize, alpha=0.5)


ax.text(4e-3, 6, "LAST", fontsize=fontsize, alpha=0.5)
text_2_lines(0.1, 9, "Weakly", "stratified")
text_2_lines(2.5, 2, "Passive", "scalar")


fig = ax.figure

fig.text(0.82, 0.07, r"$\log_{10}(\mathcal{R})$", fontsize=14)

fig.tight_layout()
save_fig(fig, "fig_ratio_Ewaves_vs_Fh.png")

if __name__ == "__main__":
    plt.show()
