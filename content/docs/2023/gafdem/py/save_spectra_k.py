import numpy as np
import matplotlib.pyplot as plt

from util import save_fig
from util_1_sim import load_1_sim_from_sys_argv

sim, tmin, N, Rb = load_1_sim_from_sys_argv()

data = sim.output.spectra.load1d_mean(tmin=tmin, verbose=False)

kx = data["kx"]
kz = data["kz"]
E_A_kx = data["spectra_A_kx"]
E_A_kz = data["spectra_A_kz"]
E_Khr_kx = data["spectra_Khr_kx"]
E_Khr_kz = data["spectra_Khr_kz"]
E_Khd_kx = data["spectra_Khd_kx"]
E_Khd_kz = data["spectra_Khd_kz"]
E_vz_kx = data["spectra_vz_kx"]
E_vz_kz = data["spectra_vz_kz"]

E_Kp_kx = E_Khd_kx + E_vz_kx
E_Kp_kz = E_Khd_kz + E_vz_kz

fig, ax = plt.subplots()

ax.set_xscale("log")
ax.set_yscale("log")

coef_norm = -5 / 3
norm_kx = kx**coef_norm
norm_kz = kz**coef_norm

alpha = 0.3

ax.plot(
    kx,
    E_Khr_kx / norm_kx,
    "r--",
    linewidth=1,
    label=r"$E_{K,toro}(k_h)$",
)

ax.plot(
    kz,
    E_Khr_kz / norm_kz,
    "r--",
    linewidth=1,
    alpha=alpha,
    label=r"$E_{K,toro}(k_z)$",
)


ax.plot(
    kx,
    E_Kp_kx / norm_kx,
    "r:",
    linewidth=1,
    label=r"$E_{K,polo}(k_h)$",
)

ax.plot(
    kz,
    E_Kp_kz / norm_kz,
    "r:",
    linewidth=1,
    alpha=alpha,
    label=r"$E_{K,polo}(k_z)$",
)


ax.plot(
    kx,
    E_A_kx / norm_kx,
    "b",
    linewidth=1,
    label=r"$E_A(k_h)$",
)

ax.plot(
    kz,
    E_A_kz / norm_kz,
    "b",
    linewidth=1,
    alpha=alpha,
    label=r"$E_A(k_z)$",
)


k = np.array([20, 200])
ax.plot(k, 2e-1 * k ** (-2 + 5 / 3), "k")
ax.text(30, 3e-2, "$k^{-2}$")

k = np.array([150, 400])
ax.plot(k, 4e3 * k ** (-3 + 5 / 3), "k")
ax.text(150, 1.8, "$k^{-3}$")

ax.set_ylim(bottom=1e-2, top=7)

ax.set_xlabel(r"$k_i$")
ax.set_ylabel(r"$E(k_i){k_i}^{5/3}$")

ax.legend(loc="upper right")
fig.tight_layout()

save_fig(fig, f"fig_spectra_k_N{N}_R{Rb}.png")

if __name__ == "__main__":
    plt.show()
