import sys

import numpy as np
import matplotlib.pyplot as plt

from util_simuls_regimes import get_sim

from util import save_fig, add_letters

print(sys.argv)
letter = sys.argv[-1]

if letter not in "VLOWP":
    letter = "L"

sim = get_sim(letter)

t_start, t_last = sim.output.print_stdout.get_times_start_last()
tmin = max(10, min(t_start + 2, t_last - 1))

sim.output.spectra.plot1d(tmin=tmin, coef_compensate=5 / 3)

ax = plt.gca()
fig = ax.figure

if letter == "W":
    k = np.array([7, 50])
    ax.plot(k, 0.03 * k ** (-1.35 + 5 / 3), "k")
    ax.text(12, 0.04, "$k^{-1.35}$")

ax.set_ylim(bottom=1e-3)
ax.set_title("")
ax.set_ylabel("$E(k) k^{5/3}$")

add_letters(fig, "a")
fig.tight_layout()
save_fig(fig, f"fig_spectra_regime_{letter}.png")

if __name__ == "__main__":
    plt.show()
