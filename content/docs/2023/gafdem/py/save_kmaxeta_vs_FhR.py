import matplotlib.pyplot as plt

from util import save_fig, plot, Fh_limit, R2_limit

from util_dataframe import df

import numpy as np

ax = plot(
    df,
    "Fh",
    "R2",
    c=df["k_max*eta"],
    logy=True,
    vmin=0.0,
    vmax=1.2,
    s=35,
)
# ax.set_xlim(right=1)
ax.set_ylim(top=1e5)

# Fh = np.array([1e-3, 1e1])
# ax.plot(Fh, 2e3 * Fh**2, "k-", label=r"$Re=2\times 10^3$")

ax.set_xlabel(r"$F_h$", fontsize=14)
ax.set_ylabel(r"$\mathcal{R}$", fontsize=14)

fig = ax.figure

fig.tight_layout()
fig.text(0.85, 0.07, r"$k_{\rm max} \eta$", fontsize=14)

ax.set_xlim((1e-3, 3))
ax.set_ylim((1e-1, 3e3))

Fh_min, Fh_max = ax.get_xlim()
R_min, R_max = ax.get_ylim()
ax.axvline(Fh_limit, linestyle=":")
ax.plot([Fh_min, Fh_limit], [R2_limit, R2_limit], linestyle=":")

save_fig(fig, "fig_kmaxeta_vs_FhR.png")

if __name__ == "__main__":
    plt.show()
