import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, Fh_limit, Fh_passive

from util_dataframe import df

ax = plot(
    df,
    "Fh",
    "I_velocity",
    c=np.log10(df["R2"]),
    vmin=0.5,
    vmax=2,
    logy=True,
    figsize=0.55,
)
ax.axvline(Fh_limit, linestyle=":")
ax.axvline(Fh_passive, linestyle=":")

ax.set_xlabel("$F_h$")
ax.set_ylabel(r"$I_{velo}$")

xs = np.linspace(1e-2, 1e-1, 4)
ax.plot(xs, 8e0 * xs**1)
ax.text(0.02, 0.4, "${F_h}^1$")

coef_lower = 1.5
fontsize = 11

def text_2_lines(x, y, line0, line1):
    ax.text(x, y, line0, fontsize=fontsize, alpha=0.5)
    ax.text(x, y/coef_lower, line1, fontsize=fontsize, alpha=0.5)


text_2_lines(1.5e-3, 0.25, "Strongly", "stratified")
text_2_lines(0.1, 0.15, "Weakly", "stratified")
text_2_lines(2.5, 0.4, "Passive", "scalar")

fig = ax.figure

fig.text(0.84, 0.07, r"$\log_{10}(\mathcal{R})$", fontsize=12)

fig.tight_layout()
save_fig(fig, "fig_isotropy_velo_vs_Fh.png")

if __name__ == "__main__":
    plt.show()
