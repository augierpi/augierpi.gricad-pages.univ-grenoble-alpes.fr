import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, R2_limit, Fh_limit, Fh_passive

from util_dataframe import df

df_tmp = df[df.R2 > R2_limit].copy()
df_tmp["E_A/E_K"] = df_tmp.EA / (df_tmp.EKh)
ax = plot(
    df_tmp,
    "Fh",
    "E_A/E_K",
    c=np.log10(df_tmp["R2"]),
    vmin=0.5,
    vmax=2,
    logy=True,
    s=35,
)

ax.axvline(Fh_limit, linestyle=":")
ax.axvline(Fh_passive, linestyle=":")

xs = np.linspace(0.3, 1, 4)
ax.plot(xs, 1e-1 * xs**-1, "k-")
ax.text(0.25, 8e-2, r"${F_h}^{-1}$", fontsize=12)

xs = np.linspace(Fh_passive, 10, 4)
ax.plot(xs, 2e-1 * xs**-2, "k-")
ax.text(2.3, 0.0025, r"${F_h}^{-2}$", fontsize=12)

ax.set_xlabel("$F_h$")
ax.set_ylabel(r"$E_A / E_K$")

ax.set_xlim(left=1e-3)


coef_lower = 1.6
fontsize = 11

def text_2_lines(x, y, line0, line1):
    ax.text(x, y, line0, fontsize=fontsize, alpha=0.5)
    ax.text(x, y/coef_lower, line1, fontsize=fontsize, alpha=0.5)


ax.text(4e-3, 0.25, "LAST", fontsize=fontsize, alpha=0.5)
text_2_lines(0.1, 4e-2, "Weakly", "stratified")
text_2_lines(2.5, 0.2, "Passive", "scalar")

fig = ax.figure

fig.text(0.82, 0.07, r"$\log_{10}(\mathcal{R})$", fontsize=14)

fig.tight_layout()
save_fig(fig, "fig_ratio_EA_EK_vs_Fh.png")

if __name__ == "__main__":
    plt.show()
