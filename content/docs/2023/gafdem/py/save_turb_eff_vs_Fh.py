import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, Fh_limit, Fh_passive

from util_dataframe import df

P = 1
L_f = 1

df["turb_eff"] = P * L_f / df.E**(3/2)

ax = plot(df, "Fh", "turb_eff", c=np.log10(df["R2"]), vmin=0.5, vmax=2, logy=True)

ax.set_xlabel("$F_h$", fontsize=14)
ax.set_ylabel("$P L_f / E_{tot}^{3/2}$", fontsize=14)

ax.set_xlim((1e-3, 20))

ax.axvline(Fh_limit, linestyle=":")
ax.axvline(Fh_passive, linestyle=":")

coef_lower = 1.26
fontsize = 11

def text_2_lines(x, y, line0, line1):
    ax.text(x, y, line0, fontsize=fontsize, alpha=0.5)
    ax.text(x, y/coef_lower, line1, fontsize=fontsize, alpha=0.5)


ax.text(4e-3, 1.2, "LAST", fontsize=fontsize, alpha=0.5)
text_2_lines(0.1, 1, "Weakly", "stratified")
text_2_lines(2.5, 1, "Passive", "scalar")

fig = ax.figure

fig.text(0.82, 0.07, r"$\log_{10}(\mathcal{R})$", fontsize=14)

fig.tight_layout()
save_fig(fig, "fig_turb_eff_vs_Fh.png")

if __name__ == "__main__":
    plt.show()
