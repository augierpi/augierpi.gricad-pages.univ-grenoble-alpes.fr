import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, Fh_limit, R2_limit, Fh_passive

from util_dataframe import df

tmp = df[df.R2 > 0.7*R2_limit].copy()

ax = plot(
    tmp,
    "Fh",
    "Gamma",
    c=np.log10(tmp["R2"]),
    logy=True,
    vmin=0.5,
    vmax=2,
    s=35,
)

df_tmp = tmp[["N", "Rb", "Fh", "Gamma"]]

ax.set_xlabel("$F_h$", fontsize=14)
ax.set_ylabel(r"$\Gamma=\varepsilon_{\rm A} / \varepsilon_{\rm K}$", fontsize=14)


ax.set_xlim((1e-3, 10))

ax.axvline(Fh_limit, linestyle=":")
ax.axvline(Fh_passive, linestyle=":")


ax.plot([1e-3, 1e-2], [0.42, 0.42], "k-")
ax.text(2e-3, 0.25, r"$0.42 \times {F_h}^0$", fontsize=12)

xs = np.linspace(0.15, 0.7, 4)
ax.plot(xs, 1e-1 * xs**-1, "k-")
ax.text(0.12, 1.4e-1, r"${F_h}^{-1}$", fontsize=12)

xs = np.linspace(Fh_passive, 6, 4)
ax.plot(xs, 1e-1 * xs**-2, "k-")
ax.text(2.3, 0.02, r"${F_h}^{-2}$", fontsize=12)

coef_lower = 1.6
fontsize = 11

def text_2_lines(x, y, line0, line1):
    ax.text(x, y, line0, fontsize=fontsize, alpha=0.5)
    ax.text(x, y/coef_lower, line1, fontsize=fontsize, alpha=0.5)


ax.text(4e-3, 0.15, "LAST", fontsize=fontsize, alpha=0.5)
text_2_lines(0.05, 4e-2, "Weakly", "stratified")
text_2_lines(1.3, 0.2, "Passive", "scalar")

fig = ax.figure
fig.tight_layout()
fig.text(0.82, 0.07, r"$\log_{10}(\mathcal{R})$", fontsize=14)

save_fig(fig, "fig_mixing_coef_vs_Fh_toro.png")

if __name__ == "__main__":
    plt.show()
