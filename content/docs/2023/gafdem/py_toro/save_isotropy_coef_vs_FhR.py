import numpy as np
import matplotlib.pyplot as plt

from util import save_fig, plot, Fh_limit, R2_limit, Fh_passive

from util_dataframe import df

ax = plot(
    df,
    "Fh",
    "R2",
    c=df["I_velocity"],
    logy=True,
    vmin=0.0,
    vmax=1.0,
    s=50 * df["I_dissipation"],
)


ax.set_xlim([1e-3, 20])
ax.set_ylim([1e-1, 1e5])

ax.axvline(Fh_limit, linestyle=":")
ax.axvline(Fh_passive, linestyle=":")
Fh_min, Fh_max = ax.get_xlim()
ax.plot([Fh_min, Fh_limit], [R2_limit, R2_limit], linestyle=":")

ax.set_xlabel(r"$F_h$", fontsize=14)
ax.set_ylabel(r"$\mathcal{R}$", fontsize=14)

fig = ax.figure
fig.tight_layout()

fig.text(0.84, 0.07, r"$I_{\rm velo}$", fontsize=14)

ax_legend = fig.add_axes([0.17, 0.76, 0.2, 0.16])
ax_legend.set_xticklabels([])
ax_legend.set_xticks([])
ax_legend.set_yticklabels([])
ax_legend.set_yticks([])
isotropy_diss = np.array([0.1, 0.5, 0.9])
heights = np.array([0.2, 0.5, 0.8])
ax_legend.scatter([0.15, 0.15, 0.15], heights, s=50 * isotropy_diss)
ax_legend.set_xlim([0, 1])
ax_legend.set_ylim([0, 1])


ax.text(2e-3, 1e3, r"Strongly", fontsize=12, alpha=0.5)
ax.text(2e-3, 3e2, r"stratified", fontsize=12, alpha=0.5)
ax.text(2e-3, 1e2, r"(LAST)", fontsize=12, alpha=0.5)

ax.text(5e-2, 1e4, r"Weakly", fontsize=12, alpha=0.5)
ax.text(5e-2, 3e3, r"stratified", fontsize=12, alpha=0.5)

ax.text(2.1e0, 3e4, r"Passive", fontsize=12, alpha=0.5)
ax.text(2.1e0, 9e3, r"scalar", fontsize=12, alpha=0.5)

ax.text(2e-3, 1e0, r"Viscosity", fontsize=12, alpha=0.5)
ax.text(2e-3, 3e-1, r"affected", fontsize=12, alpha=0.5)

ax.text(4e-1, 1e1, r"Viscous flows", fontsize=12, alpha=0.5)
ax.text(4e-1, 3e0, r"$Re < 200$", fontsize=12, alpha=0.5)

Fh = np.array([1e-3, 2e1])
ax.plot(Fh, 200 * Fh**2, "k-")

ax.text

for h, i in zip(heights, isotropy_diss):
    ax_legend.text(0.28, h - 0.06, r"$I_{\rm diss} = " + f"{i}$")


save_fig(fig, "fig_isotropy_coef_vs_FhR_toro.png")

if __name__ == "__main__":
    plt.show()
