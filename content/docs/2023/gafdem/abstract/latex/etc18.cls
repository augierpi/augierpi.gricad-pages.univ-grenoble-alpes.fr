%%%
%%%	This is file 'ETC18.cls'
%%%

\ProvidesClass{ETC18}[2017/05/16 LaTeX class for European Turbulence Conference.]
\LoadClass[a4paper]{article}
\RequirePackage{graphicx}  	%Standard package

\let\@date\@empty

\textheight=220mm % 211mm % ==200mm -> due to \maketitle
\textwidth=135mm
\hoffset=-5mm
\voffset=3mm
\headsep=0pt
\headheight=0pt
\topmargin=0pt
\linespread{1.0}

% figure placement
\setcounter{topnumber}{0}
\renewcommand\bottomfraction{.7}
\setcounter{bottomnumber}{3}
