%%% 	This template for the Euromech Fluid Mechanics
%%% 	and Turbulence Conference shall be used in the
%%%	    preparation work of the abstract when using LaTeX.
%%%	    The template uses ETC18.sty and ETC18.cls which in turn
%%%	    uses LaTeX's standard article class file 'article.cls'.

\documentclass{ETC18}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% 	Title, author and affiliation
%%%
%
%	The title should be in bold (\bfseries) and in fontsize large (\large).
%	The presenting author should be underlined and the affiliation
%	uses the standard LaTeX footnote command '\footnote{}'.
%	If two or more authors have the same affiliation only the first among
%	those will be addressed a footnote and the following will be given
%	a superscript ($^{}$) with the same symbol.
%
%	Footnotes in the title, generated by article.cls, uses @fnsymbol which
%	are listed below in order of priority [to be used if the same
%	affiliation is used more than once]:
%	$^\ast$
%	$^\dag$
%	$^{\ddag}$
%	$^{\S}$
%	$^{\P}$
%	$^{\Vert}$
%
%	See the example below.
%

\title{\bfseries\large{Comprehensive open datasets of stratified turbulence forced in vertical vorticity or wave modes}}

\author{\underline{Pierre Augier}\footnote{Laboratoire des Ecoulements
G\'eophysiques et Industriels, Universit\'e Grenoble Alpes, CNRS, Grenoble-INP,
F-38000 Grenoble, France}, Vincent Labarre\footnote{Universit\'{e} C\^{o}te
d'Azur, Observatoire de la C\^{o}te d'Azur, CNRS, Laboratoire Lagrange, Nice,
France.}, Giorgio Krstulovic$^{\dag}$ and Sergey Nazarenko$^{\dag}$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% 	Abstract and Maketitle
%%%

\newcommand{\R}{\mathcal{R}}
\newcommand{\epsK}{{\varepsilon_{\!\scriptscriptstyle K}}}
\newcommand{\epsA}{{\varepsilon_{\!\scriptscriptstyle A}}}
\newcommand{\kmax}{k_{\max}}

\begin{document}

\maketitle
\thispagestyle{empty}
\vspace{-0.4cm}


We present new comprehensive open datasets of stratified turbulence in a
periodic domain with constant Brunt-Väisälä frequency $N$. The forcing is
either in large horizontal vortices or in wave
modes. More than 80 simulations were carried out with the `ns3d.strat`
pseudo-spectral solver of the open-source CFD framework Fluidsim.

This datasets have 3 main particularities. (i) The horizontally invariant shear
modes are cleanly removed from the dynamics. (ii) Large ranges of
horizontal Froude $F_h = \epsK /(N U_h^2) \in [0.007, 2]$ and buoyancy Reynolds
$\R = Re {F_h}^2 = \epsK / (\nu N^2)$ numbers are investigated, with $U_h$ the
horizontal rms velocity, $\epsK$ the kinetic energy dissipation rate, and $\nu$
the viscosity. These datasets are adapted to investigate the different
regimes of stratified flows forced with vortices, in particular the weakly
stratified regime ($F_h > 0.1$) and the Layered Anisotropic Stratified Turbulence
regime (LAST, $F_h < 0.07$ and $\R >
10$).

\vspace{-2mm}


\begin{figure}[h] \centerline{
\includegraphics[width=0.49\textwidth]{./../../../../ipynb/20220831ISSF/fig/fig_isotropy_coef_vs_FhR.png}
\includegraphics[width=0.49\textwidth]{./../../../../ipynb/20220831ISSF/fig/fig_mixing_coef_vs_Fh.png}
}
\vspace{-5mm}
\caption{\small Isotropy coefficients (left) and mixing coefficient (right), in
a space set by $\R$ and $F_h$. Each point corresponds to one simulation. (left)
Colors represent a large scale isotropy and markersize a small scale isotropy
(see legend). The large and dark markers correspond to the LAST regime.}
\label{fig}
\end{figure}

\vspace{-2mm}

% <figure>
% <table><tr>
% <td> <img src="./docs/ipynb/20220831ISSF/fig/fig_isotropy_coef_vs_FhR.png" alt="fig_isotropy_coef_vs_FhR" style="width: 90%;"/> </td>
% <td> <img src="./docs/ipynb/20220831ISSF/fig/fig_mixing_coef_vs_Fh.png" alt="fig_mixing_coef_vs_Fh" style="width: 90%;"/> </td>
% </tr></table>

% <figcaption>
% Isotropy coefficients (left) and mixing coefficient (right), in a space set by R and F_h. Each point
% corresponds to one simulation. (left) Colors represent a large scale isotropy
% and markersize a small scale isotropy (see legend). The large and dark markers correspond to the LAST regime.
% </figcaption>
% </figure>

(iii) Statistical quantities like (1) spatial, temporal and spatio-temporal spectra
and (2) spectral energy budget are provided. We will show how it will be easy for
anyone to investigate stratified turbulence with this dataset by loading and
plotting these quantities with our Python library Fluidsim.

Different physical questions will be addressed in view of numerous geophysical
applications. (i) Characterization of the different regimes (left figure), in
particular through comparisons based on spatial and temporal spectra with
theoretical results and in-situ measurements. (ii) Scaling laws for different
averaged quantities like the mixing coefficient $\Gamma=\epsA/\epsK$ (right
figure), where $\epsA$ is the potential energy dissipation rate. (iii) Presence
and degree of nonlinearity of internal gravity waves through a spatio-temporal
analysis.



%%%
%%%	General
%%%
%%%	Enter the abstract text below. The total length of the abstract is
%%%	ONE page including figure/figures, which is optional. The
%%%	font size is \normalsize. Text height and width are not
%%%	allowed to be changed and are 200 and 130 mm, respectively.
%%%	No page numbering is allowed why \thispagestyle{empty} above
%%%	shall be kept. Indent is used automatically in LaTeX when an entire
%%%	row of text is skipped and is used whenever a new paragraph is
%%%	desired.
%%%
%%%	References
%%%
%%%	The references are entered manually by using the footnote
%%%	command in LaTeX. Only the surname of the author should be
%%%	used and if more then two authors "et al." should be used. The
%%% 	title of the reference should NOT be given, however, the journal
%%%	name in italic, volume in bold and first page number followed by
%%%	the year in parenthesis should.
%%%	See example below.
%%%
%%%	Figure: optional
%%%
%%%	Make sure that the labels and all other text in the figure have
%%%	readable and comfortable font size, i.e. not smaller than 10pt.
%%%	Check this by comparing the figure text with the text in the abstract.
%%%

% \noindent This template for the European Turbulence
% Conference (ETC18) shall be used in the preparation work of the abstract when
% using \LaTeX. The template uses `ETC18.cls', which uses LaTeX's standard
% article class file `article.cls'.

% The maximum length of the abstract for ETC18 is one page \textbf{including all
% figures}, which are optional.
% Please note that \textbf{abstracts longer than one page will not be accepted}.
% The total number of words will be 250 to~430, depending on the number and size
% of the figures.
% The presenting author should be underlined.
% More information is available in the commented text directly in the
% LaTeX template `template-ETC18.tex'.

% We have tested the compatibility of this template, and it works perfectly under Windows or Linux. It also works in Overleaf.

%%%
%%%	Reference example
%%%

% Please include references in the way indicated here for
% journals\footnote{Dabbs and Hobbes, \emph{J. Fluid Mech.} \textbf{582},
% 54 (2009).}\footnote{Dabbs et al., \emph{Phys. Fluids} \textbf{18},
% 034210 (2006).} and for
% books.\footnote{Batchelor, \emph{Fluid Dynamics}, Cambridge (1967).}

%%%
%%%	Figure example
%%%

% \bigskip
% An example for a figure is given in Fig.~\ref{fig}. Figures \ref{fig}(a) and~(b)
% clearly show that the velocity increases with time and decreases with the
% downstream distance, respectively.

%%%%%%%%
%%% Figure: The figure should be placed in the bottom of the page,
%%% above the footnotes ("[h] or [h!]").
%%% Captions should be in fontsize small (\small).
%%%%%%

% \begin{figure}[h]
% \centering
% % For eps or pdf graphics
% %\raisebox{1.3in}{a)}\includegraphics{figa}\quad
% %\raisebox{1.3in}{b)}\includegraphics{figb}
% % \raisebox{1.3in}{a)}\input{figa}\quad
% % \raisebox{1.3in}{b)}\input{figb}
% \caption{\small (a) Velocity as a function of time. (b) Velocity distribution in the downstream direction.}
% \label{fig}
% \end{figure}

%%%%%%%%

\end{document}
