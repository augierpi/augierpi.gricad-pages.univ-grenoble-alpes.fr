---
title: ""
subtitle: ""
format:
  revealjs:
    slide-number: true
    chalkboard:
      buttons: false
    preview-links: auto
    # logo: images/quarto.png
    css: styles.css
    # footer: <https://quarto.org>
    scrollable: true
    theme: default
---

## Comprehensive open datasets of stratified turbulence forced in vertical vorticity or wave modes {.centered}

::: {style="text-align: center;"}

<br style='display: block; content: ""; margin-top: 0.3em;'>

**Pierre Augier**, Vincent Labarre, <br>
Giorgio Krstulovic and Sergey Nazarenko
:::
<br>

::: {style="text-align: center; font-size: 80%;"}
GAFDEM, 11-15 Sept. 2023
:::

![](../../../images/logo_CNRS.jpg){.absolute top=70% left=0 width="13%"}

![](../../../images/logo_UGA_2020.png){.absolute top=75% left="15%" width="18%"}

![](../../../images/logo_LEGI.jpg){.absolute top=75% left="35%" width="18%"}

![](../../ipynb/20220510simons-strat-turb/fig/logo_Lagrange.jpeg){.absolute top=75% left="62%" width="18%"}

![](../../ipynb/20220510simons-strat-turb/fig/logo_OCA.jpg){.absolute top=75% left="80%" width="18%"}


::: {.hidden}
$$
\newcommand{\R}{\mathcal{R}}
\newcommand{\FhR}{(F_h,\ \R)}
$$
:::


## [Turbulence influenced by a stable density stratification]{.r-fit-text}

:::: {style="font-size: 75%;"}

#### Motivations

- Internal gravity waves turbulence proposed to explain ocean measurements

- ...

#### Summary

- Internal gravity waves (IGW), "vortices", shear modes, small-scale turbulence

- Horizontal Froude number $F_h$ and buoyancy Reynolds number $\R = Re {F_h}^2$

- LAST regime $F_h<0.02$, $\R>20$:
  - downscale energy cascade
  - anisotropic spectra

::::: {align="middle" style="position: relative; margin-top: -1.0em;"}

![](../../ipynb/lmfa20170908/fig/vert_crosssection_Brethouwer.jpg){width=70% fig-align="center"}

<p style="position: absolute; bottom: 15%; left: 18%; font-size: 70%;">
    Brethouwer, Billant, Chomaz & Lindborg (2007)
</p>

<p style="position: absolute; bottom: 55%; left: 60%; font-size: 80%;">
    $\rho'(x, z)$
</p>


:::::
::::


## [Turbulence influenced by a stable density stratification]{.r-fit-text}

:::: {style="font-size: 75%;"}

**Questions**

- Regime corresponding to the oceanic waves?

::::: {.columns}
::: {.column width="50%"}
- Effect of forcing, universality?
- Regimes in $(F_h, \R)$ space?
:::
::: {.column width="50%"}
- IG wave turbulence? Nonlinearity?
- Mixing modeling?
:::
:::::

**Would be good to have "Foundational" open datasets**

::::: {.columns}
::: {.column width="50%"}
- Span the $\FhR$ space
:::
::: {.column width="50%"}
- Different forcing schemes
:::
:::::

We built 2 first datasets with $\neq$ forcing
<small>(as Waite & Bartelo, 2004, 2006, Lindborg & Brethouwer, 2007)</small>

::::: {.columns}
::: {.column width="10%"}
:::
::: {.column width="45%"}
- forced in vortices
:::
::: {.column width="45%"}
- forced in IG waves
:::
:::::

<center>
**This presentation: focus on "wave" forcing**
</center>

::::

## Span the $\FhR$ space

:::: {.columns}
::: {.column width="50%"}
Previous studies
![](../../ipynb/20220831ISSF/fig/fig_old_simuls_vs_FhR.png){width=98% fig-align="center"}
:::
::: {.column width="50%"}
This dataset ("wave" forcing)
![](tmp_saved/fig_kmaxeta_vs_FhR.png){width=98% fig-align="center"}
:::
::::


## [Open datasets and Open-source software]{.r-fit-text}

:::: {style="font-size: 75%;"}

- Reproductibility

- Reusability

  - auto-documented standard file formats.

  - software easy to extend.

- Easily understandable/usable: not only raw states. Open software to compute, read, load and represent advanced outputs.

::::

![](../../../images/logo-fluidsim.svg){width=50% fig-align="center"}

## The FluidDyn project and Fluidsim

:::: {style="font-size: 75%;"}

**FluidDyn:** a project to foster open source and Python in fluid mechanics. A set of open source collaborative Python packages: fluidsim, fluidimage, fluidlab, ...

[![](../../../images/logo-fluiddyn.jpg){width=40% fig-align="center"}](https://fluiddyn.readthedocs.io)


<p style="margin-bottom: -0.1em; margin-top: 1.3em;"><strong>Fluidsim:</strong></p>

1. A framework to create CFD solvers from whatever (for ex.
  [Snek5000](https://github.com/snek5000/snek5000),
  [Fluidsimfoam](https://fluidsimfoam.readthedocs.io))

2. Pseudo-spectral Fourier solvers (`ns2d`, `ns3d`, `ns3d.strat`, `sw1l`, ...).

[Documented](https://fluidsim.readthedocs.io), tested, very efficient, [dev hosted in foss.heptapod.net](https://foss.heptapod.net/fluiddyn/fluidsim)

::::

## Numerical methods

:::: {style="font-size: 75%;"}

- `ns3d.strat` Fluidsim solver

  (Navier-Stokes equations with constant $N$, pseudo-spectral Fourier)

- Forcing

  - Slow internal gravity waves ($\omega/N \simeq 0.3$)

  - Large horizontal scales

  - Constant energy injection rate ($P_K = 1$)

  - Time correlated

- Shear modes removed from the dynamics

- Input parameters: $N$ and $\nu$ ($F_h$ and $Re$)

- Diffusion

  Mostly DNS or quasi DNS ($k_{max} \eta \simeq 1$) but hyperviscosity for few simulations

::::

## Large and small isotropy coefficients

:::: {.columns}
::: {.column width="50%"}
Large scale isotropy
<p style="margin-top: -0.4em;">
$$I_{velo} = \frac{3 E_{K_z}}{E_K}$$
</p>
:::
::: {.column width="50%"}
Small scale isotropy
<p style="margin-top: -0.4em;">
$$I_{diss} = \frac{1 - \varepsilon_{Kz}/\varepsilon_K}{ 1 - 1/3}$$
</p>
:::
::::

<br style='display: block; content: ""; margin-top: -0.9em;'>
<center>
Isotropic turbulence $\Rightarrow I_{velo} = I_{diss} = 1$
</center>

:::: {.columns}
::: {.column width="50%"}
![](tmp_saved/fig_isotropy_velo_vs_Fh.png){width=95%}
:::
::: {.column width="50%"}
![](tmp_saved/fig_isotropy_diss_vs_R.png){width=95%}
:::
::::

## [Regimes from small and large isotropy coefficients]{.r-fit-text}

![](tmp_saved/fig_isotropy_coef_vs_FhR.png){width=80% fig-align="center"}

::::: {style="font-size: 75%;"}
Brethouwer et al. (2007), ...
:::::


## Comparison forcings: regimes

:::: {.columns}
::: {.column width="50%"}
Forced in waves
![](tmp_saved/fig_isotropy_coef_vs_FhR.png){width=100% fig-align="center"}
:::
::: {.column width="50%"}
Forced in vortices
![](tmp_saved/fig_isotropy_coef_vs_FhR_toro.png){width=100% fig-align="center"}
:::
::::


## [Mixing coefficient $\Gamma$ and energy ratio $E_A/E_K$]{.r-fit-text}

:::: {.columns}

::: {.column width="55%"}

![](tmp_saved/fig_mixing_coef_vs_FhR.png){width=100%}

::::: {style="font-size: 70%;"}
Maffioli (2017), Garanaik & Venayagamoorthy (2019),
Le Reun, Favier & Le bars (2018)
:::::

:::

::: {.column width="45%"}
![](tmp_saved/fig_mixing_coef_vs_Fh.png){width=98% style='margin-top: -0.8em; margin-bottom: -0.2em;'}
![](tmp_saved/fig_ratio_EA_EK_vs_Fh.png){width=98% style='margin-top: -0.2em; margin-bottom: -0.2em;'}
:::
::::


## [Comparison forcings: mixing coefficient]{.r-fit-text}

:::: {.columns}
::: {.column width="50%"}
Forced in waves
![](tmp_saved/fig_mixing_coef_vs_Fh.png){width=100%}
:::
::: {.column width="50%"}
Forced in vortices
![](tmp_saved/fig_mixing_coef_vs_Fh_toro.png){width=100%}
:::
::::

::::: {style="font-size: 70%;"}
Slightly different $F_h$ values for the regimes...
:::::

## [Total energy and toroidal energy]{.r-fit-text}

Same forcing (energy injection rate and scales) for all simuls

<center>
Turbulence $\Rightarrow U^3 \sim \varepsilon L$
</center>

:::: {.columns}

::: {.column width="50%"}

![](tmp_saved/fig_E_vs_Fh.png){width=95%}

:::

::: {.column width="50%"}

![](tmp_saved/fig_ratio_Etoro_EK_vs_Fh.png){width=95%}
<center style="margin-top: -1.0em;">
Energy in "vortices"
</center>
:::

::::


## [Comparison forcings: energies versus $F_h$]{.r-fit-text}

:::: {.columns}
::: {.column width="50%"}
Forced in waves
![](tmp_saved/fig_E_vs_Fh.png){width=100%}
:::
::: {.column width="50%"}
Forced in vortices
![](tmp_saved/fig_E_vs_Fh_toro.png){width=100%}
:::
::::


## [Comparison forcings: toroidal energy]{.r-fit-text}

:::: {.columns}
::: {.column width="50%"}
Forced in waves
![](tmp_saved/fig_ratio_Etoro_EK_vs_Fh.png){width=100%}
:::
::: {.column width="50%"}
Forced in vortices
![](tmp_saved/fig_ratio_Etoro_EK_vs_Fh_toro.png){width=100%}
:::
::::


## Spatial and temporal spectra

$F_h = 0.024$ and $\R = 13$ (**LAST like**)

:::: {.columns}

::: {.column width="50%"}

![](tmp_saved/fig_spectra_k_N40_R20.png){width=95%}

::::: {style="font-size: 75%; margin-top: -1.8em;"}
- Strongly anisotropic
- ${k_h}^{-5/3}$ and steep vert. spectra
- Large toroidal spectra (vortices)
:::::

:::

::: {.column width="50%"}

![](tmp_saved/fig_spectra_omega_N40_R20.png){width=95%}

:::

::::


## Comparison forcings: spectra

:::: {.columns}
::: {.column width="50%"}
Forced in waves
![](tmp_saved/fig_spectra_k_N40_R20.png){width=100%}
:::
::: {.column width="50%"}
Forced in vortices
![](tmp_saved/fig_spectra_k_N40_R20_toro.png){width=100%}
:::
::::


## Spatio-temporal spectra

:::: {.columns}
::: {.column width="50%"}
$F_h = 0.024$ and $\R = 13$
![](tmp_saved/fig_st_spectra_Kp_ikz1_N40_R20.png){width=100%}
:::
::: {.column width="50%"}
$F_h = 0.003$ and $\R = 7.1$
![](tmp_saved/fig_st_spectra_Kp_ikz1_N80_R10.png){width=100%}
:::
::::


## [Proportion of weakly nonlinear internal waves]{.r-fit-text}

(from spatiotemporal spectra)

![](tmp_saved/fig_ratio_Ewaves_vs_Fh.png){width=80%}


## Conclusions

::::: {style="font-size: 90%;"}

### Open data / open software

-  2 datasets for 2 forcing schemes:

  - forced in large scale vortices
  - forced in large scale slow waves ($\omega/N\simeq 0.3$)

- More than 40 simulations (spanning the $(F_h,\ \R)$ space) for each forcing type, soon available as **open** datasets!

- Fluiddyn / Fluidsim

- Many different types of outputs and in particular **spatiotemporal spectra**

<!-- - Other datasets for:

  - modified equations forbidden vortices
  - faster waves ($\omega/N\simeq 0.8$)
  - ... -->

:::::

## Conclusions

::::: {style="font-size: 85%;"}

### Results on strat. turbulence forced in waves

- **5 regimes** (Viscosity affected, LAST, Optimal, Weakly strat., Passive scalar)

  <small>Lindborg (2006), Brethouwer et al. (2007), Maffioli et al. (2016), Garanaik-Venayagamoorthy (2019)</small>

- Indisputable observation of the $\Gamma \sim {F_h}^{-1}$ and ${F_h}^{-2}$ scalings

- For $F_h\ll 1$ and $\R>10$ (relevant for oceans), **not a pure wave cascade**:
  - Weak forcing but strong, non-linear flows
  - Strong vortices, LAST like
  - Weakly non-linear waves dominated by other processes

:::::
