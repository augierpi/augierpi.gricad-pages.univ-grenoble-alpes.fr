Transonic 0.4.0 !
=================

:date: 2019-09-22
:modified: 2019-09-22
:tags: fluiddyn, python
:category: Methods

Short reminder: `Transonic <http://transonic.readthedocs.io/>`_ is a pure
Python package (requiring Python >= 3.6) to easily accelerate modern
Python-Numpy code with different accelerators (currently Cython, Pythran and
Numba).

We've just reached an important technical milestone with the release of the
0.4.0 version.

If you use Pythran master, note that Transonic 0.4.0 is (on purpose) only
compatible with the stable version of Pythran so one needs to install transonic
with something like::

  pip install https://github.com/fluiddyn/transonic/archive/master.zip

I consolidated the work of Pierre Blanc-fatin on the possibility to use
different Python accelerators with the same Numpy code.

.. image:: ./images/logo_Pythran.jpeg
  :width: 35%
  :alt: group
  :align: left
.. image:: ./images/logo-fluiddyn.jpg
  :width: 40%
  :alt: guys
  :align: right

.. raw:: html

    <p>

Different accelerators for one process and one module
-----------------------------------------------------

Before 0.4.0, it was not possible to use more than one backend for one process!
This strong limitation is now overcome. We now have `an API
<https://transonic.readthedocs.io/en/latest/backends.html>`_ to choose which
backend is used for a specific module or function. For example, one can write
something like:

.. code:: python

    import os
    from transonic import boost, set_backend_for_this_module

    if os.name == 'nt':  # Windows
        backend = "numba"  # or "cython"
    else:
        backend = "pythran"

    set_backend_for_this_module(backend)

    @boost
    def myfunc(...):
        ...

Since it is possible to choose with a fine granularity which backend is used
for which function, it is now `super easy to write benchmarks with Transonic
<https://transonic.readthedocs.io/en/latest/examples/writing_benchmarks/bench.html>`_.


``transonic.typing``
--------------------

I've done a big refactoring of how types are represented in Transonic (see for
example `here
<https://transonic.readthedocs.io/en/latest/generated/transonic.typing.html>`_).
We have now a flexible solution to tell for each backend how the Transonic
types are expressed. It is for example possible to define for the Cython
backend ``np.ndarray`` and memoryviews. Or to specify the memory layout of
arrays (C, Fortran, "C_or_F" or strided) or fixed dimensions (for example for
images, :code:`Array[np.float32, "[3,:,:]"]`).


What's next?
------------

Technically, some ideas are listed in `our roadmaps
<https://transonic.readthedocs.io/en/latest/roadmap.html>`_.

But it's now time to work on adoption (in particular for packages) and on
building a "community" of users and developers.

After `my presentation at EuroScipy
<{static}/docs/ipynb/20190904-euroscipy-transonic/pres.slides.html>`_,
Ashwin Vishnu will soon have the opportunity to present Transonic at `PyCon
Sweden <https://www.pycon.se/>`_!
