---
Title: History of the Transonic project
Date: 2019-09-29
Modified: 2019-09-04
Category: Methods
Slug: transonic-history
---

As described in
<a href=./transonic-vision.html#Status-of-the-project>the transonic vision document</a>,
the motivation was to create a light runtime layer above Pythran to make it much
more easy to use in packages, with a Python API similar to Numba's API.
In October 2018, we created a pure-Python package called FluidPythran. It is the
direct ancestor of Transonic.

Serge Guelton (Pythran's creator and maintainer) and Wolf Vollprecht showed a
simple and short code to do JIT compilation with Pythran. We incorporated a more
serious version of it in FluidPythran, so that the project supports both
ahead-of-time (AOT) and just-in-time (JIT) compilations (with `boost` and `jit`
decorators, respectively).

At the beginning, there was no code analysis in FluidPythran, and we needed to
import the packages to process them, which is really bad for AOT compilation. We
needed a proper code analysis without importing the package. In January 2019, a
new package called [Beniget](https://github.com/serge-sans-paille/beniget) was
written by Serge Guelton. Beniget is a collection of Compile-time analyze on
Python Abstract Syntax Tree (AST). We use it for all analyses for Transonic
front-end.

Discussions with Ashwin Vishnu lead to the idea that the information written in
code using Transonic was sufficient to use other Python accelerators, in
particular Cython and Numba. I started to think that we could merge
FluidPythran and [Statically](https://github.com/AlanCristhian/statically) (a
similar project by Alan Cristhian using Cython to accelerate Python-Numpy
functions).

FluidPythran was renamed Transonic and we started to work on implementing this
unified API for different Python accelerators (Cython, Pythran and Numba). This
hard work was done during an internship of Pierre Blanc-fatin during summer 2019.
He also fixed many other issues and implemented other features in Transonic, as for
example the ability to use imported objects/functions in
boosted/jited functions.

After our first presentation of the project in an international conference
(EuroScipy at Bilbao, which was by the way a great conference and I'd like to
thank the organizers for this moment), this work was finalized and we were able
to release Transonic 0.4.0 on September 2019. I am now globally satisfied by
the state of Transonic code.

<a href=./transonic-vision.html>Read more on Transonic and the long-term vision of the project<a>
