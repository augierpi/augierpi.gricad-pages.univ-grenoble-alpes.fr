Make your numerical Python code fly at transonic speeds!
========================================================

:date: 2019-03-19
:modified: 2019-03-19
:tags: fluiddyn, python
:category: Talks

Le 19 mars 2019, j'ai eu l'opportunité de donner une présentation au
groupe `PySciDataGre <https://python.univ-grenoble-alpes.fr/>`_ sur la question
de la performance en Python numérique en 2019, avec pour titre :

**Make your numerical Python code fly at transonic speeds! Overview of the
Python HPC landscape and zoom on Transonic**

Après une intro générale sur la performance numérique, j'ai présenté le nouvel
outil `Transonic <https://foss.heptapod.net/fluiddyn/transonic>`_.

.. image:: ./images/logo_Pythran.jpeg
  :width: 35%
  :alt: group
  :align: left
.. image:: ./images/logo-fluiddyn.jpg
  :width: 40%
  :alt: guys
  :align: right

Voilà `les slides de ma présentation
<{static}/docs/ipynb/20190319_PySciDataGre_transonic/pres_20190319_PySciDataGre_transonic.slides.html>`_.
