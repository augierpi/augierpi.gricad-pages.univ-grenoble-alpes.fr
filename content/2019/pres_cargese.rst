2d internal gravity wave turbulence at Cargèse
==============================================

:date: 2019-07-14
:modified: 2019-07-14
:tags: fluiddyn
:category: Talks

I attended the `WITGAF 2019 summer school
<https://witgaf2019.sciencesconf.org/>`_ in Cargèse (Corsica) about Waves,
Instabilities and Turbulence in Geophysical and Astrophysical Flows. I
presented the work of Miguel Calpe Linares on 2d stratified turbulence. Here
are `my slides
<{static}/docs/ipynb/201907_2dstrat_witgaf/pres_PA_2dstrat_witgaf.slides.html>`_.
