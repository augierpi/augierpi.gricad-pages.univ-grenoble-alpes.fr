---
Title: ANR DisET meeting à grenoble
Date: 2019-01-30
Modified: 2019-01-30
Category: Talks
---

Voilà une <a
href="./docs/ipynb/20190130ANR_DisET/pres_PA_ANR_DisET.slides.html">
présentation sur notre travail avec Miguel Calpe Linares sur des simulations de
turbulence stratifiée 2D</a>
