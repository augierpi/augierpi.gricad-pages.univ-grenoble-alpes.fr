---
Title: Transonic presented at Pyconfr 2019
Date: 2019-11-03
Modified: 2019-11-03
Category: Methods
---

[Transonic](https://transonic.readthedocs.io/) will be presented at [Pyconfr
2019](https://www.pycon.fr/2019/). Unfortunately, I won't be able to go to
Bordeaux. Fortunately, [Laura MENDOZA](https://members.loria.fr/laura.mendoza/)
kindly accepted to give the presentation! Many many thanks to her!

Here are <a href=./docs/ipynb/20191103-pyconfr-transonic/pres.slides.html>the
slides of the presentation</a>.