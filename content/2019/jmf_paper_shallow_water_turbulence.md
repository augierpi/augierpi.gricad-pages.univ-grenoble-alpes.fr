---
Title: New JFM paper on Shallow water wave turbulence
Date: 2019-07-17
Modified: 2019-07-17
Category: Articles
---

Our paper "[Shallow water wave
turbulence](https://www.cambridge.org/core/journals/journal-of-fluid-mechanics/article/shallow-water-wave-turbulence/FC70A8CFDBC333070AC726C363FE3E0B)"
is now published in the Journal of Fluid Mechanics.

<figure>
  <img src="./images/fig_JFM2019_sw-wave-turb.png" alt="Figure Shallow water wave turbulence article" width="60%">
</figure>

## Abstract

The dynamics of irrotational shallow water wave turbulence forced in large
scales and dissipated at small scales is investigated. First, we derive the
shallow water analogue of the `four-fifths law' of Kolmogorov turbulence for a
third order structure function involving velocity and displacement increments.
Using this relation and assuming that the flow is dominated by shocks we
develop a simple model predicting that the shock amplitude scales as $(\epsilon
d)^{1/3}$, where $\epsilon$ is the mean dissipation rate and $d$ the mean
distance between the shocks, and that the $p$:th order displacement and
velocity structure functions scale as $(\epsilon d)^{p/3} r/d$, where $r$ is
the separation. Then we carry out a series of forced simulations with
resolutions up to $7680^2$, varying the Froude number, $F_{f} = \epsilon^{1/3}
/ ck_f^{1/3}$, where $k_f$ is the forcing wave number and $c$ is the wave
speed. In all simulations a stationary state is reached in which there is a
constant spectral energy flux and equipartition between kinetic and potential
energy in the constant flux range. The third order structure function relation
is satisfied with a high degree of accuracy. Mean energy is found to scale as
$E \sim \sqrt{\epsilon c/k_f}$,
and is also dependent on resolution, indicating
that shallow water wave turbulence does not fit into the paradigm of a
Richardson-Kolmogorov cascade. In all simulations shocks develop, displayed as
long thin bands of negative divergence in flow visualisations. The mean
distance between the shocks is found to scale as $d \sim F_f^{1/2}/k_f$.
Structure functions of second and higher order are found to scale in good
agreement with the model. We conclude that in the weak limit, $F_f \rightarrow
0$, shocks will become denser and weaker and finally disappear for a finite
Reynolds number. On the other hand, for a given $F_{f}$, no matter how small,
shocks will prevail if the Reynolds number is sufficiently large.
