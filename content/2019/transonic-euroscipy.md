---
Title: Euroscipy 2019 to present Transonic
Date: 2019-09-04
Modified: 2019-09-04
Category: Methods
---

I attended [Euroscipy 2019](https://www.euroscipy.org/2019) in Bilbao where
I presented [Transonic](https://transonic.readthedocs.io/).

Here are <a href=./docs/ipynb/20190904-euroscipy-transonic/pres.slides.html>the
slides of my presentation</a>.

### Few notes on what I saw

- Other Python accelerators: [JAX](https://github.com/google/jax),
  [pyccel](https://github.com/pyccel/pyccel),
  [pystencils](https://i10git.cs.fau.de/pycodegen/pystencils)...

- Good target for Transonic and Pythran: <https://github.com/ogrisel/pygbm>

    For Pythran, we would need more OOP support.

- Good Cython and Numba examples for Transonic:

    - <https://github.com/jeremiedbb/tutorial-euroscipy-2019>

    - <http://pycodegen.pages.walberla.net/pystencils/notebooks/demo_benchmark.html>

- We need something like a NumFocus Europe.


### About Transonic

- For Cython, we have to fully work with [fused
  types](https://cython.readthedocs.io/en/latest/src/userguide/fusedtypes.html#using-fused-types)

- For Cython, we need to be able to specify if an array will be used as a
  `memoryview` or a `np.ndarray`.

    Transonic should soon support `A = Array[int, "1d", "memview"]`

- We have to work more on type annotation of Numpy code, see
  [numpy-stub](https://github.com/numpy/numpy-stubs).
