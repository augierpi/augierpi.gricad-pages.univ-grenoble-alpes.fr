---
Title: Soutenance de thèse de Miguel Calpe-Linares
Date: 2020-02-26
Modified: 2020-05-14
Category: Thesis-stages
---

[Miguel Calpe-Linares](https://mcalpelinares.wordpress.com) a soutenu sa thèse
le 26 février 2020! Le manuscript peut être consulté [ici](
https://heptapod.host/meige/miguelcalpelinares_phd/blob/branch/default/latex/phd_MiguelCalpeLinares.pdf)
et la présentation
[là](https://heptapod.host/meige/miguelcalpelinares_phd/blob/branch/default/defense/defense_phd_CalpeLinares.pdf).

## THÈSE

Pour obtenir le grade de DOCTEUR DE L'UNIVERSITE GRENOBLE ALPES

Spécialité : Mécanique des fluides, procédés, énergétique

Arrêté ministériel : 25 mai 2016

Présentée par Miguel CALPE LINARES

Thèse dirigée par Nicolas MORDANT et coencadrée par Pierre AUGIER

préparée au sein du Laboratoire des Écoulements Géophysiques et Industriels
dans l'École Doctorale IMEP2

Étude numérique de la turbulence stratifiée 2D forcée par des ondes internes de
gravité

Numerical study of 2D stratified turbulence forced by internal gravity waves

Thèse soutenue publiquement le 26 février 2020, devant le jury composé de :

- M. Sylvain JOUBAUD,
  Maître de conférences, ENS Lyon, Rapporteur

- M. Wouter BOS,
  Chargé de recherche, École Centrale Lyon, Rapporteur

- M. Paul BILLANT,
  Directeur de recherche, École Polytechnique, Examinateur

- M. Joël SOMMERIA,
  Directeur de recherche, Université Grenoble Alpes, Président

- M. Nicolas MORDANT,
  Professeur des universités, Université Grenoble Alpes, Directeur de thèse

- M. Pierre AUGIER,
  Chargé de recherche, Université Grenoble Alpes, Co-encadrant de thèse

### Abstract

The oceanic motions are composed of eddies with a very large horizontal scale
and 3D prop- agating internal gravity waves. Its kinetic energy spectra follow
the well-known Garrett and Munk spectrum, which is usually interpreted as the
signature of interacting internal gravity waves. Our main motivation is to
reproduce the turbulence regime observed in nature by forcing waves.

Two-dimensional (2D) stratified flows on a vertical cross-section differ from
its analogous three-dimensional flows in its lack of vertical vorticity,
supporting only waves and shear modes. In this PhD work, we perform a numerical
study of 2D stratified turbulence forced with internal gravity waves. We get
rid of the shear modes, sustaining a system only with wave modes. Unlike
precedent studies, the forcing is applied to a localized region of the spectral
space, in which forced internal waves have a similar time scale. We force
intermediate-scale waves to allow the dynamics to develop both upscale and
downscale energy cascade.

We first present the different regimes of 2D stratified turbulence with a
particular interest in the ocean-like regime, i.e. strong stratification and
large Reynolds number. The dynamics of the energy cascade is analysed by means
of the spectral energy budget. Furthermore, we check if it is possible to
obtain turbulence driven by weakly non-linear interacting waves by performing a
spatio-temporal analysis. To conclude, we report results of numerical
simulations forced either on the vorticity or on the eigenmode of the Navier-
Stokes equations in order to study the degree of universality of 2D stratified
turbulence with respect to the forcing.

### Résumé

Les écoulements océaniques sont composés des tourbillons ayant une grande
échelle horizontale et des ondes internes de gravité. Le spectre d'énergie
cinétique suit le fameux spectre de Garrett et Munk qui est habituellement
interprété comme la signature des on- des internes de gravité. Notre motivation
principale est donc de reproduire le régime de turbulence observé dans la
nature avec un système forcé seulement avec des ondes. Les écoulements
stratifiés bidimensionnels (2D) sur une section transversale verticale
diffèrent des écoulements stratifiés tridimensionnels par l'absence de
vorticité verticale et par la présence d'ondes et de modes de cisaillement.
Dans ce travail de thèse, nous effectuons une étude numérique de la turbulence
stratifiée 2D forcée par des ondes internes de gravité. Nous éliminons les
modes de cisaillement pour avoir un système uniquement constitué d'ondes.
Contrairement aux études précédentes, le forçage est appliqué à une région
localisée de l'espace spectral. Nous forçons aussi les ondes avec une échelle
spatiale intermédiaire pour permettre le développement d'une cascade d'énergie
directe et aussi inverse.

Nous présentons d'abord les différents régimes de turbulence stratifiée 2D avec
un intérêt particulier au régime typique de l'océan avec une forte
stratification et un grand nombre de Reynolds. La dynamique de la cascade
d'énergie est analysée par un bilan énergétique spectral. Ensuite, nous
vérifions s'il est possible d'obtenir un régime de turbulence d'onde faible
en réalisant un analyse spatio-temporelle. Nous étudions enfin le degré
d'universalité de la turbulence stratifiée 2D par rapport au forçage.
