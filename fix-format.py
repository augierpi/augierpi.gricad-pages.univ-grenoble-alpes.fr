"""
Replace "%7Bfilename%7D" by "{filename}" in some files

Context: mdformat replace "{filename}" by "%7Bfilename%7D" but we don't want that.

"""

from pathlib import Path

path_input_dir = "content/2025"

paths = sorted(Path(path_input_dir).glob("*.md"))

replacements = {
    "%7Bfilename%7D": "{filename}",
    r"\[ \]": "[ ]",
    r"\[x\]": "[x]",
}

for path in paths:
    code = path.read_text()
    if any(bad in code for bad in replacements.keys()):
        for bad, good in replacements.items():
            code = code.replace(bad, good)
        path.write_text(code)
