PY?=python
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
CACHEDIR=$(BASEDIR)/cache
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

FTP_HOST=localhost
FTP_USER=anonymous
FTP_TARGET_DIR=/

SSH_HOST=localhost
SSH_PORT=22
SSH_USER=root
SSH_TARGET_DIR=$(HOME)/pub

S3_BUCKET=my_s3_bucket

CLOUDFILES_USERNAME=my_rackspace_username
CLOUDFILES_API_KEY=my_rackspace_api_key
CLOUDFILES_CONTAINER=my_cloudfiles_container

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

help:
	@echo 'Makefile for a pelican Web site'
	@echo ''
	@echo 'Usage:'
	@echo '   make html                  (re)generate the web site   '
	@echo '   make clean                 remove the generated files  '
	@echo '   make monitor               Monitoring directory for changes.'
	@echo '   make publish               generate using production settings'
	@echo '   make serve                 serve site at http://localhost:8000'
	@echo '   make rsync_upload          upload the web site via rsync     '
	@echo '   make sync                  pdm sync --clean'
	@echo '   make lock:                 pdm lock'
	@echo '                                                                '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html'
	@echo '                                                                '

clean:
	rm -rf $(OUTPUTDIR) $(CACHEDIR)

html:
	pdm run build

monitor:
	pdm run monitor

serve:
	pdm run serve

publish:
	pdm run publish

rsync_upload: publish
	python3 fix-links.py
	rsync -P -rvzc --hard-links --delete $(OUTPUTDIR)/ servwebuser.legi.grenoble-inp.fr:$(SSH_TARGET_DIR) --cvs-exclude

install_requirements: sync

format:
	mdformat content/2025
	python3 fix-format.py

sync:
	pdm sync --clean

lock:
	pdm lock

.PHONY: help clean monitor serve publish rsync_upload format sync lock