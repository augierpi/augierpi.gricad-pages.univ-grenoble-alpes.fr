import subprocess
from pathlib import Path
from shutil import rmtree

path_output = Path("output/docs")

assert path_output.exists()

paths_year = sorted(path_output.glob("20*"))
path_target = path_output / "2023/quarto_pres_files"
assert path_target.exists()
path_target = str(path_target)


def create_link(path):
    subprocess.run(["cp", "-al", path_target, str(path)], check=True)


for path_year in paths_year:
    if path_year.name != "2023":
        path_quarto_pres_files = path_year / "quarto_pres_files"
        rmtree(path_quarto_pres_files, ignore_errors=True)
        path_quarto_pres_files.unlink(missing_ok=True)
        create_link(path_quarto_pres_files)

    paths_dirs = sorted(
        path
        for path in path_year.glob("*")
        if path.is_dir() and path.name != "quarto_pres_files"
    )

    for path_dir in paths_dirs:
        path_pres_files = path_dir / "pres_files"
        if not path_pres_files.exists() or path_pres_files.is_symlink():
            continue
        rmtree(path_pres_files, ignore_errors=True)
        create_link(path_pres_files)
